BUTTONS

Black - #010101  -  #464646

Blue - #000096  -  #0000b6

Gold - #e8ad4f  -  #fadb4f

Green - #00721f  -  #008e1f

Grey - #bebebe  -  #dedede

Orange - #f58c1f  -  #faac18

Pink - #d40080  -  #e50080

Purple - #58008e  -  #6b008e

Red - #d20000  -  #fa0000

Turquoise - #00ccff  -  #00e4ff

——————————————————————————————

TEXT COLOUR

Black - #010101

Blue - #000096

Gold - #d68c15

Green - #048727

Grey - #515151

Orange - #f58c1f

Pink - #e50080

Purple - #6706a3

Red - #d20000

Turquoise - #00ccff