/**
 * Created by cow-duck on 7/30/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

var socialNetworksArray = [
    "be", "digg", "facebook", "ff", "flickr", "google", "green",
    "linkedin", "red", "reddit", "twickr", "twitter",
    "vimeo", "youtube", "lastfm"
];

$(function init() {
    if (isLocation("edit", currentLocation))
    {
        loadingNetworks();
    }
});

function loadingNetworks()
{
    /**
     * loads all user networks from Parse DB
     * */

    console.warn("LOADING RESOURCES. PLEASE WAIT");

    Parse.Cloud.run( "getUserSavedNetworksByUserID", { id: Parse.User.current().id }, {
        success: function (savedNetworks) {

            Parse.Cloud.run( "getAllNetworks", {}, {
                success: function (networks) {

                    var NetworksClassessssArray = new Array();
                    var networksClassNamesArray = new Array();
                    var favoritesClassNamesArray = new Array();

                    if (networks.length > 0)
                    {
                        var onload = true;
                        var $container = $(".all-netw");
                        var $nodeHeader = $("<h4></h4>");
                        var $nodeUl = $("<ul></ul>").attr({ id: "all-networks-ul"});
                        var $allNetworks;

                        $nodeHeader.append("All Networks");
                        $container.append($nodeHeader);

                        function Network (name, id, iconUrl, url, status)
                        {
                            this.name = name;
                            this.id = id;
                            this.iconUrl = iconUrl;
                            this.url = url;
                            this.status = status;
                            this.clickableNode = "";
                        }

                        Network.prototype.getName = function ()
                        {
                            return this.name;
                        };

                        Network.prototype.getId = function ()
                        {
                            return this.id;
                        };

                        Network.prototype.getIconUrl = function ()
                        {
                            return this.iconUrl;
                        };

                        Network.prototype.getUrl = function ()
                        {
                            return this.url;
                        };

                        Network.prototype.getStatus = function ()
                        {
                            return this.status;
                        };

                        Network.prototype.getClickableNode = function ()
                        {
                            return this.clickableNode;
                        };

                        Network.prototype.setNetworkNode = function ()
                        {
                            var $nodeLI = $("<li></li>");
                            var $img = $("<img />").attr({
                                src: this.iconUrl
                            });
                            var $nodeA = $("<a></a>").attr({
                                href: this.url,
                                class: this.name + " overflowedLink"
                            });
                            this.clickableNode = $nodeA;
                            var _this_ = this;

                            $nodeA.on( 'click', function (click) {
                                click.preventDefault();

                                $('.network-selection').dialog('close');
                                _this_.appendNetworkNodeToInput();
                                _this_.setAddRemoveButtons();
                                return false;
                            });

                            $nodeA.append($img);
                            $nodeLI.append($nodeA)
                            $nodeUl.append($nodeLI);
                        };

                        Network.prototype.setClickableNode = function (node)
                        {
                            this.clickableNode = node;
                        };

                        Network.prototype.setClickOnNode = function (node)
                        {
                            // do nothing
                        };

                        Network.prototype.unsetClick = function (object)
                        {
                            object.off( 'click' );
                        };

                        Network.prototype.appendNetworkNodeToInput = function ()
                        {

                            if ($.inArray(this.name, networksClassNamesArray) != -1 || $.inArray(this.name, favoritesClassNamesArray) != -1)
                            {
                                // if found the same - do nothing
                                console.log("empty");
                            }
                            else
                            {
                                var $node = $(".selected-netw");

                                $node.addClass(this.name);
                                $node.removeClass("network-selection-link"); // to disable click event and opening dialog with all networks

                                $('.network-selection').dialog( 'disable' );
                                $('.netw-empty-add').die( 'click' );

                                var $img = $("<img />").attr({
                                    src: this.iconUrl
                                });
                                $node.append($img);
                                $node.css({
                                    background: "none"
                                });

                                $(".netw-link").css("display", "block");
                                var link = this.url.substr(this.url.indexOf("//") + "//".length, this.url.length) + "/";
                                var cross = locationPrefix + controls_dir + "cross33x33.png";
                                var plus = locationPrefix + controls_dir + "plus33x33.png";
                                var $userNewNetworkInput =
                                    "<div id='user-network-input-outer-container'>" +
                                        "<span class='link'>" + link + "</span>" +
                                        "<input class='link-input' placeholder=' Enter your profile name'/>" +
                                        "<span class='link-buttons'><a class='remove'><img src='" + cross + "'></a></span>" +
                                        "<span class='link-buttons'><a class='add'><img src='" + plus + "'></a></span>" +
                                        "</div>";

                                $(".netw-link").append($userNewNetworkInput);

                                /**
                                 * don't know what is this :C
                                 * but who cares?
                                 * */
                                if ($(".drag-netw").is(":visible"))
                                {
                                    $(".netw-link").css("top", "204px");
                                }

                                $(".link-input").focus();
                            }
                        };

                        Network.prototype.setAddRemoveButtons = function ()
                        {
                            var _this_ = this;

                            $(".remove").on("click", function (e) {
                                e.preventDefault();

                                var $node = $(".selected-netw");

                                $(".netw-link").empty();
                                $(".netw-link").hide();

                                $node.find( "img" ).detach();
                                $node.addClass( "network-selection-link" );
                                $node.removeClass( _this_.name );

                                removeInlineStyle($node, "background");
                                $(".bottom-error").fadeOut(250);

                                /**
                                 * reenable add new networks event
                                 * */
                                $('.network-selection').dialog( 'enable' );

                                $('.netw-empty-add').live('click', function () {
                                    $('.network-selection').dialog('open');
                                    return false;
                                });
                                clearingData($allNetworks);
                            });

                            $(".add").on("click", function (e) {
                                e.preventDefault();

                                var $selectedNode = $(".selected-netw");

                                var userLink = $(".link-input").val().trim();
                                if (userLink.length > 0)
                                {
                                    $("#edit-networks-button").show();
                                    $(".selected-networks").show();
                                    $(".bottom-error").fadeOut(250);

                                    $(".netw-link").empty();
                                    $(".netw-link").hide();

                                    $selectedNode.find( "img" ).detach();
                                    $selectedNode.addClass( "network-selection-link" );
                                    $selectedNode.removeClass( _this_.name );

                                    removeInlineStyle($selectedNode, "background");

                                    $(".added-netw").hide();
                                    $(".pref-netw").show();
                                    $(".selected-netw").show();

                                    if ($.inArray(_this_.name, networksClassNamesArray) != -1)
                                    {
                                        // if found the same - do nothing
                                    }
                                    else
                                    {
                                        var $a = $("<a></a>").attr({
                                            href: _this_.url + "/" + userLink,
                                            class: _this_.name + " overflowedLink toSave"
                                        });

                                        $a.addClass("draggable");
                                        $a.addClass("network-icon-draggable");

                                        var $img = $("<img />").attr({
                                            src: _this_.iconUrl
                                        });

                                        $a.append($img);

                                        $a.hide();
                                        $(".all-selected").prepend($a);
                                        $a.fadeIn(250);

                                        $(".all-selected ." + _this_.name).removeData();
                                        $(".all-selected ." + _this_.name).data( 'data-href', userLink ).data( 'data-name', _this_.name).data( 'data-icon', _this_.iconUrl ).data( 'data-id', _this_.id).data( 'data-url', _this_.url).data( 'data-isFavorite', false);

                                        $(".draggable").draggable({
                                            drag: function (drag, ui) {
                                                // do something
                                            },
                                            containment: '.edit-box.user-netw',
                                            cursor: 'move',
                                            stack: '.edit-box.user-netw',
                                            revert: true
                                        });

                                        $(".draggable").on('click', function (click) {
                                            click.preventDefault();

                                        });

                                        clearingData($allNetworks);
                                        networksClassNamesArray.push(_this_.name);
                                    }
                                }
                                else
                                {
                                    window.userFeedback("Please, enter your profile name.", false)
                                }

                                /**
                                 * reenable adding new networks event
                                 * */
                                $('.network-selection').dialog( 'enable' );
                                $('.netw-empty-add').live('click', function () {
                                    $('.network-selection').dialog('open');
                                    return false;
                                });
                            });
                        };

                        for (var i in networks)
                        {
                            var network = networks[i];
                            var icon = network.get("iconURI");
                            var url = network.get("maskURI");
                            var name = network.get("name");
                            var id = network.id;
                            var status = network.get("status");

                            if (status != "deleted" && status != "suspended")
                            {
                                var networkElement = new Network(name, id, icon, url, status);

                                networkElement.setNetworkNode();

                                NetworksClassessssArray.push(networkElement);
                            }
                        }
                        $container.append($nodeUl);
                        $allNetworks = $container;

                        /**
                         * if user has saved networks
                         * */
                        if (savedNetworks.length > 0)
                        {

                            /**
                             * available back-end network values
                             *
                             * userId
                             * networksCount
                             * networkID
                             * networkURI
                             * networkName
                             * networkStatus
                             * networkIconURI
                             * userURI
                             * userFavorite
                             *
                             */

                            var $nets = $(".all-selected");
                            var $favs = $("#droppable");

                            $(".netw-link").hide();
                            $(".added-netw").hide();
                            $(".pref-netw").show();
                            $(".selected-networks").show();
                            $("#edit-networks-button").show();

                            for (var i in savedNetworks)
                            {
                                var network = savedNetworks[i];
                                if (network.networkStatus != "deleted" && network.networkStatus != "suspended")
                                {
                                    var $a = $("<a></a>").attr({
                                        href: network.networkURI + "/" + network.userURI,
                                        class: network.networkName + " overflowedLink toSave"
                                    });
                                    var $img = $("<img />").attr({
                                        src: network.networkIconURI
                                    });

                                    $a.append($img);
                                    $a.hide();
                                    if (network.userFavorite)
                                    {
                                        $a.addClass("appended-favorite-network");
                                        $favs.prepend($a);
                                        favoritesClassNamesArray.push(network.networkName);
                                    }

                                    if (!network.userFavorite)
                                    {
                                        $a.addClass( "draggable" );
                                        $a.addClass( "network-icon-draggable" );
                                        $a.off( 'click' );
                                        $nets.prepend($a);
                                        networksClassNamesArray.push(network.networkName);
                                    }

                                    $a.fadeIn(250);

                                    $("." + network.networkName + ".toSave")
                                        .data( 'data-name', network.networkName )
                                        .data( 'data-icon', network.networkIconURI )
                                        .data( 'data-id', network.networkID )
                                        .data( 'data-url', network.networkURI )
                                        .data( 'data-href', network.userURI)
                                        .data( 'data-isFavorite', network.userFavorite );
                                }
                            }
                        }


                        function setFavoritesClickable(onload)
                        {
                            /**
                             * if user already has saved networks
                             * */

                            if (onload)
                            {
                                $('.appended-favorite-network').on( 'click', function (click) {
                                    click.preventDefault();
                                });
                                return;
                            }

                            $('.appended-favorite-network').on('click', function (click) {
                                click.preventDefault();

                                var favoriteNetworkNodesCount = 7;
                                var networkName = $(this).data( 'data-name' );

                                if (networkName)
                                {
                                    if (favoritesClassNamesArray.indexOf(networkName) != -1)
                                    {
                                        $(".network-icon-draggable." + networkName).addClass( "toSave").show();
                                        $(this).fadeOut(250, function () { $(this).detach(); });

                                        favoritesClassNamesArray.splice(favoritesClassNamesArray.indexOf(networkName), favoritesClassNamesArray.indexOf(networkName) + 1);

                                        if ($(".network-icon-draggable." + networkName).length > 0)
                                            networksClassNamesArray.push( networkName );
                                    }
                                }
                                else
                                {
                                    console.log("empty networkName for " + $(this));
                                }

                                //
                                if ($("#droppable").children().length < favoriteNetworkNodesCount + 1)
                                {
                                    $("#droppable").droppable( 'enable' );
                                    $("#droppable").css('border', '2px solid #cdcdcd');
                                }
                            });
                        }

                        function setEditButton()
                        {
                            /**
                             * edit button customizing
                             * */
                            $("#action-edit-networks-button").fadeTo( 10, 0.7, function () {
                                // animation is over
                            });
                            $("#action-edit-networks-button").on( 'click', function (click) {
                                click.preventDefault();

                                if ($(".all-selected").data( 'action' ) != "edit")
                                {
                                    $(".all-selected").data( 'action', 'edit' );

                                    $("#droppable").droppable( 'disable' );
                                    $(".add-netw").fadeTo( 250, 0.0, function () {
                                        $(this).css('visibility', 'hidden')
                                    });
                                    $(".netw-empty-add").fadeTo( 250, 0.0, function () {
                                        $(this).css('visibility', 'hidden')
                                    });

                                    $(this).html("EXIT EDIT MODE");
                                    $(this).parent().css( "width", "109px" );

                                    $(".draggable").draggable( 'option', 'distance', 1000 );
                                    $(".draggable").css('cursor', 'pointer');

                                    $("#action-edit-networks-button").fadeTo( 250, 1.0, function () {
                                        // animation is over
                                    });

                                    setFavoritesClickable(false);

                                    $.each($(".network-icon-draggable"), function (val, i) {

                                        $(this).css('background-image', 'url("' + locationPrefix + controls_dir + 'cross33x33.png"), url("' + $(this).data( 'data-icon' ) + '")');
                                        $(this).css('background-size', '20px 20px, 42px');
                                        $(this).css('background-position', 'right top, left top');
                                        $(this).css('background-repeat', 'no-repeat');
                                        $(this).find("img").hide();

                                        $(this).on( 'click', function (click) {
                                            click.preventDefault();

                                            $(this).fadeOut(250, function () { $(this).detach();  });

                                            var networkName = $(this).data( "data-name" );
                                            if (networkName)
                                            {
                                                if (favoritesClassNamesArray.indexOf(networkName) != -1)
                                                {
                                                    $("#droppable a." + networkName).fadeOut(250, function () { $(this).detach(); });
                                                    favoritesClassNamesArray.splice(favoritesClassNamesArray.indexOf(networkName), favoritesClassNamesArray.indexOf(networkName) + 1);
                                                }
                                                if (networksClassNamesArray.indexOf(networkName) != -1)
                                                {
                                                    networksClassNamesArray.splice(networksClassNamesArray.indexOf(networkName), networksClassNamesArray.indexOf(networkName) + 1);
                                                }
                                            }
                                            else
                                            {
                                                console.log("empty networkName for " + $(this));
                                            }
                                        });
                                    });
                                }
                                else
                                {
                                    $(".all-selected").data( 'action', "back" );

                                    $(".add-netw").fadeTo( 250, 1.0, function () {
                                        $(this).css('visibility', 'visible')
                                    });
                                    $(".netw-empty-add").fadeTo( 250, 1.0, function () {
                                        $(this).css('visibility', 'visible')
                                    });

                                    $(this).html("EDIT MODE");
                                    removeInlineStyle($(this).parent(), "width");

                                    $("#droppable").droppable( 'enable' );
                                    $(".draggable").draggable( 'option', 'distance', 10 );
                                    $(".draggable").css('cursor', 'default');
                                    $("#action-edit-networks-button").fadeTo( 250, 0.7, function () {
                                        // animation is over
                                    });

                                    setFavoritesClickable(onload);

                                    $.each($(".network-icon-draggable"), function (val, i) {

                                        removeInlineStyle($(this), "background-image");
                                        removeInlineStyle($(this), "background-size");
                                        removeInlineStyle($(this), "background-position");
                                        removeInlineStyle($(this), "background-repeat");
                                        $(this).find("img").show();

                                        $(this).on( 'click', function (click) {
                                            click.preventDefault();
                                        });
                                    });
                                }
                            });
                        }

                        function setDroppableEvent()
                        {
                            /**
                             * we do things dropping...
                             * */

                            $("#droppable").droppable({
                                accept: '.all-selected a',
                                hoverClass: 'favorites-network-hovered',
                                drop: function (drop, ui) {
                                    var countFavorites = $(this).children().length;
                                    var networkNodesCount = 7;
                                    /**
                                     * if we has 7 favorite networks. 7 for now. may be increased in the future
                                     * */
                                    if (countFavorites < networkNodesCount )
                                    {
                                        var networkName = ui.draggable.data( "data-name" );
                                        if (networkName)
                                        {
                                            if ($.inArray(networkName, favoritesClassNamesArray) != -1)
                                            {
                                                /**
                                                 * if found the same
                                                 * */
                                                $(this).css('border','2px solid #FF554B');
                                                $(this).delay(1500).queue( function (next) {
                                                    $(this).css('border', '2px solid #e5e5e5');
                                                    next();
                                                });
                                            }
                                            else
                                            {
                                                var $appendedString = setDroppedNetwork(ui);
                                                $appendedString.hide();
                                                $(this).append($appendedString);
                                                $($appendedString).fadeIn(250);

                                                if (networksClassNamesArray.indexOf(networkName) != -1)
                                                {
                                                    ui.draggable.fadeOut(250, function () { $(this).removeClass( "toSave").hide(); });
                                                    networksClassNamesArray.splice(networksClassNamesArray.indexOf(networkName), networksClassNamesArray.indexOf(networkName) + 1);
                                                }

                                                if (countFavorites == networkNodesCount-1)
                                                {
                                                    /**
                                                     * stack of favorites is full
                                                     * */
                                                    $(this).css('border','2px solid #FF554B');
                                                    $(this).delay(1500).queue( function (next) {
                                                        $(this).css('border', '2px solid #e5e5e5');
                                                        next();
                                                    });
                                                    $(this).droppable( 'disable' );
                                                }
                                            }
                                        }
                                        else
                                        {
                                            console.log("empty networkName for " + $(this));
                                        }
                                    }
                                    else
                                    {
                                        /**
                                         * stack of favorites is full
                                         * */
                                        $(this).css('border','2px solid #FF554B');
                                        $(this).delay(1500).queue( function (next) {
                                            $(this).css('border', '2px solid #e5e5e5');
                                            next();
                                        });
                                        $(this).droppable( 'disable' );
                                    }

                                    /**
                                     * objects data
                                     *
                                     * .data( 'data-name' )
                                     * .data( 'data-icon' )
                                     * .data( 'data-id' )
                                     * .data( 'data-url' )
                                     * .data( 'data-href' )
                                     * .data( 'data-isFavorite' )
                                     *
                                     */
                                    setFavoritesClickable(onload);
                                }
                            });
                        }

                        function setDroppedNetwork(droppedObject)
                        {
                            /**
                             * returns string to append in droppable box
                             * */

                            var href = droppedObject.draggable.data( 'data-href' );
                            var className = droppedObject.draggable.data( 'data-name' );
                            var imgHref = droppedObject.draggable.data( 'data-icon' );
                            var id = droppedObject.draggable.data( "data-id" );
                            var url = droppedObject.draggable.data( "data-url" );

                            favoritesClassNamesArray.push(className);

                            var $a = $("<a></a>").attr({
                                href: url + "/" + href,
                                class: className + " appended-favorite-network overflowedLink toSave"
                            });

                            $a.removeData();
                            $a.data( 'data-name', className ).data( 'data-icon', imgHref ).data( 'data-id', id).data( 'data-url', url ).data( 'data-href', href ).data( 'data-isFavorite', true );

                            var $img = $("<img />").attr({
                                src: imgHref
                            });

                            $a.append($img);

                            return $a;
                        }

                        function setSaveEvent()
                        {
                            var saveButton = $("#action-save-networks");

                            saveButton.on( 'click', function (click) {
                                click.preventDefault();

                                var $toSave = $(".toSave");
                                var dataObject = {};
                                var dataObjectsArray = new Array();
                                var nodesIdsArray = new Array();

                                if ($toSave.length > 0)
                                {
                                    $.each( $toSave, function (i, val) {
                                        try
                                        {
                                            var $node = $(this);
                                            if (typeof $node != 'undefined')
                                            {
                                                if (typeof $node.data() != 'undefined')
                                                {
                                                    /**
                                                     * if user setting up
                                                     * */
                                                    if ($.inArray($node.data( 'data-id' ), nodesIdsArray) != -1 && !$node.data( 'data-isFavorite' ))
                                                    {
                                                        return;
                                                    }

                                                    dataObject = {
                                                        href: $node.data( 'data-href' ),
                                                        icon: $node.data( 'data-icon' ),
                                                        id: $node.data( 'data-id' ),
                                                        isFavorite: $node.data( 'data-isFavorite' ),
                                                        name: $node.data( 'data-name' ),
                                                        url: $node.data( 'data-url' )
                                                    }
                                                    nodesIdsArray.push($node.data( 'data-id' ));
                                                    dataObjectsArray.push(dataObject);
                                                }
                                            }
                                        }
                                        catch (err)
                                        {
                                            console.log("bad for " + val);
                                            console.log(err);
                                            return;
                                        }
                                    });

                                    saveNetworks(dataObjectsArray);
                                }
                                else
                                {
                                    saveNetworks(dataObjectsArray);
                                }
                            });
                        }

                        setDraggableEvent();
                        setFavoritesClickable(onload);
                        setEditButton();
                        setDroppableEvent();
                        setSaveEvent();

                        console.warn("LOADING RESOURCES COMPLETE");

                    }
                    else
                    {
                        window.userFeedback("Sorry something went wrong while loading networks. Try to reload page.", false);
                    }
                }, // all networks
                error: function (error) {
                    console.error(error);
                }
            });
        }, // saved networks
        error: function (error) {
            console.error(error);
        }
    });
}

function clearingData(all)
{
    /**
     * housekeeping a little
     * */
    $("#netw-search").val("");
    $(".social").children().detach();
    $(".social").append(all);
    all.show();
}

function setDraggableEvent()
{
    /**
     * speaking function names RULES!
     * */
    $(".draggable").draggable({
        drag: function (drag, ui) {
            // do something
        },
        containment: '.edit-box',
        cursor: 'move',
        stack: '.edit-box',
        revert: true
    });
    $(".draggable").css('cursor', 'default');
    $(".draggable").on('click', function (click) {
        click.preventDefault();
    });
}

function saveNetworks(networks)
{
    var data = {
        networks: networks,
        currentID: Parse.User.current().id
    };

    Parse.Cloud.run( "saveNetworks" , data, {
        success: function (success) {
            if (success)
            {
                console.log("Network saved correctly! Congrats!");
//                window.userFeedback("Last second saved!", true);

                $('#about_selection').click();
            }
        },
        error: function (error) {
            console.log(error);
            window.userFeedback(error.message, false);
        }
    });
}