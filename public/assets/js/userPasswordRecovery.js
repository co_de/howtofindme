/**
 * Created by cow-duck on 7/4/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

$(function () {
    if (currentLocation.indexOf("sign-in") >= 0)
    {
        init();
    }
});

function init()
{
    // hiding password recovery box then first start
    $('.password-recovery').hide();
    $('#recovery-success-text-for-back').hide()

    // show password recovery box
    $('.forgot a').on( 'click', function (click) {
        click.preventDefault();
        $('.signin-box').hide(250);
        $('.password-recovery').show();
    });

    // show bottom error if necessary
    $('#reset-pass-email').on( 'blur', function() {
        return emailValidator($(this));
    });

    // redefine click event fro submitting recovery form
    $('#password-recovery-send').on( 'click', function (click) {
        click.preventDefault();

        if (emailValidator($('#reset-pass-email')))
        {
            var email = $('#reset-pass-email').val().trim();

            parseSender(email);
        }
        else
        {
            console.log("something went wrong with email validation");
        }
    });

    $('#password-recovery-cancel').on( 'click', function (click) {
        click.preventDefault();

        $('.password-recovery').hide(250);
        $('.signin-box').show(250);

        backToNormalInput();
    });

    $('#back-to-login').on( 'click', function (click) {
        click.preventDefault();

        $('.password-recovery').hide(250);
        $('.signin-box').show(250);

        setTimeout( function() {
            $('#password-recovery-send').show();
            $('#password-recovery-cancel').show();
            $('#recovery-success-text-for-back').hide();
        }, 400);
    });
}

window.emailValidator = function (object)
{
    var notEmpty = false;
    var validate = false;
    var message;
    var error = false;
    notEmpty = checkIfEmpty(object);
    validate = emailChecker(object);

    if (!notEmpty)
    {
        message = "Enter your e-mail!";
        error = true;
    }
    else
    {
        if (!validate)
        {
            message = "Check your e-mail!";
            error = true;
        }
        else
        {
            message = "";
            error = false;
        }
    }

    if (error && message != '' && message.length > 0)
    {
        // make input border red and adds the error icon
        colorizeInvalidateInput(object);
        window.userFeedback(message, false)
        return false;
    }
    else
    {
        // make input border green
        colorizeValidateInput(object);
        $('.bottom-error').fadeOut(250);
        return true;
    }
}

function emailChecker(object)
{
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(object.val());
}

function checkIfEmpty(object)
{
    if (object.val() == "" && object.val().length <= 0) return false;
    else return true;
}

function colorizeValidateInput(object)
{
    $('#'+object.attr('id')+'_error').fadeOut(250);
    object.css('border','1px solid #6EFF66');
}

function colorizeInvalidateInput(object)
{
    $('#'+object.attr('id')+'_error').fadeIn(250);
    object.css('border','1px solid #FF6666');
}

function parseSender(email)
{
    var message;

    Parse.User.requestPasswordReset(email, {
        success:function() {
            $('input#reset-pass-email').val("");
            console.log("User found. Please check your mail box!");
            message = "The email with password reset information has been sent successfully'. Please check your mail inbox.";

            window.userFeedback(message, true, 3000);
            var timeout;
            timeout = setTimeout( function() {
                window.location.reload(true);
            }, 10000);

            $('#password-recovery-send').hide(250);
            $('#password-recovery-cancel').hide(250);
            $('#recovery-success-text-for-back').show(250)

            $('#back-to-login').on( 'click', function (click) {
                click.preventDefault();

                clearTimeout(timeout);
                backToNormalInput();
            });

            $('#password-recovery-cancel').on( 'click', function (click) {
                click.preventDefault();

                clearTimeout(timeout);
                backToNormalInput();
            });
        },
        error:function(error) {
            message = "This user is not our member yet!"

            window.userFeedback(message, false);
            console.error("nothing found on: " + email +". " + message);
        }
    });
}

function backToNormalInput()
{
    $(".bottom-error").html("");
    $('.bottom-error').fadeOut(50);
    $('#reset-pass-email').css( "border", "1px solid #e5e5e5");
    $('#reset-pass-email_error').fadeOut(50);
}