/**
 * Created by cow-duck on 7/26/14.
 */
var parse_id = 'RBCXcBe4RLU3hfbpXYgqSAtgO2MYUpidQfJaFd4I';
var js_key = 'bqJAuYLQXgGrESHGT9LCVUoyyuqp2qpwK1yZ575z';

Parse.initialize(parse_id, js_key);


var currentUser = Parse.User.current();
var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

/**
 * protocol declaration
 * */
var prot = "https:";
var devider = "/";
var ddevider = "//";
var locationPrefix = prot + ddevider + currentHostname + devider;
var locationPrefixWithoutDevider = prot + ddevider + currentHostname;

$(function a_init() {
    Parse.Cloud.run( "a", { id: currentUser.id }, {
        success: function (a) {
            if (a)
            {
                pageConstructor();
            }
            else
            {
                window.location = locationPrefixWithoutDevider;
            }
        },
        error: function (error) {
            console.log(error);
            window.location = locationPrefixWithoutDevider;
        }
    });
});

function pageConstructor()
{
    setHeader();
    setContent("networks")
}

function setHeader()
{
    var $body = $("body");
    var $main = $("<div></div>").attr({ id: "main" });

    var $header = $("<div></div>").attr({ id: "header" });
    var $container = $("<div></div>").attr({ id: "container" });

    var $hContainer = $("<div></div>").attr({ id: "header-cont" });
    var $pE = $("<p></p>").attr({ id: "exit-cont" });
    var $exit = $("<a></a>").attr({ id: "exit", href: locationPrefix + (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id) });
    $pE.append($exit);

    var $p = $("<p></p>").attr({ id: "header-text" });
    var $topButts = $("<div></div>").attr({ id: "top-buttons" });

    $main.append($header);
    $main.append($container);
    $body.append($main);

    /**
     * top buttons
     * */
    var $networks = $("<a>Networks</a>").attr({ id: "network-edit", href: "networks" }).on( 'click', function (click) {
        click.preventDefault();

        setContent($(this).attr('href'));
    });
    var $themes = $("<a>Themes<a/>").attr({ id: "themes-edit", href: "themes"}).on( 'click', function (click) {
        click.preventDefault();

        setContent($(this).attr('href'));
    });

    $p.html("Hello, " + currentUser.get("first_name"));
    $exit.html("Exit");
    $hContainer.append($pE);
    $hContainer.append($p);
    $header.append($hContainer);
    $header.append($topButts);

    $topButts.append($networks);
    $topButts.append($themes);
}

function setContent(block)
{
    $("#container").children().detach();
    if (block == "networks")
    {
        loadNetworks();
    }
    if (block == "themes")
    {
        console.log("themes here");
//        loadthemes();
    }
    if (block == "users")
    {
        console.log("users here");
//        loadUsers();
    }
}

function loadNetworks()
{
    Parse.Cloud.run( "getAllNetworks", {}, {
        success: function (networks) {

            var $table = $("<table></table>").attr({ id: "main-table", cellpadding: "10px", rules: "rows", bordercolor: "#d5d5d5" });
            var $tableCaption = $("<caption></caption>").attr({ id: "table-header" });

            $tableCaption.append("<b>Networks</b>");
            $table.append($tableCaption);
            $("#container").append($table);



            var headers = [ "#", "Name" , "Icon", "URL", "Status", "Created At", "Edit"];

            var $tableHeaders = $( "<tr></tr>" );

            for (var i in headers)
            {
                $tableHeaders.append("<th>" + headers[i] + "</th>");
            }

            $("#main-table").append($tableHeaders);

            if (networks.length > 0)
            {
                for (var j in networks)
                {
                    var index = j;
                    var network = networks[j];
                    var $tableContents = $(
                        "<tr class=\"node node_" + ++index  + "\">" +
                            "<td>" + index + " <div id=\"hidden-network-id\">" + network.id + "</div></td>" +
                            "<td> <div class=\"cell data\" name=\"name\">" + network.get("name") + "</div> </td>" +
                            "<td> <div class=\"cell data\" name=\"icon\" id=\"node_image\"><img src=\"" + network.get("iconURI") + "\"/></div> </td>" +
                            "<td> <div class=\"cell data\" name=\"url\"><a href=\"" + network.get("maskURI") + "\">" + network.get("maskURI") + "</a></div> </td>" +
                            "<td> <div class=\"cell data\" name=\"status\"><p>" + network.get("status") + "</p></div> </td>" +
                            "<td> <div class=\"cell\"><p><b>" + network.createdAt + "</b></p></div> </td>" +
                            "<td> <div class=\"cell\">" +
                            "<div class=\"editable\">" +
                            "<div class=\"edit-node node_edit\" id=\"node_edit_" + index + "\"><a data-id=\"" + network.id + "\" data-index=\"" + index +"\" id=\"node_edit_button_" + index + "\" href=\"#\">EDIT</a> /</div>" +
                            "<div class=\"edit-node node_del\" id=\"node_del_" + index + "\"><a data-id=\"" + network.id + "\" data-index=\"" + index +"\" id=\"node_del_button_" + index + "\" href=\"#\">DEL</a></div>" +
                            "</div>" +
                            "</div> " +
                            "</td>" +
                            "</tr>"
                    );
                    $("#main-table").append($tableContents);

                    $("#node_edit_button_" + index).on( 'click', function (click) {
                        click.preventDefault();

//                        var $back = $(this).parent().next().find("a");
                        var index = $(this).attr( "data-index" );
                        var id = $(this).attr( "data-id" );
                        var $dataContainer = $('tr.node_' + index + " td .cell.data");
                        var data = new Array();
                        var keys = new Array();
                        var values = new Array();
                        var datanode = {};

                        $.each($dataContainer, function (i, val) {
                            var _this = $(this);

                            keys.push(_this.attr('name'));
                            values.push(_this.attr('id') == 'node_image' ? _this.find('img').attr('src') : _this.text());
                        });

                        for (var i in keys)
                        {
                            datanode[keys[i]] = values[i];
                        }
                        data.push(datanode);

                        $(".node_" + index).children().hide();
                        $(".node_" + index).append(getAppStringForEdit(data[0], index, id));

                        $.each($("#new_status_" + index + " option"), function (i, option) {
                            var _this = $(this);
                            if (_this.val() == data[0].status)
                                _this.attr( 'selected', 'selected' );
                        });

                        $(".node_" + index).addClass( "new_node_" + index );
                        $(".node_" + index).addClass( "new_node_edit" );
                        $(".new_node_" + index).removeClass( "node_" + index );
                        $(".new_node_" + index).removeClass( "node" );

                        $("#node_save_button_" + index).on( 'click', function (click) {
                            click.preventDefault();

                            var id = $(this).attr( "data-id" );

                            checkDataBeforeSave($(".new_node_edit"), 'update', id);
                        });

                        $("#node_back_button_" + index).on( 'click', function (click) {
                            click.preventDefault();

                            $(".new_node_" + index + ".new_node_edit").find("td.newtd").detach();
                            $(".new_node_" + index).addClass( "node_" + index );
                            $(".node_" + index).addClass( "node" );
                            $(".node_" + index).removeClass( "new_node_" + index );
                            $(".node_" + index).removeClass( "new_node_edit" );
                            $(".node_" + index).children().show();
                        });
                    });
                    /**
                     * TODO provide networks deletion
                     * */

                }
                appendNewNode(networks.length);
                $(".new_node input").attr({ disabled: "disabled" });
                $(".new_node select").attr({ disabled: "disabled" });
            }
            else
            {
                appendNewNode(networks.length);
            }
            setButtons();
            setFunctions(networks);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function setButtons()
{
    var $container = $("#container");
    var $buttonContainer = $("<div></div>").attr({ id: "button-cont" });
''
    var $add = $("<input />").attr({ id: "add", class: "action-button", type: "button", value: "Add" });
    var $cancel = $("<input />").attr({ id: "cancel", class: "action-button", type: "button", value: "Cancel" })
    var $save = $("<input />").attr({ id: "save", class: "action-button", type: "button", value: "Save" });

    $container.append($buttonContainer);
    $buttonContainer.append($save);
    $buttonContainer.append($cancel);
    $buttonContainer.append($add);
}

function setFunctions(networks)
{
    var table = $("#main-table");

    var saveButton = $("#save");
    var addButton = $("#add");
    var cancelButton = $("#cancel");
    var exitButton = $("#exit");
    var node = $(".new_node input");
    var node2 = $(".new_node select");

    if (node.attr( "disabled" ) == "disabled")
    {
        saveButton.attr({ disabled: "disabled" });
        addButton.removeAttr( "disabled" );
        cancelButton.attr({ disabled: "disabled" });
    }
    else
    {
        saveButton.removeAttr( "disabled" );
        addButton.attr({ disabled: "disabled" });
        cancelButton.removeAttr( "disabled" );
    }

    /**
     * buttons handlers
     * */
    addButton.on( 'click', function (click) {
        node.removeAttr( "disabled").addClass("new_node_edit");
        node2.removeAttr( "disabled" ).addClass("new_node_edit");
        saveButton.removeAttr( "disabled" );
        cancelButton.removeAttr( "disabled" );
        addButton.attr({ disabled: "disabled" });
    });

    cancelButton.on( 'click', function (click) {
        node.attr({ disabled: "disabled" }).removeClass("new_node_edit");
        node2.attr({ disabled: "disabled" }).removeClass("new_node_edit");
        saveButton.attr({ disabled: "disabled" });
        addButton.removeAttr( "disabled" );
        cancelButton.attr({ disabled: "disabled" });
        removeInlineStyle($(".new_node input"), "border");
    });

    saveButton.on( 'click', function (click) {
        checkDataBeforeSave($(".new_node"), 'save', null);
    });

    exitButton.on( 'click', function (click) {
        click.preventDefault();

        if ($(".new_node_edit").length > 0)
        {
            if (confirm("Are you sure you want to exit without saving?"))
                window.location = $(this).attr( "href" );
            else
                return;
        }
        else
        {
            window.location = $(this).attr( "href" );
        }
    });
}

function appendNewNode(lastDigit)
{
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
        var $str = getAppString(lastDigit);
        $("#main-table").append($str);
        return true;
    }
    else
    {
        console.log("No HTML5 FIle reading support. Sorry!");
        alert("No HTML5 FIle reading support. Sorry!");
        return false;
    }
}

function checkDataBeforeSave(object, action, id)
{
    var dataCount = object.length;
    if (dataCount > 0 )
    {
        var isVarified = true;
        $.each($(object.find("input")), function (i, val) {

            if (action == "save")
            {
                if ($(val).val() == "")
                {
                    isVarified = false;
                    $(val).css({ border: "1px solid red" });
                }
            }
            if (action == "update")
            {
                if ($(val).attr("type") != "file")
                {
                    if ($(val).val() == "")
                    {
                        isVarified = false;
                        $(val).css({ border: "1px solid red" });
                    }
                }
            }
        });

        if (isVarified)
        {
            var data = {};
            var dataCollection = new Array();
            var file = object.find(".new_icon")[0].files[0];

            if (file)
            {
                //var fileName = (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : "date") + "_icon.png";
                var networkIcon = new Parse.File("icon.png", file);
                networkIcon.save().then( function () {
                    var image_url = networkIcon._url;
                    if (image_url)
                    {
                        var url = object.find(".new_url").val().trim();
                        if (url.indexOf("//") >= 0)
                        {
                            url.substr(url.indexOf("//"), url.indexOf("//") + 1);
                        }
                        url = prot + ddevider + url;
                        data = {
                            name: object.find(".new_name").val().toLowerCase().trim(),
                            icon: image_url,
                            url: url,
                            status: object.find(".new_status").val().trim()
                        }
                        dataCollection.push(data);
                    }
                    if (action == 'save')
                        saveData(dataCollection);
                    if (action == 'update')
                        updateData(dataCollection, id);
                });
            }
            else
            {
                if (object.find(".new_icon").data( 'url' ))
                {
                    data = {
                        name: object.find(".new_name").val().toLowerCase().trim(),
                        icon: object.find(".new_icon").data( 'url' ),
                        url: prot + ddevider + object.find(".new_url").val().trim(),
                        status: object.find(".new_status").val().trim()
                    }
                    dataCollection.push(data);

                    if (action == 'save')
                        saveData(dataCollection);
                    if (action == 'update')
                        updateData(dataCollection, id);
                }
                else
                {
                    object.find(".new_icon").css({ border: "1px solid red" });
                    console.log("empty file");
                }
            }
        }
    }
    else
    {
        return;
    }
}

function saveData(data)
{
    console.log(data);
    var networkClassName = "Network";

    var NetworkNodeClass = Parse.Object.extend(networkClassName);
    var networkNode = new NetworkNodeClass();

    networkNode.set( "iconURI", data[0].icon);
    networkNode.set( "maskURI", data[0].url);
    networkNode.set( "name", data[0].name);
    networkNode.set( "status", data[0].status);

    networkNode.save( null, {
       success: function (node) {
           location.reload(true);
       },
       error: function (node, error) {
           console.log(error);
       }
    });
}

function updateData(data, id)
{
    var networkClassName = "Network";

    var NetworkNodeClass = Parse.Object.extend(networkClassName);
    var query = new Parse.Query(NetworkNodeClass);

    query.equalTo("objectId", id);
    query.find({
        success: function (results) {
            if (results.length > 0)
            {
                var networkNode = results[0];

                networkNode.set( "iconURI", data[0].icon);
                networkNode.set( "maskURI", data[0].url);
                networkNode.set( "name", data[0].name);
                networkNode.set( "status", data[0].status);

                networkNode.save( null, {
                    success: function (node) {
                        location.reload(true);
                    },
                    error: function (node, error) {
                        console.log(error);
                    }
                });
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function getAppString(lastDigit)
{
    var $str = $(
        "<tr class=\"new_node new_node_"+ ++lastDigit + "\">" +
            "<td> <p class=\"cell number\" id=\"number_" + lastDigit + "\">" + lastDigit + "</p> </td>" +
            "<td> <input class=\"cell new_name\" id=\"new_name_" + lastDigit + "\" placeholder=\"Enter a new network name\" type=\"text\" /> </td>" +
            "<td> <input class=\"cell new_icon\" id=\"new_icon_" + lastDigit + "\" placeholder=\"Choose a new network icon file\" type=\"file\" accept=\"image/*\" name=\"file[]\" /> </td>" +
            "<td> <div class=\"https-prev cell\"><p>http://</p><input class=\"new_url\" id=\"new_url_" + lastDigit + "\" placeholder=\"Enter a new network URL\" type=\"text\" /></div> </td>" +
            "<td>" +
            "<select class=\"cell new_status\" id=\"new_status_" + lastDigit + "\">" +
            "<option>active</option>" +
            "<option>deleted</option>" +
            "<option>suspended</option>" +
            "<option>popular</option>" +
            "<option>promoted</option>" +
            "</select>" +
            "</td>" +
            "<td> <p class=\"cell new_time\" id=\"new_time_" + lastDigit + "\"><b>" + new Date() + "</b></p> </td>" +
            "</tr>"
    );
    return $str;
}

function getAppStringForEdit(data, lastDigit, id)
{
    var $str = $(

            "<td class=\"newtd\"> <p class=\"cell number\" id=\"number_" + lastDigit + "\">" + lastDigit + "</p> </td>" +
            "<td class=\"newtd\"> <input class=\"cell new_name\" id=\"new_name_" + lastDigit + "\" placeholder=\"Enter a new network name\" type=\"text\" value=\"" + data.name + "\" /> </td>" +
            "<td class=\"newtd\"> <div class=\"old-icon\"><img src=\"\" /></div> <input class=\"cell new_icon\" id=\"new_icon_" + lastDigit + "\" placeholder=\"Choose a new network icon file\" type=\"file\" accept=\"image/*\" name=\"file[]\" data-url=\"" + data.icon + "\" /> </td>" +
            "<td class=\"newtd\"> <div class=\"https-prev cell\"><p>https://</p><input class=\"new_url\" id=\"new_url_" + lastDigit + "\" placeholder=\"Enter a new network URL\" type=\"text\" value=\"" + data.url.split("//")[1] + "\" /></div> </td>" +
            "<td class=\"newtd\">" +
            "<select class=\"cell new_status\" id=\"new_status_" + lastDigit + "\">" +
            "<option value=\"active\">active</option>" +
            "<option value=\"deleted\">deleted</option>" +
            "<option value=\"suspended\">suspended</option>" +
            "<option value=\"popular\">popular</option>" +
            "<option value=\"promoted\">promoted</option>" +
            "</select>" +
            "</td>" +
            "<td class=\"newtd\"> <p class=\"cell new_time\" id=\"new_time_" + lastDigit + "\"><b>" + new Date() + "</b></p> </td>" +
            "<td class=\"newtd\"> <div class=\"cell\">" +
                "<div class=\"editable\">" +
                "<div class=\"edit-node node_edit\" id=\"node_edit_" + lastDigit + "\"><a data-id=\"" + id + "\" rel=\"" + lastDigit +"\" id=\"node_save_button_" + lastDigit + "\" href=\"#\">SAVE</a> /</div>" +
                "<div class=\"edit-node node_del\" id=\"node_del_" + lastDigit + "\"><a data-id=\"" + id + "\" rel=\"" + lastDigit +"\" id=\"node_back_button_" + lastDigit + "\" href=\"#\">BACK</a></div>" +
                "</div>" +
                "</div> " +
            "</td>"

    );

    return $str;
}

function removeInlineStyle($object, styleValue)
{
    /**
     https://stackoverflow.com/a/6553745
     * */
    var search = new RegExp(styleValue + '[^;]+;?', 'g');
    $object.attr( "style", function (i, style) {
        if (style) return style.replace(search, '');
    });
}