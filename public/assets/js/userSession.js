/**
 * Created by cow-duck on 6/5/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);


//signing the new user in
$(function () {

    $("#sign_in").on("click", function (e) {
        e.preventDefault();

        var uname = $('#uname').val();
        var pwd = $('#pwd').val();
        var message;

        if (uname.trim().length <= 0 && pwd.trim().length <= 0)
        {
            message = "All fields are empty. Please fill them.";
            window.errorBorder($('#uname'), 350);
            window.errorBorder($('#pwd'), 350);
            window.userFeedback(message, false);
            return;
        }
        if (uname.trim().length <= 0)
        {
            message = "Email field is empty. Please fill it.";
            window.userFeedback(message, false);
            window.errorBorder($('#uname'), 350);
            return;
        }
        if (pwd.trim().length <= 0)
        {
            message = "Password field is empty. Please fill it";
            window.userFeedback(message, false);
            window.errorBorder($('#pwd'), 350);
            return;
        }
        if (uname.length > 0 && pwd.length > 0)
        {
            var here = "sign-in";
            $(this).fadeOut(150, function () {
                window.showLoading(true, here);
            });

            try
            {
                Parse.User.logIn(uname, pwd, {
                    success: function (user) {
                        unsetCookie();
                        window.location = locationPrefix + "edit";
                    },
                    error: function (user, error) {
                        window.showLoading(false, here);

                        window.errorBorder($('#uname'), 550);
                        window.errorBorder($('#pwd'), 550);

                        message = error.message.toUpperCase() + ".<br/>Try to check your login data.";
                        window.userFeedback(message, false);
                    }
                });
            }
            catch (err)
            {
                console.error(err.message);
                window.showLoading(false, here);
            }
        }
    });
});

//checking if the user is logged in and chanding the login/logout button
$(function login_logout() {
    if (Parse.User.current()) {
        $('#login_logout').html('Logout');
    }
    else
    {
        $('#login_logout').html('Login');
        $('#login_logout').attr( "href", locationPrefix + "sign-in.htm");
    }

    $('#login_logout').click(function (click) {
        click.preventDefault();

        if (Parse.User.current()) {
            Parse.User.logOut();
            unsetCookie();
//            window.location = locationPrefixWithoutDevider;
            window.location = locationPrefix + "sign-in.htm";
        }
        else
        {
            window.location = locationPrefix + "sign-in.htm";
        }
    });
});
