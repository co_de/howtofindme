/**
 * Created by cow-duck on 7/18/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);
var parametersArray = getSearchParameters();

$(function init () {
    if (
         window.isLocation("search", currentLocation) ||
         window.isLocation("mycontacts", currentLocation) ||
         window.isLocation("connections", currentLocation)
       )
    {
        window.showLoading(true);

        $(".pag-top").hide();
        $("#inner_search_result").hide();

        var currentUser = Parse.User.current();

        if (parametersArray)
            setSearchQuery(parametersArray);
        setInputHandlers(parametersArray, currentUser);
        onLoad(currentUser);
    }
});

/**
 * http://stackoverflow.com/a/5448635
 * */
function transformToAssocArray( prmstr )
{
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++)
    {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function getSearchParameters()
{
    var prmstr = decodeURIComponent(window.location.search.substr(1));
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : undefined;
}

function setEmptyResults()
{
    var zero = 0;

    window.showLoading(false);
    $(".quantity").html("<b><span class=\"users-found\">" + zero + "</span> People Found</b>");
    $(".search-result").fadeOut(250, function() {
        $("#search-no-results").fadeIn(250);
    });
    $(".pagination").fadeOut(250, function() {

    });
}

function setLocationSearchFromUserPage(parametersArray, currentUser)
{
    if (parametersArray.q.indexOf(",") >= 0)
    {
        var city = parametersArray.q.split(",")[0].trim();
        var country = parametersArray.q.split(",")[1].trim();

        var data = {
            search: "",
            current: (currentUser ? currentUser.id : undefined),
            city: city,
            country: country
        };

        $("#search-no-results").hide(250);
        $("#inner_search_result").hide(250);

        $("#location_input").val(city + ", " + country);

        search(data, currentUser);
    }
}

function setInputHandlers(getParameters, currentUser)
{
    setCross($("#name_input"));
    setCross($("#location_input"));

//    setCross($("#country_input"));

//    var attr = "This field should not be left blank."
//    $('#name_input').attr( 'title', attr );
//    $('#name_input').attr( 'x-moz-errormessage', attr );
//    $('#name_input').attr( 'required' );
//
//    var name = document.getElementById("name_input");
//
//    name.addEventListener( "input", function() {
//        name.setCustomValidity(name.value.length == 0 ? attr : name.value.length < 3 ? "Three (3) symbols minimum are required." : "");
////        if (name.validity.customError)
////            window.errorBorder($("#name_input"), 1500);
//    }, false);

    $("#search-form-query").on( 'submit', function (submit) {
        submit.preventDefault();

        if ($('#search_ncc').data( 'disabled' ))
            return false;

        searchHandler(getParameters, currentUser);
    });

    $("#name_input").on( 'keyup', function (keyup) {
        setCross($(this));

        if ($(this).val().length >= 3)
            setIconDisabled($('#search_ncc'), false);
        else
            setIconDisabled($('#search_ncc'), true);
    });

    $("#location_input").on( 'keyup', function (keyup) {
        setCross($(this));
    });

    $(".input-clear-button").on( 'click', function () {
        var className = $(this).attr( "class");
        if (className.indexOf(" ") >= 0)
        {
            var cnSplit = className.split(" ");
            $("#" + cnSplit[1]).val("");
            $(".input-clear-button." + cnSplit[1]).hide(250)
            if ($('#name_input').val().length < 3 && $('#location_input').val().length < 3)
                setIconDisabled($('#search_ncc'), true);
        }
    });
}

function setCross(object)
{
    var val = object.val();
    var id = object.attr( "id" );
    var cross =  $(".input-clear-button." + id);

    if (val.length > 0)
    {
        if (id.indexOf("name") >= 0)
            cross.css( "left", object.parent().width() + 23 + "px");
        if (id.indexOf("location") >= 0)
            cross.css( "left", object.parent().prev().width() + object.parent().width() + 23 + 10 + "px");

        cross.show(250);
    }
    else
    {
        cross.hide(250);
    }
}

function setSearchQuery(params)
{
    /**
     * if we have GET parameters
     * */

    setInputs(params.q, $("#name_input"));

    if  (params.lci && params.lco)
        setInputs(params.lci + ", " + params.lco, $("#location_input"));

    if ($('#name_input').val().length < 3 && $("#location_input").val().length < 3)
        setIconDisabled($('#search_ncc'), true);
    else
        setIconDisabled($('#search_ncc'), false);

    $("#name_input").focus();
}

function setInputs(query, object)
{
    if (query)
    {
        var paramsSplit;
        var searchQuery = "";

        if (query.indexOf("+") >= 0 )
            paramsSplit = query.split("+");

        if (paramsSplit)
        {
            for (var i in paramsSplit)
            {
                searchQuery += paramsSplit[i];
                if (i != paramsSplit.length - 1)
                    searchQuery += " ";
            }
        }
        else
        {
            searchQuery = query;
        }

        object.val(searchQuery.trim());
    }
}

function searchHandler(getParameters, currentUser)
{
    var name = $("#name_input").val();
    var city = $("#location_input").val().split(',')[0];
    var country = $("#location_input").val().split(',')[1];
    var nameFromGet = "";

    if (name)
        name = name.trim();
    else
        name = "";

    if (city)
        city = city.trim();
    else
        city = "";

    if (country)
        country = country.trim();
    else
        country = "";

    if (parametersArray.lci && city == "")
    {
        delete parametersArray.lci;
    }

    if (parametersArray.lco && country == "")
    {
        delete parametersArray.lco;
    }

    if (parametersArray.q && name == "")
    {
        delete parametersArray.q
    }

    if (name == "" && city == "" && country == "")
    {
        window.errorBorder($("#name_input"), 1500);
        window.errorBorder($("#location_input"), 1500);
        return;
    }

    if (name != "") setCross($("#name_input"));
    if (city != "") setCross($("#location_input"));

    if (getParameters.q)
    {
        if (getParameters.q.indexOf("+") >= 0)
        {
            var splittedValues = getParameters.q.split("+");
            for (var i in splittedValues)
            {
                nameFromGet += splittedValues[i];
                if (i != splittedValues.length - 1)
                    nameFromGet += " ";
            }
        }
        else
        {
            nameFromGet = getParameters.q;
        }
    }
    else
    {
        nameFromGet = "";
    }

    if (name == nameFromGet && (name != "" && nameFromGet != "") && (city == "" && country == "" || city == "" || country == ""))
    {
        return;
    }

    if ( (name != "" && name.length < 3) || (city != "" && country != "" && name != "" && name.length < 3) )
    {
        window.errorBorder($("#name_input"), 1500);
        return;
    }

    if (name != nameFromGet)
    {
        var data = null;
        var title = document.title;
        var search = window.location.search;
        var url = currentLocation + (parametersArray.lci ? "?lci=" + parametersArray.lci + "&lco=" + parametersArray.lco + "&q=" : "?q=" ) + name;

        try
        {
            history.pushState(data, title, url);
            parametersArray.q = name;
        }
        catch(err)
        {
            console.error(err.message);
        }
    }

    if (city != "" && country != "")
    {
        var data = null;
        var title = document.title;
        var search = window.location.search;
        var url = currentLocation + (parametersArray.q ? "?q=" + parametersArray.q + "&lci=" : "?lci=") + city + "&lco=" + country;

        try
        {
            history.pushState(data, title, url);
            parametersArray.lci = city;
            parametersArray.lco = country;
        }
        catch(err)
        {
            console.error(err.message);
        }
    }

    var data = {
        search: (name && name.length > 0 ? name.toLowerCase() : name),
        current: (currentUser ? currentUser.id : undefined),
        city: city,
        country: country
    };
    $(".quantity").html("");
    $('#pagination-pager').children().hide(250, function() {
        $(this).detach();
    });
    $("#search-no-results").hide(250);
    $("#inner_search_result").hide(250);
    window.showLoading(true);
    searchUsers(data, currentUser);
}

function onLoad(currentUser)
{
    var searchQuery = $("#name_input").val();
    var city = $("#location_input").val().split(',')[0];
    var country = $("#location_input").val().split(',')[1];

    if (city)
        city = city.trim();
    else
        city = "";

    if (country)
        country = country.trim();
    else
        country = "";

    if (currentUser)
        $("#search_not_logged_in").css("display", "none");
    else
        $("#search_not_logged_in").css("display", "block");

    if (searchQuery == "" && city == "" && country == "")
    {
        console.error("searchQuery is empty");
        setEmptyResults();
        return;
    }

    window.showLoading(true);
    $('#pagination-pager').children().hide(250, function() {
        $(this).detach();
    });
    $("#search-no-results").hide(250);
    $("#inner_search_result").hide(250);


    var data = {
        search: (searchQuery && searchQuery.length > 0 ? searchQuery.toLowerCase() : searchQuery),
        current: (currentUser ? currentUser.id : undefined),
        city: city,
        country: country,
        limit: 6,
        page: 1
    }
    searchUsers(data, currentUser);
}

function searchUsers(data, currentUser)
{
    Parse.Cloud.run( "getUserObjectsFromSearchString", data, {
        success: function(users) {
            appendFoundedUserData(users, currentUser, data);
        },
        error: function (error) {
            console.error(error);
            setEmptyResults();
        }
    });
}

function appendFoundedUserData(userObjects, currentUser, data)
{
    if (userObjects.length > 0)
    {
        $(".quantity").html("<b><span class=\"users-found\">" + userObjects.length + "</span> People Found</b>");
        $('#pagination-pager').children().detach();
        $("#inner_search_result").children().detach();
        $("#inner_search_result").hide();

        var resultsPerPage = 6;
        var firstPage = 1;
        var visiblePages = 5;
        var pagesCount = Math.ceil(userObjects.length / resultsPerPage);

        for (var i in userObjects)
        {
            var user = parseInt(i, 10) + 1;
            var page = Math.ceil(user / resultsPerPage);

            var userId = userObjects[i].id;
            var userPath = (userObjects[i].url == undefined ? userId : ( userObjects[i].url.length <= 0 ? userId : userObjects[i].url) );
            var userHref = locationPrefix + userPath;
            var userName = userObjects[i].username;
            var userMainLocation = userObjects[i].mainloc;
            var userImg = ( userObjects[i].img.length > 0 ?
                            userObjects[i].img : locationPrefix + backgrounds_dir + "no-result92x92.png" );
            var sharedConnectionHref = locationPrefix + "connections?u=" + (userPath == undefined ? userId : userPath);

            var conCount = (currentUser ? userObjects[i].connections : 0);

            var innerHtml = (typeof conCount != 'undefined' ? conCount : 0) + " Shared Connection" + (conCount == 1 ? "" : "s");
            var apStr =
                "<div rel=\"" + page + "\" class='result-item result-item-active user_" + userId + "' id='other_user'>" +
                    "<span class='pic'>" +
                    "<a href=" + userHref + " title=''>" +
                    "<img class='user_photo' src='" + userImg + "'/>" +
                    "</a>" +
                    "</span>" +
                    "<span class='txt'>" +
                    "<span class='heading'>" +
                    "<a href=" + userHref + " title=''>" + userName + "</a>" +
                    "</span>" +
                    "<span class='title'>" + userMainLocation + "</span>" +
                    "<span class='share-yellow'>" +
                    (currentUser ?
                        "<a class=\"sc-link-zero-connections shared-connections-count_" + userId + "\" " +
                        "href=" + sharedConnectionHref + " title=\"" + innerHtml + "\">" + innerHtml + "</a>" : "") +
                    "</span>" +
                    "</span>" +
                    "</div>";

            $("#inner_search_result").append(apStr);

            if (currentUser)
            {
                var userConnectionsNode = $(".shared-connections-count_" + userId);
                if (currentUser.id == userId)
                {
                    /**
                     * if user found himself in results
                     * */
                    innerHtml = "It's you!";
                    userConnectionsNode.attr( "href" , "#" );
                    userConnectionsNode.attr( "title" , "It's you!" );
                    userConnectionsNode.on( 'click' , function (click) {
                        click.preventDefault();
                    });
                    $(".user" + userId + " .share-yellow").css( "background", "none");
                    $(".user" + userId + " .share-yellow").css( "padding", "0");
                    userConnectionsNode.html(innerHtml);
                }
                else
                {
                    if (conCount <= 0)
                    {
                        userConnectionsNode.attr( "href" , "#" );
                        userConnectionsNode.attr( "title" , "" );
                        userConnectionsNode.on( 'click' , function (click) {
                            click.preventDefault();
                        });
                        userConnectionsNode.css( "color" , "#cbcbcb" );
                        $(".user" + userId + " .share-yellow").addClass("share-dark");
                        $(".user" + userId + " .share-dark").removeClass("share-yellow");
                    }
                    else
                    {
                        userConnectionsNode.removeClass( "sc-link-zero-connections" );
                        userConnectionsNode.addClass( "sc-link-NOzero-connections" );
                    }
                }
            }

            window.showLoading(false);
            $(".search-result").show();
            $("#inner_search_result").fadeIn(300);
        }

        setPagination(firstPage);

        /**
         * http://esimakin.github.io/twbs-pagination/
         * */
        if (pagesCount > 1)
        {
            $('#pagination-pager').twbsPagination({
                totalPages: pagesCount,
                visiblePages: visiblePages,
                href: '#',
                onPageClick: function (event, page) {
                    setPagination(page);
                }
            });
        }
        setWidth(pagesCount);
    }
    else
    {
        setEmptyResults();
    }
}


function setPagination(pageNumber)
{
    var $itemsCollection = $('.result-item');

    $.each($itemsCollection, function (item, value) {
        if ($(this).attr( "rel" ) != pageNumber)
        {
            $(this).hide();
        }
        else
        {
            $(this).show();
        }
    });
}

function setWidth(pagesCount)
{
    var war = 123;
    var buf = 20;
    var px = 'px';
    if (pagesCount == 2)
        $('ul.pagination').css( 'width', war + px );
    if (pagesCount == 3)
        $('ul.pagination').css( 'width', war + buf + px );
    if (pagesCount == 4)
        $('ul.pagination').css( 'width', war + buf*2 + px );
}