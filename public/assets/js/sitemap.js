/**
 * Created by cow-duck on 7/25/14.
 */

$(function siteMapInit () {
    if (window.isLocation("sitemap", currentLocation))
    {
        var linksContainer = $("article.content ul");

        var registerLink = "";
        var signIN = "sign-in.htm",
            search = "search?q=harlan",
            privacy = "privacy.htm",
            contactUs = "contacts",
            legal = "legal.htm";


        var allowedHREF = [  registerLink,   signIN,     search,     privacy,    contactUs,      legal ];
        var allowedHTML = [ "Registration", "Sign In",  "Search",   "Privacy",  "Contact Us",   "Legal"];

        for (var i in allowedHREF)
        {
            var href = locationPrefix + allowedHREF[i];
            var html = allowedHTML[i];
            var link = "<li><a href=\"" + href + "\" title=\"" + html + "\">" + html +  "</a></li>";

            linksContainer.append(link);
        }

        if (currentUser)
        {
            linksContainer.append("<span id=\"sitemap-terminator-line\" /><br />");

            var myProfile = (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id ),
                editMyProfile = "edit-profile",
                MyContacts = "mycontacts",
                inviteFriends = "invite-friends.htm";

            var deniedHREF = [ myProfile,       editMyProfile,      MyContacts,     inviteFriends ];
            var deniedHTML = [ "My Profile",    "Edit My Profile",  "My Contacts",  "Invite Friends"]

            for (var i in deniedHREF)
            {
                var href = locationPrefix + deniedHREF[i];
                var html = deniedHTML[i];
                var link = "<li><a href=\"" + href + "\" title=\"" + html + "\">" + html +  "</a></li>";

                linksContainer.append(link);
            }
        }
    }
});
