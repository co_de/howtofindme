/**
 * Created by cow-duck on 10/21/14.
 */

/* ****************************************************************************************************************** */
/* ************************************* GOOGLE GEO API SUPPORT IMPLEMENTATION ************************************** */
/* ****************************************************************************************************************** */

/**
 * initialization of the google location
 * */
$(function GoogleGeoApiLocationInit () {
    if (
        window.isLocation("edit", currentLocation) ||
            window.isLocation("search", currentLocation) ||
            window.isLocation("mycontacts", currentLocation)
        )
    {

        window.onload = loadScript;
    }
});


function loadScript()
{
    var script = document.createElement( "script" );

    script.type = "text/javascript";
    script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=en&libraries=places&callback=initialize";

    document.body.insertBefore(script, document.body.firstChild);
}

function initialize()
{
    /**
     * all of address types from google geo api. just info
     * */
    var adressTypes = ["street_address", "route", "intersection", "political", "country", "administrative_area_level_1",
        "administrative_area_level_2", "administrative_area_level_3", "colloquial_area", "locality",
        "sublocality", "neighborhood", "premise", "subpremise", "postal_code", "natural_feature",
        "airport", "park", "point_of_interest", "post_box", "street_number", "floor", "room"];

    if (window.isLocation("edit", currentLocation))
        currentLocationInit();

    if (window.isLocation("search", currentLocation))
        googleGeoApiInit();
}


function googleGeoApiInit()
{
    /** @type {HTMLInputElement} */
    var input = document.getElementById('location_input');
//    var options = {
//        types: ['(cities)']
//    };
    var autocomplete = new google.maps.places.Autocomplete(input);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
//        console.log(place);
        var _address = '';
        var cityIt = 0, countryIt = 0;
        var cityIndex = 0, countryIndex = 0;
        var locatedCountry = "";
        var locatedCity = "";

        if (place.address_components)
        {
            _address = [
                (place.address_components[0] && place.address_components[0].long_name || ''),
                (place.address_components[1] && place.address_components[1].long_name || ''),
                (place.address_components[2] && place.address_components[2].long_name || '')
            ].join(' ');

            for (var i = 0; i < place.address_components.length; i++)
            {
                for (var j = 0; j < place.address_components[i].types.length; j++)
                {
                    if (place.address_components[i].types[j] == "political")
                    {
                        if (place.address_components[i].types[j - 1] == "country")
                        {
                            locatedCountry = place.address_components[i].long_name;
                            countryIt++;
                            countryIndex = i;
                        }
                        if (place.address_components[i].types[j - 1] == "locality")
                        {
                            locatedCity = place.address_components[i].long_name;
                            cityIt++;
                            cityIndex = i
                        }
                        // if we found the country and the city and they are in one common object
                        if ((countryIt == 1 && cityIt == 1) && (countryIndex == cityIndex))
                        {
                            countryIt = 0;
                            cityIt = 0;
                            break;
                        }
                    }
                }
            }
        }

        var composed_addr = get_addr(place);
//        console.log("composed_addr = " + composed_addr);
        var addressString = (composed_addr ? composed_addr : place ? place.formatted_address : "") + (locatedCity != "" ? (typeof locatedCity != 'undefined' ? "|" + locatedCity : "") : "");
//        console.log("addressString = " + addressString);
        var addressArray = get_updated_google_geo_ui(addressString != 'undefined' ? addressString : "");
//        console.log("addressArray = " + addressArray);

        if (addressArray.length > 1)
        {
            var address = addressArray[0] + ", " + addressArray[1];
            input.addEventListener( 'blur', function() {
                if (input.value != "")
                {
                    console.log(address);
                    input.value = address;
                    clearFields();
                    $("#name_input").focus();
                    setIconDisabled($('#search_ncc'), false);
                }
            });
            setTimeout( function() {
                $("#location_input").val(address);
                setIconDisabled($('#search_ncc'), false);
                clearFields();
            }, 1);
        }
        else
        {
            input.addEventListener( 'blur', function() {
                if (input.value != "")
                {
                    console.log(_address);
                    input.value = _address;
                    clearFields();
                    $("#name_input").focus();
                    setIconDisabled($('#search_ncc'), false);
                }
            });
            setTimeout( function() {
                $("#location_input").val(_address);
                setIconDisabled($('#search_ncc'), false);
                clearFields();
            }, 1);
        }



    });
}

function clearFields()
{
    var btn = $('#add_current_location');
    var innerText = "Current Location";
    var swappedInnerText = "Clear fields";

    if ($('#location_input').val() != '')
    {
        if (btn.html() == innerText)
            btn.html(swappedInnerText);
    }
}

function get_updated_google_geo_ui(address)
{
//    console.log("update_ui started with address: " + address);

    //$('#location_input').val('');
    var buf, addressArray;
    var array = new Array();

    // if we found local city name we print it after a |-symbol
    if (address.indexOf('|') > 1)
    {
        buf = address.split('|');
        if (buf.length > 1)
            addressArray = buf[0].split(',');
    }
    else
    {
        addressArray = address.split(',');
    }

    if (addressArray.length > 1)
    {
        if (address.indexOf('|') > 1)
        {
            // if we found a local city name we insert it into a value input
            array.push(address.substr(address.indexOf('|') + 1, address.length));
            array.push((addressArray[addressArray.length - 1].trim()));
        }
        else if (addressArray.length >= 3)
        {
            // inserting two first items of full formatted address (if we has more then two items of it)
            array.push(addressArray[0].trim() + ", " + addressArray[1].trim());
            array.push(addressArray[addressArray.length - 1].trim());
        }
        else
        {
            array.push(addressArray[0].trim());
            array.push(addressArray[addressArray.length - 1].trim());
        }
    }
    else
    {
        // if the country is empty (almost impossible, but provided)
        $('#location_input').val(address);
        array.push(address)
    }

    return array;
}


function get_addr(place)
{
    var string = null;
    var locality = null;
    var country_name = null;

    if (place)
    {
        if (place.adr_address)
        {
            if ($(place.adr_address).filter('span.locality').length > 0)
                locality = $(place.adr_address).filter('span.locality').html();
            else
                locality = $(place.adr_address).filter('span.extended-address').html();

            if ($(place.adr_address).filter('span.country-name').length > 0)
                country_name = $(place.adr_address).filter('span.country-name').html();

            string = (locality && country_name ? locality + ", " + country_name : (locality ? locality : (country_name ? country_name : null)) );
        }
    }

    return string;
}




function currentLocationInit()
{
    var geocoder;
    var currentCountry, currentCity;
    try
    {
        geocoder = new google.maps.Geocoder();
    }
    catch (error)
    {
        geocoder = null;
        console.log(error.message);
    }

    if (typeof geocoder != 'undefined' && geocoder)
    {
//        console.log("geocoder init");

        if (navigator.geolocation) {
            /**
             * if user approves the browser geo pos visual query
             * */
            navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
        }

        function successFunction(position)
        {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            codeLatLng(lat, lng);
        }

        function errorFunction()
        {
            console.log("Something went wrong with geolocation. Sorry.");
        }

        function codeLatLng(lat, lng)
        {
            /** TODO current location must be tested with proxy servers * now tested only for Belarus
             * */

            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    //console.log(results);
                    if (results[1])
                    {
                        for (var i = 0; i < results[0].address_components.length; i++)
                        {
                            for (var j = 0; j < results[0].address_components[i].types.length; j++)
                            {
                                if (results[0].address_components[i].types[j] == "political")
                                {
                                    currentCountry = results[0].address_components[i].long_name;
                                    currentCity = results[0].address_components[i - 1].long_name;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        console.log("No results found!");
                    }
                }
                else
                {
                    console.log("Geocoder failed due to: " + status);
                }
            });
        }

        /**
         * define current location insert from google geo.api support
         * */
        $('#add_current_location').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var innerText = "Current Location";
            var otherInnerText = "Clear fields";
            if (btn.html() == innerText)
            {
                btn.html(otherInnerText);
                $('#location_input').val(currentCity + ", " + currentCountry);
            }
            else
            {
                btn.html(innerText);
                $('#location_input').val("");
            }
        });
    }
}