var parse_id = 'RBCXcBe4RLU3hfbpXYgqSAtgO2MYUpidQfJaFd4I';
var js_key = 'bqJAuYLQXgGrESHGT9LCVUoyyuqp2qpwK1yZ575z';

Parse.initialize(parse_id, js_key);



/**
 * declarations
 * */
var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);
var deprecated = true;

var isRetina = (
    window.devicePixelRatio > 1 ||
        (window.matchMedia && window.matchMedia("(-webkit-min-device-pixel-ratio: 1.5),(-moz-min-device-pixel-ratio: 1.5),(min-device-pixel-ratio: 1.5)").matches)
    );

var prot = "https:";
var devider = "/";
var ddevider = "//";
var locationPrefix = prot + ddevider + currentHostname + devider;
var locationPrefixWithoutDevider = prot + ddevider + currentHostname;

var currentUser = Parse.User.current();

/**
 * project directories
 * */
var assets_dir = 'assets/';
var css_dir = assets_dir + 'css/';
var img_dir = assets_dir + 'img/';
var themes_dir = assets_dir + 'themes/';
var backgrounds_dir = img_dir + 'backgrounds/';
var controls_dir = img_dir + 'controls/';
var favicon_dir = img_dir + 'favicon/';
var icons_dir = img_dir + 'icons/';
var logos_dir = img_dir + 'logos/';
var pins_dir = img_dir + 'pins/';


$(window).load( function pageSecure (onload) {
    onload.stopPropagation(true);

    /**
     * prevent unauthorized users to watch some pages
     * */

    /**
     * allowed pages for unauthorized users
     * */
    var allow = [
                    "sign-in.htm",
                    "index.htm",
                    "search",
                    "sitemap.htm",
                    "privacy.htm",
                    "contacts",
                    "legal.htm"
                ];

    /**
     * denied pages for unauthorized users
     * */
    var deny = [
                    "invite-friends.htm",
                    "register/thank-you",
                    "register/step2",
                    "mycontacts",
                    "connections",
                    "edit"
               ];

    if (currentLocation.length > 0 && currentLocation != "")
    {
        if ($.inArray(currentLocation, deny) >= 0)
        {
            var currentUser = Parse.User.current();
            if (!currentUser)
            {
                /**
                 * deny unsigned user for watching pages content and redirect to sign in form
                 * */
                window.location = locationPrefix + "sign-in.htm";
                console.log("Are you signed-in, sir Stranger?");
            }
        }
        if ($.inArray(currentLocation, allow) >= 0)
        {
            /**
             * allow user to see pages even if no currentUser session found
             * */
            console.log("Take a tour, sir Sir!");
        }
    }
});


//$(function setGoogleFonts()
//{
//    WebFontConfig = {
//        google: { families: [ 'Open+Sans:700,300:latin,cyrillic' ] }
//    };
//    (function() {
//        var wf = document.createElement('script');
//        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
//            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
//        wf.type = 'text/javascript';
//        wf.async = 'true';
//        var s = document.getElementsByTagName('script')[0];
//        s.parentNode.insertBefore(wf, s);
//    })();
//});


function setLoadingWrapper()
{
    var loadingText = "Loading...";
    var $body = $('body');
    var $p = $('<p></p>').html(loadingText);
    var $div = $('<div></div>').attr({
        id: "loading-wrapper"
    });

    $div.append($p);
    $body.prepend($div);
}

function addLoadingWrapper()
{
    $('#loading-wrapper').fadeIn(100);
}

function hideLoadingWrapper()
{
    $('#loading-wrapper').fadeOut(250);
}


$(function setUserColor () {

    /**
     * when page is loading user see default color-theme because of Parse Cloud async delay
     * */
    //insertUserColorCssTag("orange");
    if (currentUser)
        getUserColor(currentUser.id);
    else
        insertUserColorTags("orange");
});


function getUserColor(id)
{
    Parse.Cloud.run( "getUserColor", { id: id }, {
        success: function (uscolor) {
            var color = "";

            if (uscolor && uscolor.length > 0)
            {
                color = uscolor[0].get("userColor").get("name");
                var _css_url_ = locationPrefix + themes_dir + color + "/" + color + ".css";
                if (typeof color != 'undefined')
                {
                    $.get(_css_url_)
                        .done(function() {
                            insertUserColorTags(color);
                        })
                        .fail(function() {
                            insertUserColorTags("orange");
                            console.error("No '" + color + "' color theme has been found. Switching to defaults.");
                        });
                }
                else
                {
                    insertUserColorTags("orange");
                    console.error("No such color theme has been found. Switching to defaults.");
                }
            }
            else
            {
                Parse.Cloud.run( "getAvailableUserColors", {}, {
                    success: function (colors) {
                        if (colors.length > 0)
                        {
                            for (var i in colors)
                            {
                                var ucolor = colors[i];

                                if (ucolor.get("isDefault"))
                                {
                                    color = ucolor.get("name");
                                }
                            }

                            insertUserColorTags(color);
                        }
                    },
                    error: function (error) {
                        insertUserColorTags("orange");

                        console.error(error);
                    }
                });
            }
        },
        error: function (error) {
            insertUserColorCssTag("orange");

            console.error(error);
        }
    });
}


function insertUserColorTags(color)
{
    document.head || (document.head = document.getElementsByTagName('head')[0]);

    var css = document.createElement('link');

    var icon_name = "favicon-" + color + ".ico";
    var default_icon_name = "favicon-default.ico";
    var src = locationPrefix + themes_dir + color + "/favicon/" + icon_name;
    var default_src = locationPrefix + favicon_dir + default_icon_name;

    $('#userColorId').detach();
//    css.href = locationPrefix + themes_dir + color + '/' + color + (isRetina ? "@2x" : "") + '.css';
    css.href = locationPrefix + themes_dir + color + '/' + color + '.css';
    css.id = "userColorId";
    css.className = color;
    css.rel = 'stylesheet';
    css.media = 'all';

    document.head.insertBefore(css, document.head.firstChild);

    $.get(src)
        .done(function() {
            setFavicon(src);
        })
        .fail(function() {
            setFavicon(default_src);
            console.error("No '" + color + "' color theme FAVICON has been found. Switching to defaults.");
        });
}


/**
 * https://gist.github.com/mathiasbynens/428626
 * */
function setFavicon(src)
{
    document.head || (document.head = document.getElementsByTagName('head')[0])

    var link = document.createElement('link'), oldLink = document.getElementById('dynamic-favicon');

    link.id = 'dynamic-favicon';
    link.rel = 'shortcut icon';
    link.href = src;

    if (oldLink)
    {
        document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
}


$(window).load( function gaLoad() {
    try
    {
        gaSSDSLoad("UA-54763027-1");
    }
    catch (err)
    {
        console.log(err.message);
    }
});


/**
 * http://liveweb.archive.org/web/20121005211251/http://lyncd.com/2009/03/better-google-analytics-javascript/
 * */
function gaSSDSLoad(acct)
{
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www."),
        s;
    s = document.createElement('script');
    s.src = gaJsHost + 'google-analytics.com/analytics.js';
    s.type = 'text/javascript';
    s.onloadDone = false;
    function init () {
        ga('create', acct, 'auto');
        ga('send', 'pageview');
    }
    s.onload = function () {
        s.onloadDone = true;
        init();
    };
    s.onreadystatechange = function() {
        if (('loaded' === s.readyState || 'complete' === s.readyState) && !s.onloadDone) {
            s.onloadDone = true;
            init();
        }
    };
    document.getElementsByTagName('head')[0].appendChild(s);
}

//<script>
//(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
//
//ga('create', 'UA-54763027-1', 'auto');
//ga('send', 'pageview');
//
//</script>



$(window).load( function setIPhoneIcon () {
    document.head || (document.head = document.getElementsByTagName('head')[0])

    var icon = "apple-touch-icon.png";
    var src = locationPrefix + icons_dir + icon;
    var link = document.createElement('link'), oldLink = document.getElementById('dynamic-iphicon');

    link.id = 'dynamic-iphicon';
    link.rel = 'apple-touch-icon';
    link.href = src;

    if (oldLink)
    {
        document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
});


$(function currentUserRedirection () {
    /**
     * redirects current user to his profile
     * */

    if (currentLocation.length <= 0)
    {
        var currentUser = Parse.User.current();

        if (currentUser) window.location = locationPrefix + ( currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id);
    }
});


$(function a () {
    var currentUser = Parse.User.current();
    if (currentUser)
    {
        Parse.Cloud.run( "a", { id: currentUser.id }, {
            success: function (a) {
                if (a)
                {
                    var myContactsLink = "<li><h3><a id=\"admin-panel\" href=\"" + locationPrefix + "admin\" title=\"Admin panel\">Admin Panel</a></h3></li>";
                    $(".top-profile .txt ul").append(myContactsLink);
                }
            },
            error: function (error) {
                console.error(error);
            }
        });
    }
});



$(function setTopNotificationBar() {
    /**
     * top activation notification (because of parse cookies it will be shown until user will logged out and log in again)
     * */
    var currentUser = Parse.User.current();
    if (currentUser)
    {
        if (!currentUser.get("emailVerified"))
        {
            var message = "Your account has not yet been activated, check in your emails for the activation link. After activation you will be able to add other users to your connections. ";
            var linkMessage = "Resend Activation";
            var $header = $("#header");
            var $text = $("<p></p>").html(message);
            var $a = $("<a></a>").attr({
                id: "resend-activation",
                title: "Resend Activation Link",
                class: "resend-activation-link btn"
            }).html(linkMessage);
            var $topBar = $("<div></div>").attr({
                id: "topBarEmailAccept"
            });

            $text.append($a);
            $topBar.append($text);
            $header.append($topBar);

            $("#resend-activation").on( 'click', function (click) {
                click.preventDefault();

                if (confirm ("Resend activation link to your e-mail?"))
                {
                    Parse.Cloud.run( "resendActivationLink", { id: currentUser.id }, {
                        success: function (resend) {
                            if (resend)
                                console.log("Success! Check your e-mail!");
                        },
                        error: function (error) {
                            console.log(error.message);
                        }
                    });
                }
                else
                {
                    return;
                }

            });

            $topBar.delay(7500).queue( function (next) {
                $topBar.css('height', '0px');
                next();
            });
        }
    }
});

$(function redirectToMyProfile() {
    if (isLocation("myprofile", currentLocation))
    {
        var currentUser = Parse.User.current();
        if (currentUser)
        {
            window.location = locationPrefix + (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id);
        }
        else
        {
            window.location = locationPrefixWithoutDevider;
        }
    }
});



$(document).ready(function () {

    if (!currentUser) {
        $(".top-profile").css('display', 'none');
    }

    if (currentUser)
    {
        var returned_username = currentUser.get("first_name") + " " + currentUser.get("last_name");
        var userPath = currentUser.get('unique_path') != undefined ? currentUser.get('unique_path') : currentUser.id;
        var userProfileLink = "<a href=\"" + locationPrefix + userPath + "\" title=\"Your profile page\">" + returned_username + "</a>";
        var myContactsLink = "<li><h3><a id=\"my-contacts\" href=\"" + locationPrefix + "mycontacts\" title=\"My contacts\">My Contacts</a></h3></li>";

        $("#username_current").html(userProfileLink);
        $('#edit-profile').attr({
            title: 'Edit your profile'
        });

        $(".top-profile .txt ul").append(myContactsLink);

        try
        {
            if (currentUser.get("image").length > 0) {
                var user_photo = currentUser.get("image");

                if (isLocation("edit", currentLocation))
                    $(".user_photo").attr("src", user_photo);

                $("#user_pic").attr("src", user_photo);
                $("#user_pic").attr("style", "width:41px; height:auto;");
            }
            else
            {
                $("#user_pic").attr("src", locationPrefix + backgrounds_dir + "no-result92x92.png");
            }
        }
        catch (err)
        {
            console.error(err);
            $("#user_pic").attr("src", locationPrefix + backgrounds_dir + "no-result92x92.png");
        }


        $("#edit_profile").html('Edit Profile');
        $("#invite_people").html('Invite People');

    } else {
        $(".top-profile").css("display", "none");
    }
});




function transformToAssocArray( prmstr )
{
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function getSearchParameters()
{
    var prmstr = decodeURIComponent(window.location.search.substr(1));
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

$(function activateMyProfile() {
    /**
     * when user click the activation link in email he will be redirected to activated.htm
     * */

    if (isLocation("activated", currentLocation))
    {
        var parametersArray = getSearchParameters();
        var currentUser = Parse.User.current();

        if (currentUser || parametersArray.username)
        {
            var activation = $("#user-activation-myprofile");
            var passwordField = $("#user-activation-password");
            var activationForm = $("#user-activation-form");

            var $aside = $("aside");
            var $bottomError = $("<div></div>").attr({ class: "bottom-error" });
            $aside.append($bottomError);

            passwordField.focus();

            activationForm.on( 'submit', function (submit) {
                submit.preventDefault();

                activation.click();
            });

            activation.on( 'click', function (click) {
                click.preventDefault();

                var password = passwordField.val();

                if (password != "" && password.length != 0)
                {
                    var username = parametersArray.username;

                    Parse.User.logOut();
                    Parse.User.logIn(username, password, {
                        success: function (user) {
                            unsetCookie();
                            window.location = locationPrefix + "myprofile";
                        },
                        error: function (user, error) {
                            errorBorder(passwordField);
                            userFeedback(error.message.toUpperCase() + ". Try to check your password.", false);
                        }
                    });
                }
                else
                {
                    errorBorder(passwordField);
                    userFeedback("Your password is empty", false);
                    return;
                }
            });


        }
        else
        {
            //window.location = locationPrefixWithoutDevider;
        }
    }
});


/**
 * implementing user search in header
 * */
$(function search_users() {
    if ($('#search_form').length > 0)
    {
        var form = document.getElementById("search_form");

        setIconDisabled($('#search_people'), true);

        $("#search_form").on( 'submit', function(submit) {
            submit.preventDefault();
            submit.stopPropagation();

            var searchedUser = $("#find_someone").val().trim();

            if (searchedUser.length < 3)
            {
                $('#find_someone').focus();
                return false;
            }


            if ($('#find_someone').data( 'disabled' ))
                return false;

            setIconDisabled($('#search_people'), false);

            form.q = searchedUser;
            form.submit();
        });

        $('#find_someone').on( 'keyup', function(keyup) {
            var query = $(this).val();

            if(query.length >= 3)
                setIconDisabled($('#search_people'), false);
            else
                setIconDisabled($('#search_people'), true);
        });
    }
});


function setIconDisabled($object, disabled)
{
    var filename = "zoom28x27.png";
    var disabledBackgroundCss = "transparent url('" + locationPrefix + icons_dir + filename + "') no-repeat left top";
    var disabledBackgroundSize = "24px 22px";

    if (disabled)
    {
        $object.css({
            'background': disabledBackgroundCss,
            'background-size': disabledBackgroundSize,
            'cursor': 'default'
        });
        $object.data( 'disabled', true );
    }
    else
    {
        removeInlineStyle($object, 'background');
        removeInlineStyle($object, 'background-size');
        removeInlineStyle($object, 'cursor');
        $object.data( 'disabled', false );
    }
}


$(function setHelpVideo () {
    if (isLocation("edit", currentLocation))
    {
        $(".youtube").colorbox({
            iframe: true,
            innerWidth: 640,
            innerHeight: 390
        });
    }
});


$(function setCurrentYear () {
    // returns current year for footer copyright content
    var current_year = (new Date).getFullYear();
    var footer_link = " <a href=\"" + locationPrefix + "\" title=\"howtofind.me\">howtofind.me</a>";
    var placer = "Copyright &copy; " + current_year + footer_link;

    $("footer .fixer h6").html(placer);
});

$(function twitterFacebookShare () {
    /**
     * implementing twitter and facebook functionality
     * */
    if (window.isLocation("invite", currentLocation))
    {
        var currentUser = Parse.User.current();
        var currentHostname = window.location.hostname;
        if (currentUser)
        {
            $('#tweet-share').attr('data-url', locationPrefix + (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id));
            $('#fb-share').attr('data-href', locationPrefix + (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id));
        }
        else
        {
            $('#tweet-share').attr('data-url', currentHostname);
            $('#fb-share').attr('data-url', currentHostname);
        }
        $("#tweet-share").attr('data-text', "Find me at " + currentHostname);
    }
});


window.reservedNames = [
    "admin",
    "administrator",
    "superuser",
    "superadmin",
    "superadministrator",
    "user",
    "edit",
    "connections",
    "mycontacts",
    "step2",
    "thank-you",
    "search",
    "contacts"
];


window.isLocation = function (location, pathname)
{
    if (typeof pathname == 'undefined' && pathname == undefined) pathname == currentLocation;
    if (pathname.indexOf(location) >= 0)
        return true
    else
        return false;
}

window.userFeedback = function (message, noError, timing)
{
    var _error_box = $(".bottom-error");
    if (typeof timing == 'undefined') timing = 2000;
    _error_box.html(message);

    if (noError)
        _error_box.css('color', '#6EFF66');
    else
        _error_box.css('color', '#FF6666');

    _error_box.fadeIn(250, function() {
        setTimeout( function() {
            _error_box.fadeOut(timing);
        }, timing);
    });
}

window.errorBorder = function (object, timing)
{
    if (typeof timing == 'undefined') timing = 350;
    object.css('border','1px solid #FF554B');
    object.delay(timing).queue( function (next) {
        object.css('border', '1px solid #e5e5e5');
        next();
    });
}

window.showLoading = function (isShowing, where)
{
    if (!where)
    {
        if (isShowing)
        {
            $(".animated-loading").fadeIn(250);
        }
        else
        {
            $(".animated-loading").fadeOut(250);
        }
    }
    else
    {
        if (isShowing)
        {
            $(".animated-loading." + where).fadeIn(250);
        }
        else
        {
            $(".animated-loading." + where).fadeOut(250, function (){
                if (where == 'sign-in')
                    $('#sign_in').fadeIn(150);
            });
        }
    }
};

$(window).load( function setBottomLink () {
    /**
     * setting up bottom links (costyl yopta)
     * */

    var sitemap = "Sitemap",
        privacy = "Privacy",
        contactUs = "Contact Us",
        legal = "Legal";

    var sH = "sitemap.htm",
        pH = "privacy.htm",
        cH = "contacts",
        lH = "legal.htm";

    $("footer div.fixer ul li").each(function (i, val) {
        var a = $(val).find("a");
        if (a.html() == sitemap)
        {
            a.attr( "href", locationPrefix + sH);
            a.attr( "title", sitemap);
        }

        if (a.html() == privacy)
        {
            a.attr( "href", locationPrefix + pH);
            a.attr( "title", privacy);
        }

        if (a.html() == contactUs)
        {
            a.attr( "href", locationPrefix + cH);
            a.attr( "title", contactUs);
        }

        if (a.html() == legal)
        {
            a.attr( "href", locationPrefix + lH);
            a.attr( "title", legal);
        }
    });
});

function removeInlineStyle($object, styleValue)
{
    /**
     https://stackoverflow.com/a/6553745
     * */
    var search = new RegExp(styleValue + '[^;]+;?', 'g');
    $object.attr( "style", function (i, style) {
        if (style) return style.replace(search, '');
    });
}




$(window).load( function () {
    setTimeout( function () {
        hideLoadingWrapper();
    }, 700);

    //setTabIndex();
});


function setTabIndex()
{
    $('input, select').each(
        function (tabindex) {
            if (this.type != "hidden")
                if (!this.disabled)
                    $(this).attr('tabindex', ++tabindex );
        }
    );
}