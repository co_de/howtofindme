/**
 * Created by cow-duck on 7/18/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

$( function init () {
    if (window.isLocation("mycontacts", currentLocation))
    {
        window.showLoading(true);
        $(".pagination").hide();
        $("#inner_search_result").hide();

        setContactsData();
        setSearch();
    }
});

function setContactsData()
{
    var currentUser = Parse.User.current();
    var data = {
        id: currentUser.id
    }

    Parse.Cloud.run( "getCurrentUserContactsByID", data, {
        success: function(friendsIDs) {
            /**
             * returns user id for current user connections (friends ids)
             * */
            if (friendsIDs.length > 0)
            {
                var data = {
                    array: friendsIDs
                }

                Parse.Cloud.run( "getUserObjectsByIdArray", data, {
                    success: function(userObjects) {
                        /**
                         * returns full user object (id, unique_path, name, main location, image, vip status, networks)
                         * */
                        if (typeof userObjects != 'undefined' && userObjects.length > 0 && userObjects)
                        {
                            $(".quantity").html(userObjects.length + " People");

                            var data = {
                                current: currentUser.id,
                                userArray: userObjects
                            }
                            Parse.Cloud.run( "getSharedConnectionsByUserIDsArray", data, {
                                success: function (connectionsCount) {
                                    /**
                                     * returns shared connections count to every user in friends section
                                     * */

                                    appendData(userObjects, connectionsCount);
                                },
                                error: function (error) {
                                    console.error(error);
                                }
                            });
                        }
                    },
                    error: function(error) {
                        console.error(error);
                    }
                });
            }
            else
            {
                window.showLoading(false);
                $(".pagination").fadeOut(250, function() {
                    $(".search-result").fadeOut(250, function() {
                        $("#search-no-results").fadeIn(250);
                    });
                });
                console.error("Sorry! Something went wrong or something like that. Maybe data is empty?");
            }
        },
        error: function(error) {
            window.showLoading(false);
            $(".pagination").fadeOut(250, function() {
                $(".search-result").fadeOut(250, function() {
                    $("#search-no-results").fadeIn(250);
                });
            });
            console.error(error);
        }
    });
}

function getNetworkByURL(url)
{
    /**
     * returns the string contains only network name
     * */
    var result;
    var slash = "/"
    var dSlash = "//";
    var www = "www.";
    var domens = ["com", "ru", "by", "net"]; /** could be scalable */
    var indexSlash = url.indexOf(dSlash);
    if (indexSlash >= 0)
    {
        result = url.substr(indexSlash + dSlash.length);
        var indexWWW = result.indexOf(www);
        if (indexWWW >= 0)
        {
            result = result.substr(indexWWW + www.length);
        }

        var indexSlash = result.indexOf(slash);
        if (indexSlash >= 0)
        {
            result = result.substr(0, indexSlash);
        }
        for (var i in domens)
        {
            if (result.indexOf("." + domens[i]) >= 0)
            {
                result = result.substr(0, result.indexOf("." + domens[i]));
            }
        }
    }
    return result;
}

function setSearch()
{
    /**
     * setting up events
     * */
    $("#search-form-query").on( 'submit', function (submit) {
        submit.preventDefault();
    });

    var nodes = document.getElementById("inner_search_result").childNodes;
    if (nodes && nodes.length > 0)
    {
        $("#name_input").on( 'keyup', function(keyup) {
            searchByInputHandler(nodes, $(this).val(), 'byName');
        });

        $("#city_input").on( 'keyup', function (keyup) {
            searchByInputHandler(nodes, $(this).val(), 'byCity');
        });

        $("#network_input").on( 'keyup', function (keyup) {
            searchByInputHandler(nodes, $(this).val(), 'byNetwork');
        });

    }
}

function searchByInputHandler(nodes, value, type)
{
    /**
     * providing search functionality
     * */
    var activeNodesCount = 0;
    var disabledNodesCount = 0;

    /**
     * every node contains full user block with info
     * */
    for (var i in nodes)
    {
        var node = nodes[i];
        if (node.className)
        {
            if (node.className.indexOf("result-item") >= 0)
            {
                activeNodesCount++;
                if (value.length <= 2)
                {
                    $(node).show();
                }
                else
                {

                    var searchString = "";

                    /**
                     * search types
                     * */
                    if (type == 'byName')
                        searchString = $(node).find(".txt .heading a").html();

                    if (type == 'byCity')
                        searchString = $(node).find(".txt .title").html();

                    if (type == 'byNetwork')
                        searchString = $(node).find(".txt .hidden-networks").html();

                    console.log(searchString.toLowerCase());
                    console.log(value.toLowerCase().trim());

                    if (searchString.toLowerCase().indexOf(value.toLowerCase().trim()) < 0)
                    {
                        $(node).hide();
                        disabledNodesCount++;
                    }
                    else
                    {
                        $(node).show();
                    }
                }
            }
        }
    }

    /**
     * when search value does not match data results we return all nodes to their default status (visible) and do animation
     * */
    if (disabledNodesCount == activeNodesCount)
    {
        if (type == 'byName')
        {
            window.errorBorder($("#name_input"), 350);
        }

        if (type == 'byCity')
        {
            window.errorBorder($("#city_input"), 350);
        }

        if (type == 'byCountry')
        {
            window.errorBorder($("#country_input"), 350);
        }

        if (type == 'byNetwork')
        {
            window.errorBorder($("#network_input"), 350);
        }

        for (var i in nodes)
        {
            var node = nodes[i];
            if (node.className)
            {
                if (node.className.indexOf("result-item") >= 0)
                    $(node).show();
            }
        }
    }
}

function appendData(userObjects, connectionsCount)
{
    for (var i in userObjects)
    {
        var userId = userObjects[i].id;
        var userPath = (userObjects[i].url == undefined ? userId : userObjects[i].url );
        var userHref = locationPrefix + userPath;
        var userName = userObjects[i].username;
        var userMainLocation = userObjects[i].mainloc;
        var userImg = ( userObjects[i].img.length > 0 ?
                        userObjects[i].img : locationPrefix + backgrounds_dir + "no-result92x92.png" );
        var sharedConnectionHref = locationPrefix + "connections?u=" + (userPath == undefined ? userId : userPath );
        var vip = userObjects[i].isVip;

        var conCount = connectionsCount[i];
        var innerHtml = (typeof conCount != 'undefined' ? conCount : 0) + " Shared Connection" + (conCount == 1 ? "" : "s");

        /**
         * setting up hidden networks block for providing search by networks
         * */
        var userNetworksArray = ""
        var networks = userObjects[i].nets;

        for (var j in networks)
        {
            var network = networks[j];
            if (network && network.length > 0)
            {
                network = getNetworkByURL(network);
                if (typeof network != 'undefined')
                {
                    userNetworksArray += (userNetworksArray.length <= 0 ? "" : " ") + network;
                }
            }
        }

        var apStr =
            "<div class='result-item result-item-active user_" + userId + "' id='other_user'>" +
                "<span class='pic'>" +
                "<a href=" + userHref + " title=''>" +
                "<img class='user_photo' src='" + userImg + "'/>" +
                "</a>" +
                ( vip ? "<span class=\"vip\"></span>" : "" ) +
                "</span>" +
                "<span class='txt'>" +
                "<span class='heading'>" +
                "<a href=" + userHref + " title=''>" + userName + "</a>" +
                "</span>" +
                "<span class='title'>" + userMainLocation + "</span>" +
                "<div class=\"hidden-networks\" style=\"display: none\">" + userNetworksArray + "</div>" +
                "<span class='share-yellow'>" +
                "<a class=\"sc-link-zero-connections shared-connections-count_" + userId + "\" href=" + sharedConnectionHref + " title=\"" + innerHtml + "\">" + innerHtml + "</a>" +
                "</span>" +
                "</span>" +
            "</div>";

        $("#inner_search_result").append(apStr);

        var currentUserBlock = $(".shared-connections-count_" + userId);

        /**
         * if user found himself in results
         * */
        if (userId == Parse.User.current().id)
        {
            innerHtml = "It's you!";
            currentUserBlock.attr("href", "#");
            currentUserBlock.attr("title", "");
            currentUserBlock.on('click', function (click) {
                click.preventDefault();
            });
            $(".user" + userId + " .share-yellow").css( "background", "none");
            currentUserBlock.html(innerHtml);
        }
        else
        {
            /**
             * setting up the hover effects for shared connections links
             * */
            if (conCount > 0 && typeof conCount != 'undefined')
            {
                currentUserBlock.removeClass( "sc-link-zero-connections" );
                currentUserBlock.addClass( "sc-link-NOzero-connection" );
            }
            else
            {
                currentUserBlock.attr("href", "#");
                currentUserBlock.attr("title", "");
                currentUserBlock.on('click', function (click) {
                    click.preventDefault();
                });
                $(".user" + userId + " .share-yellow").addClass("share-dark");
                $(".user" + userId + " .share-dark").removeClass("share-yellow");
                currentUserBlock.css("color", "#cbcbcb");
            }
        }

        window.showLoading(false);
        $("#inner_search_result").fadeIn(300);
    }
}
