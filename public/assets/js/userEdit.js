/**
 * Created by cow-duck on 10/21/14.
 */

var socialNetworksArray = [
    "be", "digg", "facebook", "ff", "flickr", "google", "green",
    "linkedin", "red", "reddit", "twickr", "twitter",
    "vimeo", "youtube", "lastfm"
];

$(function init(){
    if (isLocation("edit", currentLocation) && currentUser)
    {
        settingUpEditSection();
    }
});


function settingUpEditSection()
{
    /**
     * INFO
     * */

    setSwapButtons();

    var returned_username = currentUser.get("first_name") + " " + currentUser.get("last_name");

    $("#edit_profile").css("display", "none");
    $("#username_edit").html(returned_username);

    /**
     * trying to get user's image section
     * */
    try
    {
        if (currentUser.get("image").length > 0)
        {
            setImageSection(currentUser.get("image"));
        }
    }
    catch (err)
    {
        console.error("Error while trying to get user's image");
        console.error(err);
    }


    /**
     * trying to get user's intro section
     * */
    try
    {
        if (currentUser.get("intro").length >= 0)
        {
            setIntroSection(currentUser.get("intro"));
        }
    }
    catch (err)
    {
        console.error("Error while trying to get user's intro");
        console.error(err);
    }

    /**
     * trying to get user's selected main location
     * */
    try
    {
        if (currentUser.get("main_location").length > 0)
        {
            setMainLocationSection((currentUser.get("main_location")));
        }
    }
    catch (err)
    {
        console.error("Error while trying to get user's main location");
        console.error(err.message);
    }


    /**
     * trying to get user's selected locations
     * */
    try
    {
        if (currentUser.get("locations").length > 0)
        {
            setLocationsSection(currentUser.get("locations"));
        }
    }
    catch (err)
    {
        console.error("Error while trying to get user's locations");
        console.error(err.message);
    }

    setFinishedLink((typeof currentUser.get("unique_path") == 'undefined' ? currentUser.id : currentUser.get("unique_path")));
    setThemesSection();
    setColorThemePins();
    setDragToPhoto();
    setCropCircle();
    setUserColorSelection(currentUser);

    $("#save_resized").on("click", function (click) {
        click.preventDefault();

        saveImage();
    });

    $("#my_intro").on( 'keydown', function (keydown) {

        setIntro(keydown);
    });


    $("#intro").on("click", function (click) {
        click.preventDefault();

        saveIntro();
    });



    /**
     * NETWORKS
     * */

    window.socials = new Array("youtube", "linkedin", "be", "google", "reddit", "facebook", "twitter", "flickr");
    var all = $(".all-netw");

    /**
     * hides the panel with 'show all' and 'search results' functions if input field is empty. keypress event implemented
     * */
    $("#netw-search").on('keypress', function (keypress) {
        if ($(this).val() == '' || $(this).val().length <= 0)
        {
            $(".search-results").hide(250);
        }
    });

    /**
     * hide the panel then initialize
     * */
    $(".search-results").hide();


    /**
     * search function
     * */
    $("#search_netw").on("click", function (click) {
        click.preventDefault();

        searchNetworks($("#netw-search").val().trim(), all);
    });

    /**
     * show all function
     * */
    $(".show-all-netw").on("click", function (click) {
        click.preventDefault();

        $(".social").children().hide();
        $(".social").append(all);
        all.show();
    });

    /**
     * show search results if input field is not empty. if empty - shows all networks
     * */
    $(".show-search").on("click", function (click) {
        click.preventDefault();

        searchNetworks($("#netw-search").val(), all);
    });



    /**
     * LOCATION
     * */

    $('form#add-location-form').on( 'submit', function(submit) {
        submit.preventDefault();

        $("#add_location").click();
    });

    $("#add_location").on("click", function (click) {
        click.preventDefault();

        addLocation();
    });

    setDeleteLocationButton($("a.delete_country"));

    $("#save_location").live("click", function (e) {
        e.preventDefault();

        saveLocation();
    });
}

var userLocations = new Array();

function getUserLocations()
{
    return userLocations;
}

function setUserLocations(element)
{
    userLocations.push(element);
}

function unsetUserLocations(first, last)
{
    if (last)
    {
        userLocations.splice(getUserLocations().indexOf(first));
    }
    else
    {
        userLocations.splice(getUserLocations().indexOf(first), getUserLocations().indexOf(first) + 1);
    }
}

function setDeleteLocationButton(button)
{
    if (button.length > 0) {
        button.on("click", function (click) {
            click.preventDefault();

            var removed_loc = $(this).prev().find("p.added_locations").html();
            var userLocations = getUserLocations();

            if (userLocations.indexOf(removed_loc) == userLocations.length - 1)
            {
                unsetUserLocations(removed_loc, true);
            }
            else
            {
                unsetUserLocations(removed_loc, false);
            }

            $(this).parents('li.loc_li').remove();
        });
    }
}

function saveLocation()
{
    if ($("#selected_locations").children().length > 0)
    {
        var currentUser = Parse.User.current();
        var main_location = $(".set_loc:checked").closest('div').find("p.added_locations").text();
        var all_locations = getUserLocations();
        if (currentUser) {
            //updating the user profile with the edited information
            currentUser.set("first_name", currentUser.attributes.first_name.toLowerCase());
            currentUser.set("last_name", currentUser.attributes.last_name.toLowerCase());
            currentUser.set("email", currentUser.attributes.email);
            currentUser.set("password", currentUser.attributes.password);
            currentUser.set("username", currentUser.attributes.email);
            currentUser.set("image", currentUser.attributes.image);
            currentUser.set("intro", currentUser.attributes.intro);
            currentUser.set("locations", all_locations);
            currentUser.set("main_location", main_location);
            currentUser.set("theme_color", currentUser.attributes.theme_color);
            currentUser.set("vip", currentUser.attributes.vip);
            currentUser.set("unique_path", currentUser.attributes.unique_path);
            currentUser.set("cookieAccepted", currentUser.get("cookieAccepted"));
            currentUser.save( null, {
                success: function () {
                    $("#user-register-default-check-location-ok").addClass('checked');
                    $("#user-register-default-check-location-ok").removeClass('empty');
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
            console.log("current user info was not updated");
        }
        $('#location-details').dialog('close');
    }
    else
    {
        window.errorBorder($("#location_input"), 1500);
    }
}

function addLocation()
{
    var choosed_location = $("#location_input").val();
    var new_location = null;

    if (choosed_location != '' && choosed_location != 'undefined')
    {
        new_location = choosed_location;
    }

    if (new_location && typeof new_location != 'undefined')
    {
        if (getUserLocations().indexOf(new_location) < 0)
        {
            $("#selected_locations").children().detach();
            setUserLocations(new_location);

            var all_locations = getUserLocations();

            for (var i in all_locations)
            {
                var location = all_locations[i];
                var lCity = location.split(",")[0].trim();
                $("#selected_locations").append("" +
                    "<li class='loc_li'>" +
                    "<div class='column it" + i + "'>" +
                    "   <p class='added_locations'>" + location + "</p>" +
                    "</div>" +
                    "<a href='#' title='' class='btn-delete delete_country'></a>" +
                    "</li>");

                var $input = $("<input />").attr({ type: 'radio', name: 'town', id: lCity }).addClass("set_loc");

                try
                {
                    if (currentUser.get("main_location").length > 0)
                    {
                        if ((currentUser).get("main_location") == location)
                        {
                            $input.attr({ checked: 'checked' });
                        }
                    }
                }
                catch(err)
                {
                    console.error(err.message);
                }

                var $label = $("<label />").attr({ for: lCity });
                $(".it" + i).append($input);
                $(".it" + i).append($label);

                setDeleteLocationButton($("a.delete_country"));
            }
        }
        else
        {
            var $addedLocations = $("p.added_locations");

            $.each($addedLocations, function(key,val) {
                var $loc = $(val);

                if ($loc.html() == new_location)
                {
                    var object = $loc.parent();
                    object.css('border','1px solid #FF554B');
                    object.delay(500).queue( function (next) {
                        object.css('border', '1px solid #f6f6f6');
                        next();
                    });
                }
            });
        }
        $('#add_current_location').html("Current Location");
        $("#location_input").val("");
    }
    else
    {
        if (choosed_location == '')
        {
            $("#location_input").css('border','1px solid #FF554B');
            $("#location_input").delay(1500).queue( function (next) {
                $("#location_input").css('border', '1px solid #e5e5e5');
                next();
            });
        }
        return false;
    }
}

function setSwapButtons()
{
    var $networksButton = $('#netw_selection');
    var $infoButton = $('#about_selection');
    var hided_class = "hided-elem";
    var active_button_class = "btn-about-active";

    $networksButton.on( 'click', function(click) {
        click.preventDefault();

        $infoButton.removeClass(active_button_class);
        $(this).addClass(active_button_class);

        $('.user-netw').removeClass(hided_class);
        $('.user-acc').addClass(hided_class);

        $('#save-div').removeClass(hided_class);
        $('#finish-div').addClass(hided_class);
    });

    $infoButton.on( 'click', function(click) {
        click.preventDefault();

        $networksButton.removeClass(active_button_class);
        $(this).addClass(active_button_class);

        $('.user-acc').removeClass(hided_class);
        $('.user-netw').addClass(hided_class);

        $('#finish-div').removeClass(hided_class);
        $('#save-div').addClass(hided_class);
    });
}

function setDragToPhoto()
{
    /**
     * editing user profile. adding a draggable/clickable profile photo
     * */

    var obj = $('body');

    $('#upload-box').on('click', function (click) {
        click.preventDefault();
        $(this).fadeOut(250);
    });

    obj.on({
        dragenter: function (dragenter) {
            dragenter.stopPropagation();
            dragenter.preventDefault();
        },
        dragleave: function (dragleave) {
            dragleave.preventDefault();
            dragleave.stopPropagation();
        },
        dragover: function (dragover) {
            dragover.stopPropagation();
            dragover.preventDefault();

            var dt = dragover.originalEvent.dataTransfer;
            // check if we drag a file or an current page object

            if(dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('application/x-moz-file')))
            {
                $('#upload-box').fadeIn(250);
            }
        },
        drop: function (drop) {
            drop.preventDefault();
            drop.stopPropagation();

            $('#upload-box').fadeOut(250);

            var dt;

            try
            {
                dt = drop.originalEvent.dataTransfer;
            }
            catch (err)
            {
                console.log(err.message);
            }

            //We need to send dropped files to Server
            if(dt && dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('application/x-moz-file')))
            {
                handleFileUpload(dt.files);
            }
            else return;
        }
    });

    $(':file#select-file').change(function (e) {
        var file_tmp = this.files;

        //We need to send dropped files to Server
        if (file_tmp) handleFileUpload(file_tmp);
        else return;
    });

    $(':file#fileopen').change(function (e) {
        var file_tmp;

        try
        {
            file_tmp = this.files;
        }
        catch (err)
        {
            console.log(err.message);

        }
        $('#upload').click(function (e) {
            e.preventDefault();

            //We need to send dropped files to Server
            if (file_tmp)
            {
                handleFileUpload(file_tmp);
                setChecked($("#user-register-default-check-image-ok"));
            }
            else
            {
                console.log("Something broke while processing: " + file_tmp + ". " + e);
                return;
            }
        });
    });
}

function setCropCircle()
{
    /**
     * resizing the image with jquery ui
     * */

    var photo = document.getElementById('usr_pic_crop');
    var circle = document.getElementById('crop_circle');
    var minWidth = 60;
    var maxWidth = 280;

    window.coordstop = '0';
    window.coordsleft = '0';
    window.newWidth = '60';

    $("#crop_circle").draggable({
        drag: function (event, ui) {
            var dragposition = ui.position;
            window.coordstop = dragposition.top;
            window.coordsleft = dragposition.left;
        },
        containment: '#usr_pic_crop',
        cursor: 'move',
        stack: '#usr_pic_crop',
        refreshPositions: true
    });

    $("#slider").slider({
        value: 60,
        min: minWidth,
        max: maxWidth,
        step: 10,
        slide: function (event, ui) {

            var circleW = ui.value;
            var circleH = circleW;

            $("#crop_circle").css("width", circleW  + "px");
            $("#crop_circle").css("height", circleH + "px");

            if (circle.offsetTop + circle.offsetHeight > photo.offsetHeight)
            {
                $("#crop_circle").css( 'top', (photo.offsetHeight - circle.offsetHeight) + "px" );
            }
            if (circle.offsetLeft + circle.offsetWidth > photo.offsetWidth)
            {
                $("#crop_circle").css( 'left', (photo.offsetWidth - circle.offsetWidth) + "px" );
            }

            window.newWidth = circleW;
            window.coordsright = window.coordsleft + window.newWidth;
            window.coordsbot = window.coordstop + window.newWidth;
        }
    });
}

function setFinishedLink(user_path)
{
    $("#finished-editing").attr( "href", locationPrefix + user_path);
}

function setThemesSection()
{
    /**
     * check if user select a color-theme
     * */

    Parse.Cloud.run( "getUserColor", { id: currentUser.id }, {
        success: function (color) {
            if (color && color.length > 0)
            {
                setChecked($("#user-register-default-check-color-ok"));
            }
        },
        error: function (error) {
            console.error(error);
        }
    });
}

function setLocationsSection(user_locations)
{
    for (var i in user_locations)
    {
        var location = user_locations[i];
        if (location)
        {
            try
            {
                var city = location.split(", ")[0];

                $("#selected_locations").append(
                    "<li class='loc_li'>" +
                        "<div class='column'>" +
                        "<p class='added_locations'>" + location + "</p>" +
                        "<input type='radio' id='" + city + "' class='set_loc' " +
                        ((currentUser).get("main_location").length > 0 ? ((currentUser).get("main_location") == location ? "checked" : "") : "") +
                        " name='town'/>" +
                        "<label for='" + city + "'></label>" +
                        "</div>" +
                        "<a href='#' title='' class='btn-delete delete_country'></a>" +
                        "</li>");

                setUserLocations(location);
                setDeleteLocationButton($("a.delete_country"));
            }
            catch (err)
            {
                console.error("Error while trying to splitting user's location");
                console.error(err);
            }
        }
    }
    setChecked($("#user-register-default-check-location-ok"));
}

function setIntroSection(intro_str)
{
    var limit = 200;
    var counter = " / " + limit.toString();

    $("#intro_add").html(intro_str);
    $("#my_intro").html(intro_str);
    $("#my_intro").val(intro_str);
    $("#letters-counter").html( (intro_str.length >= limit ? intro_str.length : limit - intro_str.length ) + counter );

    setIntroLengthControl(intro_str, limit, $("#letters-counter"));
    setChecked($("#user-register-default-check-intro-ok"));

    $('#clear-intro').on( 'click', function (click) {
        click.preventDefault();

        $('textarea#my_intro').html('');
        $('textarea#my_intro').val('');

        $("#letters-counter").html( ($('textarea#my_intro').val().length >= limit ? $('textarea#my_intro').val().length : limit - $('textarea#my_intro').val().length ) + counter );
        setIntroLengthControl($('textarea#my_intro').val(), limit, $("#letters-counter"));

        showLoading(true);
        saveIntro();
    });
}

function setImageSection(image_src)
{
    $("#user-register-default-check-image-ok").addClass('checked');
    $("#user-register-default-check-image-ok").removeClass('empty');

    $("#usr_pic_crop").attr("src", image_src);
    $("#usr_pic_crop").data( 'loaded', true );
    $('#save_resized').removeClass('disabled-button');
    $('#save_resized').removeAttr('disabled');

    $(".user_photo").on("load", function () {
        image_scale($(this));
    });
}

function setMainLocationSection(user_location)
{
    $("#main_loc").html(user_location);
}

function saveColor(id)
{
    Parse.Cloud.run( "getUserColor", { id: currentUser.id }, {
        success: function (uscolor) {
            if (uscolor && uscolor.length > 0)
            {
                var payload = {
                    id: currentUser.id,
                    color: id,
                    update: true
                };
                Parse.Cloud.run( "saveUserColorTheme", payload, {
                    success: function (output) {
                        if (output)
                        {
                            $('#customise-profile').dialog('close');
                            window.location.reload();
                            //$("#user-register-default-check-color-ok").css('background', 'url("' + locationPrefix + 'assets/img/tick22x17.png") no-repeat scroll 0 -17px rgba(0, 0, 0, 0)');
                        }
                        else
                        {
                            $('#customise-profile').dialog('close');
                            console.error("Error while updating user color-theme");
                        }
                    },
                    error: function (error) {
                        $('#customise-profile').dialog('close');
                        console.error(error);
                    }
                });
            }
            else
            {
                var payload = {
                    id: currentUser.id,
                    color: id,
                    update: false
                };
                Parse.Cloud.run( "saveUserColorTheme", payload, {
                    success: function (output) {
                        if (output)
                        {
                            $('#customise-profile').dialog('close');
                            window.location.reload();
                        }
                        else
                        {
                            $('#customise-profile').dialog('close');
                            console.error("Error while saving user color-theme");
                        }
                    },
                    error: function (error) {
                        $('#customise-profile').dialog('close');
                        console.error(error);
                    }
                });
            }
        },
        error: function (error) {
            $('#customise-profile').dialog('close');
            console.error(error);
        }
    });
}

function searchNetworks (search, all)
{
    // provides search through all networks. input params - value from input field and array of all networks
    if (search == '' || search.length <= 0)
    {
        $(".social").children().detach();
        $(".search-results").hide(250);
        $(".social").append(all);
        all.show();
        return false;
    }
    else
    {
        // cleaning the previous search results
        $(".social .netw-found").detach();

        $.each(window.socials, function (i, val) {
            if (val.indexOf(search) != -1) {
                console.log(all);
                all.hide();
                $(".search-results").css("display", "block");
                $(".social").append("<div class='netw-found'><a class='" + val + "'></a><div class='netw-name'>" + val + "</div><div class='add-to-netw'><a class='add-to-netw'><span>+</span>Add to Networks</a></div></div>");
            }
        });

        // if nothing found show all networks
        if ($(".social").children().length <= 0)
        {
            $(".social").append(all);
            all.show();
            $(".search-results").hide(250);
        }
        else
        {
            // show results and panel with functions
            $(".search-results").show(250);
        }

        // appending found network to the username selection field
        $.each($(".add-to-netw a"), function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                var found_class = $(this).parents(".netw-found").find("a").attr("class");
                var site_ref = $(".all-netw ." + found_class + "-link").attr("href");
                $('.network-selection').dialog('close');

                //appendUserNetworkInput(found_class, site_ref, all);

                return false;
            });

        });
    }
}

function setUserColorSelection(currentUser)
{
    Parse.Cloud.run( "getUserColor", { id: currentUser.id }, {
        success: function (userColor) {
            var colorsArray = $('.customize-profile div.list ul li a');

            if (userColor && userColor.length > 0)
            {
                $.each(colorsArray, function(key, value) {
                    var $a = $(this);

                    if ($a.hasClass(userColor[0].get("userColor").get("name") + '_color'))
                    {
                        $a.addClass('active');
                    }
                });
            }
            else
            {
                $('#customise-profile .customize-profile a.save').addClass('disabled-button');
            }

            if ($('#customise-profile .customize-profile a.save').length > 0)
            {
                $('#customise-profile .customize-profile a.save').on( 'click', function(click) {
                    click.preventDefault();

                    if (!$(this).hasClass('disabled-button'))
                    {
//                        var filename = "ajax-squares-loader.gif";
//                        var filepath = locationPrefix + controls_dir;
//
//                        var $div = $('<div></div>').attr({
//                            class: "loading-squares"
//                        });
//                        var $img = $('<img />').attr({
//                            src: filepath + filename
//                        });
//
//                        $div.append($img);
//                        $(this).addClass('loading-squares-button');
//                        $(this).html("");
//                        $(this).append($div);

                        $(this).addClass('disabled-button');
                        $('.customize-profile .animated-loading').fadeIn(250);

                        $.each(colorsArray, function(key, value) {
                            var $a = $(this);
                            if ($a.hasClass('active'))
                            {
                                saveColor($a.data( 'colorId' ));
                            }
                        });
                    }
                });
            }
        },
        error: function (error) {
            console.error(error);
        }
    });
}

function setColorThemePins()
{
    /**
     * dynamically loads the available colors from db
     * */

    Parse.Cloud.run( "getAvailableUserColors", {}, {
        success: function(colors) {
            if (colors.length > 0)
            {
                var $colorProfileDialog = $('#customise-profile .customize-profile');
                var $list = $('<div></div>').attr({
                    class: 'list'
                });
                var $ul = $('<ul></ul>');

                for (var i in colors)
                {
                    var color = colors[i];
                    var name = color.get("name");
                    var title = name + ' color';
                    var className = name + '_color';
                    var $li = $('<li></li>');
                    var $a = $('<a></a>')
                        .attr({
                            href: '#',
                            title: title,
                            class: className + (!color.active ? "" : " unactive") + (name == $('#userColorId').attr('class') ? " active" : "")
                        })
                        .data( 'colorId', color.id)
                        .on( 'click', function (click) {
                            click.preventDefault();

                            $colorProfileDialog.find('div.list ul li a').removeClass('active');
                            $(this).addClass('active');
                            $('#customise-profile .customize-profile a.save').removeClass('disabled-button');
                        });
//                    var filename = 'htf-pin-' + name + (isRetina ? "@2x" : "") + '.png';
                    var filename = 'htf-pin-' + name + '.png';
                    var $img = $('<img />').attr({
                        src: locationPrefix + pins_dir + filename,
                        alt: title
                    });

                    $a.append($img);
                    $li.append($a);
                    $ul.append($li);
                }

                $list.append($ul);
                $colorProfileDialog.prepend($list);
            }
        },
        error: function (error) {
            console.error(error);
        }
    });
}

var savingTimeout;
function setIntro(keydown)
{
    /**
     * http://jsfiddle.net/3uhNP/1/
     * */
//    if (keypress.which < 0x20)
//    {
//        // e.which < 0x20, then it's not a printable character
//        // e.which === 0 - Not a character
//        return;     // Do nothing
//    }

    var intro_str = $("#my_intro").val();
    var limit = 200;
    var counter = " / " + limit.toString();

    if (intro_str.length >= limit)
    {
        $("#letters-counter").html(intro_str.length + counter);
        $("#my_intro").css('border', '1px solid #FF554B');
        $("#my_intro").delay(1500).queue( function (next) {
            $("#my_intro").css('border', '1px solid #cdcdcd');
            next();
        });
        $("#my_intro").val(intro_str.substr(0, limit-1));
    }
    else
    {
        $("#letters-counter").html(limit - intro_str.length + counter);
    }

    setIntroLengthControl(intro_str, limit, $("#letters-counter"));

    clearTimeout(savingTimeout);
    savingTimeout = setTimeout(function () {
        showLoading(true);
        saveIntro();
    }, 2000);
}

function saveIntro()
{
    var intro = $("#my_intro").val();

    if (currentUser) {
        if (intro.length <= 200)
        {
            currentUser.set("first_name", currentUser.attributes.first_name.toLowerCase());
            currentUser.set("last_name", currentUser.attributes.last_name.toLowerCase());
            currentUser.set("email", currentUser.attributes.email);
            currentUser.set("password", currentUser.attributes.password);
            currentUser.set("username", currentUser.attributes.email);
            currentUser.set("image", currentUser.attributes.image);
            currentUser.set("intro", intro);
            currentUser.set("locations", currentUser.attributes.locations);
            currentUser.set("main_location", currentUser.attributes.main_location);
            currentUser.set("theme_color", currentUser.attributes.theme_color);
            currentUser.set("vip", currentUser.attributes.vip);
            currentUser.set("unique_path", currentUser.attributes.unique_path);
            currentUser.set("cookieAccepted", currentUser.get("cookieAccepted"));
            currentUser.save(null, {
                success: function (currentUser) {
                    if (currentUser)
                    {
                        showLoading(false);
                    }
                },
                error: function (currentUser, error) {
                    showLoading(false);
                    console.error(error);
                }
            });
            $("#user-register-default-check-intro-ok").addClass('checked');
            $("#user-register-default-check-intro-ok").removeClass('empty');
        }
        else
        {
            showLoading(false);
            $("#my_intro").css('border', '1px solid #FF554B');
            $("#my_intro").delay(1500).queue( function (next) {
                $("#my_intro").css('border', '1px solid #cdcdcd');
                next();
            });
            return;
        }
    } else {
        showLoading(false);
        console.log("current user info was not updated");
    }
//    $('#personal-statement').dialog('close');
}

function saveImage()
{
    /**
     * crop the user profile image in the modal window using Parse image module
     * */

    if (!$('#usr_pic_crop').data( 'loaded' ))
    {
        console.error("No image file has been selected.");
        return;
    }
    else
    {

        var widthMax = $('#usr_pic_crop').width();
        var image_src = $('#usr_pic_crop').attr('src');

        var $circle = $('#crop_circle');
        var $newWidth = $circle.width();
        var $newHeight = $circle.height();
        var $newTop = parseInt($circle.css('top'));
        var $newLeft = parseInt($circle.css('left'));

        var uploadedImageObject = new Image();
        uploadedImageObject.src = image_src;

        $(uploadedImageObject).load(function () {

//            var newWidth = window.newWidth;
//            var setWidth = newWidth;
//            var setHeight = newWidth;
//            var setTop = window.coordstop;
//            var setLeft = window.coordsleft;
//                "width": origWidth * setWidth,
//                "height": origWidth * setWidth,
//                "curUs": curUs,
//                "top": top,
//                "left": left
//            var left = origWidth * setLeft;
//            var top = origHeight * setTop;

            var widthRatio = uploadedImageObject.width / widthMax;
            var heightRatio = uploadedImageObject.height / widthMax;

            var width = $newWidth * widthRatio;
            var height = $newHeight * heightRatio;
            var left = $newLeft * widthRatio;
            var top = $newTop * heightRatio;

            console.log(widthMax);
            console.log(uploadedImageObject.width);
            console.log(uploadedImageObject.height);


            var url = this.src;

            var data ={
                url: url,
                width: width,
                height: height,
                top: top,
                left: left
            };
            console.log(data);
            Parse.Cloud.run( "cropper", data, {
                success: function (url) {
                    if (url.length > 0)
                    {
                        $(".user_photo").attr("src", url);
                        $("#user_pic").attr("src",url);
                        $("#user_pic").attr("style", "width:41px; height:auto;");
                        $("#usr_pic_crop").attr("src", url);

                        image_scale($(".user_photo"));

                        if (currentUser)
                        {
                            //updating the user profile with the edited information
                            currentUser.set("image", url);
                            currentUser.set("first_name", currentUser.attributes.first_name.toLowerCase());
                            currentUser.set("last_name", currentUser.attributes.last_name.toLowerCase());
                            currentUser.set("email", currentUser.attributes.email);
                            currentUser.set("password", currentUser.attributes.password);
                            currentUser.set("username", currentUser.attributes.email);
                            currentUser.set("intro", currentUser.attributes.intro);
                            currentUser.set("locations", currentUser.attributes.locations);
                            currentUser.set("main_location", currentUser.attributes.main_location);
                            currentUser.set("netw", currentUser.attributes.netw);
                            currentUser.set("fav_netw", currentUser.attributes.fav_netw);
                            currentUser.set("theme_color", currentUser.attributes.theme_color);
                            currentUser.set("vip", currentUser.attributes.vip);
                            currentUser.set("unique_path", currentUser.attributes.unique_path);
                            currentUser.save();
                        }
                        else
                        {
                            console.log("current user info was not updated");
                        }

                        $('#profile-image').dialog('close');
                    }
                },
                error: function (error) {
                    console.error(error);
                }
            });
        });
    }
}

function image_scale($user_photo_object)
{
    /**
     * scale the user profile photo
     * */

    $user_photo_object.each(function () {
        var maxWidth = 115; // Max width for the image
        var maxHeight = 115;    // Max height for the image
        var ratio = 0;  // Used for aspect ratio
        var width = $(this).width();    // Current image width
        var height = $(this).height();  // Current image height

        // Check if the current width is larger than the max
        if (width > maxWidth)
        {
            ratio = maxWidth / width;   // get ratio for scaling image
            $(this).css("width", maxWidth); // Set new width
            $(this).css("height", height * ratio);  // Scale height based on ratio
            height = height * ratio;    // Reset height to match scaled image
        }

        width = $(this).width();    // Current image width
        height = $(this).height();  // Current image height

        // Check if current height is larger than max
        if (height > maxHeight) {
            ratio = maxHeight / height; // get ratio for scaling image
            $(this).css("height", maxHeight);   // Set new height
            $(this).css("width", width * ratio);    // Scale width based on ratio
            width = width * ratio;    // Reset width to match scaled image
        }
    });
}

function handleFileUpload(file_tmp) {
    /**
     * uploading the image to Parse cloud and adding the image to the user object
     * */

    var file;

    if (file_tmp)
        file = readURL(file_tmp[0]);

    if (file)
    {
        var userPhoto = new Parse.File("photo.png", file);
        userPhoto.save().then(function () {
            $("#user_photo").attr("src", userPhoto._url);
            $("#user_pic").attr("src", userPhoto._url);
            $("#user_pic").attr("style", "width:41px; height:auto;");
            $("#usr_pic_crop").attr("src", userPhoto._url);
            $("#usr_pic_crop").data( 'loaded', true );

            $('#profile-image').dialog('open');
            $('#upload').removeAttr('disabled');
            $('#upload').removeClass('disabled-button');

            if (currentUser) {
                //updating the user profile with the edited information
                currentUser.set("image", userPhoto._url);
                currentUser.set("first_name", currentUser.attributes.first_name.toLowerCase());
                currentUser.set("last_name", currentUser.attributes.last_name.toLowerCase());
                currentUser.set("email", currentUser.attributes.email);
                currentUser.set("password", currentUser.attributes.password);
                currentUser.set("username", currentUser.attributes.email);
                currentUser.set("intro", currentUser.attributes.intro);
                currentUser.set("locations", currentUser.attributes.locations);
                currentUser.set("main_location", currentUser.attributes.main_location);
                currentUser.set("theme_color", currentUser.attributes.theme_color);
                currentUser.set("vip", currentUser.attributes.vip);
                currentUser.set("unique_path", currentUser.attributes.unique_path);
                currentUser.set("cookieAccepted", currentUser.get("cookieAccepted"));
                currentUser.save();
            } else {
                console.log("current user info was not updated");
            }
        }, function (error) {
            // The file either could not be read, or could not be saved to Parse.
        });
    }
    else
    {
        console.log("something broke while being uploading a file");
        return;
    }
}

function readURL(input) {
    /**
     * getting the image contents
     * */

    if (input && input[0]) {
        var reader = new FileReader();
        var base64str = input;
        reader.onload = function (e) {
            base64str = e.target.result;
        }
        reader.readAsDataURL(input[0]);
    }
    return input;
}

function setIntroLengthControl(intro_str, limit, $counter_object)
{
    if (intro_str.length >= 180)
    {
        $counter_object.css('color', '#FF8800');
    }
    if (intro_str.length >= 190)
    {
        $counter_object.css('color', '#FF554B');
    }
    if (intro_str.length >= limit)
    {
        $counter_object.css('color', '#ff0000');
    }
    if (intro_str.length < 180)
    {
        $counter_object.css('color', '#4b4b4b');
    }
}

function setChecked($object)
{
    $object.addClass('checked');
    $object.removeClass('empty');
}