/**
 * Created by cow-duck on 6/11/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);
var currentParseUser = Parse.User.current();

$(document).ready(function () {
    // adding blocks when looking at someone else's profile page

    var dynamicPath = currentLocation;
    if (dynamicPath.length > 0)
    {
        if (dynamicPath.indexOf('.htm') === -1 )
        {
            console.log("We are at the dynamic page. Do some dynamic request handles.");
            var currentUser = Parse.User.current();
            var current = false;

            if (currentUser && (dynamicPath == currentUser.get("unique_path") || dynamicPath == currentUser.id))
            {
                current = true;
                console.log("Session user found! Redirecting to user profile page...");
                setAppendedData(currentUser, current);
                setFindMeLink(currentUser, current);

                //window.location.replace(locationPrefix + dynamicPath);
            }
            else
            {
                var query = new Parse.Query(Parse.User).equalTo("unique_path", dynamicPath).find({
                    success: function (results) {
                        if (results.length <= 0)
                        {
                            // trying to find user through an unique id
                            query = new Parse.Query(Parse.User).equalTo("objectId", dynamicPath).find({
                                success: function (results) {
                                    if (results.length <= 0)
                                    {
                                        console.log("No user found with name: " + dynamicPath);
                                        $('figure.cloud').css('display','none');
                                        $('span.star-vip').css('display','none');
                                        $('aside.profile-box').css('display','none');
                                        $('article.profile').append('<aside class="panel-box profile-box"><h3>Page not found</h3></aside>');
                                    }
                                    else
                                    {
                                        setAppendedData(results, current);
                                        setFindMeLink(results, current);
                                    }
                                },
                                error: function (error) {
                                    console.log("Undefined error while trying to find '" + dynamicPath + "' user");
                                }
                            });
                        }
                        else
                        {
                            setAppendedData(results, current);
                            setFindMeLink(results, current);
                        } // else
                    },
                    error: function (error) {
                        console.log("Undefined error while trying to find '" + dynamicPath + "' user");
                    }
                });
            }
        }
        else
        {
            console.log("We are at the static page. Do nothing.");
        }
    }
});


function setUserColorTheme(id)
{
    getUserColor(id);
}


function setAppendedData(results,isCurrent)
{
    if (!isCurrent)
    {
        for (var i = 0; i < results.length; i++)
        {
            appendProcessing(results[i], isCurrent);
        }
    }
    else
    {
        appendProcessing(results, isCurrent);
    }
}

function appendProcessing(object, currentUser)
{
    if (!currentParseUser)
    {
        var $a = $("<a></a>").attr({
            href: locationPrefixWithoutDevider,
            class: "btn btn-join",
            id: "profile-not-logged-in"
        });
        var $text = "Join Now";
        $a.append($text);
        $("article.profile").append($a);
    }
    if (!currentUser)
    {
        setUserColorTheme(object.id);
    }

    var returned_username = object.attributes.first_name + " " + object.attributes.last_name;
    var d = new Date();

    $("#other_username_edit").html(returned_username);
    if (object.attributes.image.length > 0)
    {
        $("#other_user_photo").attr("src", object.attributes.image + "?ver=" + d.getTime());
    }
    else
    {
        $("#other_user_photo").attr("src", locationPrefix + backgrounds_dir + "no-result92x92.png" + "?ver=" + d.getTime());
    }

    if (object.attributes.intro.length > 0) {
        $("#other_intro_add").html(object.attributes.intro);
    }
    if (object.attributes.main_location.length > 0)
    {
        var loc = object.attributes.main_location;
        var city = loc.split(",")[0].trim();
        var country = loc.split(",")[1].trim();
        var href = locationPrefix + "search?lci=" + encodeURIComponent(city) + "&lco=" + encodeURIComponent(country);
        var appStr = "<a class=\"user-location-search-from-userpage\" href=\"" + href + "\" title=\"Search other people in " + loc + "\"><span itemprop=\"locality\">" + city + "</span>, <span itemprop=\"region\">" + country + "</span></a>";
        $("#other_main_loc").html(appStr);
    }
    var locs = object.attributes.locations;

    if (typeof locs != 'undefined' && locs.length > 0) {
        $.each(locs, function (i, val) {
            try
            {
                if (val != object.attributes.main_location && val)
                {
                    var city = val.split(",")[0].trim();
                    var country = val.split(",")[1].trim();
                    var href = locationPrefix + "search?";
                    var params = "lci=" + encodeURIComponent(city) + "&lco=" + encodeURIComponent(country);
                    var link = href + params;
                    var $span = $("<span></span>");
                    var $a = $("<a></a>").attr({
                        href: link,
                        class: "user-location-search-from-userpage"
                    });
//                    var $innerSpan = "<span class=\"locality\">" + city + "</span>, <span class=\"region\">" + country + "</span>";
                    //var $text = (i == 0 ? "" : " | ") + val;

                    $a.append(val);
                    $span.append($a);
                    if (i > 0)
                        $("#other_all_loc").append(" | ");
                    $("#other_all_loc").append($span);
                }
            }
            catch (err)
            {
//                console.log(err);
//                console.log(val);
//                console.log(i);
                $.each(val, function (key, value) {
                    if (value != object.attributes.main_location && value)
                    {
                        var city = value.split(",")[0].trim();
                        var country = value.split(",")[1].trim();
                        var href = locationPrefix + "search";
                        var paramsDevider = "?";
                        var params = "lci=" + encodeURIComponent(city) + "&lco=" + encodeURIComponent(country);
                        var link = href + paramsDevider + params;
                        var $span = $("<span></span>");
                        var $a = $("<a></a>").attr({
                            href: link,
                            class: "user-location-search-from-userpage"
                        });
//                        var $innerSpan = "<span class=\"locality\">" + city + "</span>, <span class=\"region\">" + country + "</span>";
                        //var $text = (i == 0 ? "" : " | ") + val;

                        $a.append(value);
                        $span.append($a);
                        if (i > 0)
                            $("#other_all_loc").append(" | ");
                        $("#other_all_loc").append($span);
                    }
                });
            }
        });
    }

    $(".user-location-search-from-userpage").on( 'click', function (click) {

        //window.setCookie("locsfrou", "true");
    });


    Parse.Cloud.run( "getUserSavedNetworksByUserID", { id: object.id }, {
        success: function (networks) {
            if (networks.length > 0)
            {
                var $nets = $("#other_networks");
                var $favs = $("#other_favonetworks");

                for (var i in networks)
                {
                    var network = networks[i];

                    if (network.networkStatus != "deleted" && network.networkStatus != "suspended")
                    {
                        var $li = $("<li></li>");
                        var $a = $("<a></a>").attr({
                            href: network.networkURI + "/" + network.userURI,
                            class: network.networkName + " overflowedLink",
                            target: '_blank',
                            rel: (network.networkName.indexOf("google") >= 0 ? "publisher" : "")
                        });
                        var $img = $("<img />").attr({
                            src: network.networkIconURI
                        });

                        $a.append($img);
                        $li.append($a);
                        $a.hide();
                        if (network.userFavorite)
                            $favs.append($li);
                        if (!network.userFavorite)
                            $nets.append($li);
                        $a.fadeIn(250);
                    }
                }

                $(".favo").each(function() {
                    var count = $(".favo li").length;
                    $('.social-favo-inner').css({ 'width': 52 * count + 'px' });
                });
                $(".network").each(function() {
                    var count = $(".network li").length;
                    $('.social-networks-inner').css({ 'width': 52 * count + 'px' });
                });
            }
            else
            {
                $('.social.favo').hide();
                $('.social.network').hide();
            }
        },
        error: function (error) {
            console.error(error);
        }
    });


    /**
     * providing shared connections support only for logged in users and not for current user's page
     * */
    var currentUser = Parse.User.current();
    if (currentUser)
    {
        if (currentUser.id != object.id)
        {
            if (currentUser.get("emailVerified"))
            {
                setAddToConnectionsLink(object);
            }
            else
            {
                $(".add-del-to-connections").html("<p class=\"unverified-email-p\">ADD TO CONTACTS</p>");
                $(".add-del-to-connections").show();
            }
        }
    }

    setVIPstatus(object);
}

/**
 * visualize user VIP status
 * */
function setVIPstatus(userObject)
{
    if (userObject.get("vip") == true)
    {
        var vip = "<span class=\"star-vip\">VIP</span>";
        $("article.profile").prepend(vip);
    }
}

/**
 * setting bottom link to user profile in user profile page
 * */
function setFindMeLink(object, isCurrent)
{
    var userPath = "";
    var returned_username = "";
    if (!isCurrent)
    {
        returned_username =
            object[0].attributes.first_name.toLowerCase().replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase(); }) + " " +
            object[0].attributes.last_name.toLowerCase().replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase(); });

        userPath = object[0].attributes.unique_path != undefined ? object[0].attributes.unique_path : object[0].id;
    }
    else
    {
        returned_username = "Your";
        userPath = object.get('unique_path') != undefined ? object.get('unique_path') : object.id;
    }
    var userProfileLink = "<h5><a href=\"" + locationPrefix + userPath + "\" title=\"" + returned_username + " profile page\">HowtoFind.Me/" + userPath + "</a></h5>";
    if (object)
        $("div.findme").append(userProfileLink);
    else
        $("div.findme").append(locationPrefixWithoutDevider);
}

function setAddToConnectionsLink(object)
{
    //add-to-connections class
    var button = $(".add-del-to-connections");
    var friendClassName = "UserFriend";
    var userClassName = "_User";

    var UserFriendNodeClass = Parse.Object.extend(friendClassName);
    var userFriendNode = new UserFriendNodeClass();
    var currentUserNode = Parse.User.current();

    var currentUserId = currentUserNode.id;
    var friendUserId = object.id;

    var accepted = false;

    if (userFriendNode)
    {
        var query = new Parse.Query(UserFriendNodeClass);

        // searching shared connections
        query.include("user");
        query.equalTo("user", { __type: "Pointer", className: userClassName, objectId: currentUserId });
        query.equalTo("friend", { __type: "Pointer", className: userClassName, objectId: friendUserId });
        query.find({
            success: function (results) {
                if (results.length > 0)
                {
                    // setting up onclick handlers for deleting user from shared connections
                    for (var i in results)
                    {
                        var currentNode = results[i];

                        // if user already added this connection but then removed it
                        if (currentNode.attributes.destructedAt != undefined)
                            addButtonHandler(button, currentNode, userClassName, currentUserId, friendUserId, accepted);
                        // if add a new one instance
                        else
                            deleteButtonHandler(button, userFriendNode, results);
                    }
                }
                else
                {
                    // setting up onclick handler for adding user to shared connections
                    addButtonHandler(button, userFriendNode, userClassName, currentUserId, friendUserId, accepted);
                }
            },
            error: function (results, error) {
                console.log(error);
            }
        });
    }
    else
    {
        console.log("WOW WOW! userFriendNode id not defined!");
    }
}

function deleteButtonHandler(button, userFriendNode, results)
{
    var link = button.find("a");
    var delImg = "<i class=\"fa fa-minus\" ></i>";
    link.attr( "title", "Delete this user from your contacts" );
    link.html("DEL FROM CONTACTS");

    button.prepend(delImg);
    button.data( 'action' , 'delete' );
    button.show();

    // user could cancel his action by clicking one more time on the link
    button.on( 'click', function (click) {
        click.preventDefault();

        if ($(this).data( 'action' ) == 'delete')
        {
            delConnection(results);

            button.fadeOut(150, function () {
                button.data( 'action' , 'cancel' );
                link.attr( "title", "Cancel" );
                link.html("CANCEL");
                button.find("img").detach();
                button.fadeIn(150);
            });
        }
        else
        {
            cancelAction('del', results, userFriendNode);

            button.fadeOut(150, function () {
                button.data( 'action' , 'delete' );
                link.attr( "title", "Delete this user from your contacts" );
                link.html("DEL FROM CONTACTS");
                button.prepend(delImg);
                button.fadeIn(150);
            });
        }
    });
}

function addButtonHandler(button, currentNode, userClassName, currentUserId, friendUserId, accepted)
{
    var link = button.find("a");
    var addImg = "<i class=\"fa fa-plus\" ></i>";

    link.attr( "title", "Add this user to your contacts" );
    link.html("ADD TO CONTACTS");

    button.prepend(addImg);
    button.data( 'action' , 'add' );
    button.show();
    button.on( 'click', function(click) {
        click.preventDefault();

        if ($(this).data( 'action' ) == 'add')
        {
            addConnection(currentNode, userClassName, currentUserId, friendUserId, accepted);

            button.fadeOut(250, function () {
                button.data( 'action' , 'cancel' );
                link.attr( "title", "Cancel" );
                link.html("CANCEL");
                button.find("img").detach();
                button.fadeIn(250);
            });
        }
        else
        {
            cancelAction('add', null, currentNode);

            button.fadeOut(250, function () {
                button.data( 'action' , 'add' );
                link.attr( "title", "Add this user to your contacts" );
                link.html("ADD TO CONTACTS");
                button.prepend(addImg);
                button.fadeIn(250);
            });
        }
    });
}

function addConnection(currentNode, userClassName, currentUserId, friendUserId, accepted)
{
    currentNode.set("user", { __type: "Pointer", className: userClassName, objectId: currentUserId });
    currentNode.set("friend", { __type: "Pointer", className: userClassName, objectId: friendUserId });
    currentNode.set("accepted", accepted);
    currentNode.unset("destructedAt");
    currentNode.save( null, {
        success: function (userFriendNode) {
            var message = "User successfully added to your shared connections";
            window.userFeedback(message, true);
        },
        error: function (userFriendNode, error) {
            console.log(error);
            var message = "Error: " + error.code + ". Something went wrong while adding new shared connection. Info: " + error.message;
            window.userFeedback(message, false);
        }
    });
}

function delConnection(results)
{
    for (var i in results)
    {
        var destructedDate = new Date();
        var currentNode = results[i];
        if (currentNode.attributes.destructedAt == undefined)
        {
            currentNode.set("user", currentNode.attributes.user);
            currentNode.set("friend", currentNode.attributes.friend);
            currentNode.set("accepted", currentNode.attributes.accepted);
            currentNode.set("destructedAt", destructedDate);
            currentNode.save( null, {
                success: function (results) {
                    var message = "User successfully deleted from your shared connections";
                    window.userFeedback(message, true);
                },
                error: function (results, error) {
                    console.log(error);
                    var message = "Error: " + error.code + ". Something went wrong while deleting this user from your shared connection. Info: " + error.message;
                    window.userFeedback(message, false);
                }
            });
        }
    }
}

function cancelAction(action, results, userFriendNode)
{
    if (action == 'add')
    {
        var currentNode = userFriendNode;
        if (currentNode.attributes.destructedAt == undefined)
        {
            var destructedDate = new Date();
            currentNode.set("user", currentNode.attributes.user);
            currentNode.set("friend", currentNode.attributes.friend);
            currentNode.set("accepted", currentNode.attributes.accepted);
            currentNode.set("destructedAt", destructedDate);
            currentNode.save( null, {
                success: function (results) {
                    var message = "ADDING CANCELLED";
                    window.userFeedback(message, true);
                },
                error: function (results, error) {
                    console.log(error);
                    var message = "Error: " + error.code + ". Something went wrong while cancelling. Info: " + error.message;
                    window.userFeedback(message, false);
                }
            });
        }
    }

    if (action == 'del')
    {
        for (var i in results)
        {
            var currentNode = results[i];
            if (currentNode.attributes.destructedAt != undefined)
            {
                currentNode.set("user", currentNode.attributes.user);
                currentNode.set("friend", currentNode.attributes.friend);
                currentNode.set("accepted", currentNode.attributes.accepted);
                currentNode.unset("destructedAt");
                currentNode.save( null, {
                    success: function (results) {
                        var message = "DELETING CANCELLED";
                        window.userFeedback(message, true);
                    },
                    error: function (results, error) {
                        console.log(error);
                        var message = "Error: " + error.code + ". Something went wrong while cancelling. Info: " + error.message;
                        window.userFeedback(message, false);
                    }
                });
            }
        }
    }
}