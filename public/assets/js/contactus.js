$(document).ready( function () {
    init_contact_us_form();
});

function init_contact_us_form()
{
    setREcaptcha();

    if (currentUser)
    {
        $('input[name=contactusEmail]').val(currentUser.get('email'));
    }

    $('form#contact-us').on( 'submit', function (submit) {
        submit.preventDefault();

        sendResponse();
    });
}

function setREcaptcha()
{
    Parse.Cloud.run( "getSecureData", { field: 'recaptcha_public_key' }, {
        success: function (key) {
            if (key)
            {
                Recaptcha.create(key, "REcaptcha",
                    {
                        lang: 'en',
                        theme: "clean"
                    }
                );
            }
            else
            {
                console.error("Error while taking public captcha key");
            }
        },
        error: function (error) {
            console.error(error);
        }
    });
}

function sendResponse()
{
    var email_val = $('input[name=contactusEmail]').val();
    var name_val = $('input[name=contactusName]').val();
    var message_val = $('textarea[name=contactusMessage]').val();
    var cap_challenge = $('#recaptcha_challenge_field').val();
    var cap_response = $('#recaptcha_response_field').val();
    var recaptcha_data, message = "";

    var data = {
        email: email_val,
        name: name_val,
        message: message_val,
        href: locationPrefixWithoutDevider
    };

    if (cap_response)
    {
        showLoading(true);

        recaptcha_data = {
            challenge: cap_challenge,
            response: cap_response
        };

        Parse.Cloud.run( 'checkREcaptcha', recaptcha_data, {
            success: function (response) {
                var isOk = JSON.parse(response.split("\n")[0]);
                var erMsg = response.split("\n")[1];

                if (!isOk)
                {
                    if (erMsg == "invalid-site-private-key")
                        message += "We weren't able to verify the private key. ";
                    if (erMsg == "invalid-request-cookie")
                        message += "The challenge parameter of the verify script was incorrect. ";
                    if (erMsg == "incorrect-captcha-sol")
                        message += "Captcha code was incorrect. Try one more time. ";
                    if (erMsg == "captcha-timeout")
                        message += "The solution was received after the CAPTCHA timed out. ";
                    if (erMsg == "recaptcha-not-reachable")
                        message += "reCAPTCHA never returns this error code. A plugin should manually return this code in the unlikely event that it is unable to contact the reCAPTCHA verify server. ";

                    Recaptcha.reload();
                    showLoading(false);
                    userFeedback(message, false);
                }
                else
                {
                    Parse.Cloud.run( 'contactUs', data, {
                        success: function (response) {
                            Recaptcha.reload();
                            showLoading(false);
                            userFeedback("Message was sent", true);
                        },
                        error: function (error) {
                            console.error(error);
                        }
                    });
                }
            },
            error: function (error) {
                console.error(error);
            }
        });
    }
    else
    {
        userFeedback("Fill the captcha please", false);
    }
}