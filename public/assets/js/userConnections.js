/**
 * Created by cow-duck on 7/14/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

$(function init () {
    if (window.isLocation("connections", currentLocation))
    {
        window.showLoading(true);
        $(".pagination").hide();
        $("#inner_search_result").hide();

        var parametersArray = getSearchParameters();

        setData(parametersArray);
        setSearch(); /** userContacts.js */
    }
});

/**
 * https://stackoverflow.com/a/5448635
 * */
function transformToAssocArray( prmstr )
{
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function getSearchParameters()
{
    var prmstr = window.location.search.substr(1);
    return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function setData(user)
{
    if (Parse.User.current().id == user.u)
    {
        window.location = locationPrefix + "mycontacts";
    }

    var data = {
        path: user.u
    }

    Parse.Cloud.run( "getUserDataByUniquePathOrID", data, {
        success: function(userObject) {
            /**
             * returns full user object by unique url or user id
             * */
            if (typeof userObject != 'undefined' && userObject.length > 0 && userObject)
            {
                if (userObject.length < 2)
                {
                    for (var i in userObject)
                    {
                        var userName = userObject[i].get("first_name") + " " + userObject[i].get("last_name");
                        if (userName.length > 2)
                            $("#user-header-info").append("With " + userName);

                        setSharedConnections(userObject);
                    }
                }
            }
            if (!userObject)
                console.log("You are so alone! Why don't you get some friends?");
        },
        error: function(error) {
            console.error(error);
        }
    });
}

function setSharedConnections(user)
{
    var data = {
        current: Parse.User.current().id,
        user: user[0].id
    }
    Parse.Cloud.run( "getSharedConnectionsByUserID", data, {
        success: function(sharedConnections) {
            /**
             * returns user friends IDs
             * */
            if (typeof sharedConnections != 'undefined' && sharedConnections.length > 0 && sharedConnections)
            {
                var data = {
                    array: sharedConnections
                };
                Parse.Cloud.run( "getUserObjectsByIdArray", data, {
                    success: function(userObjects) {
                        /**
                         * returns full user object (id, unique_path, name, main location, image, vip status, networks)
                         * */
                        if (typeof userObjects != 'undefined' && userObjects.length > 0 && userObjects)
                        {
                            $(".quantity").html(userObjects.length + " People");
//                            for (var i in userObjects)
//                            {
                                var data = {
                                    current: Parse.User.current().id,
                                    userArray: userObjects
                                }
                                Parse.Cloud.run( "getSharedConnectionsByUserIDsArray", data, {
                                    success: function (connectionsCount) {
                                        /**
                                         * returns shared connections count to every user in friends section
                                         * */

                                        appendData(userObjects, connectionsCount); /** userContacts.js */
                                    },
                                    error: function (error) {
                                        console.error(error);
                                    }
                                });
                            //}
                        }
                    },
                    error: function(error) {
                        console.error(error);
                    }
                });
            }
            else
            {
                window.showLoading(false);
                $(".pagination").fadeOut(250, function() {
                    $(".search-result").fadeOut(250, function() {
                        $("#search-no-results").fadeIn(250);
                    });
                });
                console.error("Sorry! Something went wrong or something like that. Maybe data is empty?");
            }
        },
        error: function(error) {
            console.error(error);
        }
    });
}

