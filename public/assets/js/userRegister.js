/**
 * Created by cow-duck on 6/5/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

$(document).ready(function (){

    if (currentLocation.indexOf("edit-networks") >= 0)
    {

    }

    $('#user_signup_form div.row input').each(function (){
        if ($(this).val() != "")
            $(this).css('border','1px solid #6EFF66');
        $(this).blur(function(){
            checkRegisterForm($(this));
        });

        $(this).focus(function(){
            $('#'+$(this).attr('id')+'_error').fadeOut(250);
        });
    });

    $('#signup-btn').hover(function (){$(this).css('color','#FF6666');}, function(){$(this).css('color','#ffffff');});
});

var hasSomeErrors = false;

function checkRegisterForm(input)
{
    var inputId = input.attr('id');

    var isNotEmpty = false;
    var isEmailCorrect = false;
    var isPasswordCorrect = false;
    var isPasswordMatch = false;
    var hasError = false;
    var errorMessage = "";

    isNotEmpty = checkIfEmpty(input);
    if (inputId == 'email')
        isEmailCorrect = emailChecker(input.val());
    else if (inputId == 'password')
        isPasswordCorrect = userPasswordChecker(input.val());
    else if (inputId == 'check_password')
    {
        var pwd = $('#password').val();
        var checkpwd = $('#check_password').val();
        isPasswordMatch = stringMatcher(pwd, checkpwd);
    }

    if (!isNotEmpty)
    {
        colorizeInvalidateInput(input);
        errorMessage = "Some fields are empty. ";
        hasSomeErrors = true;
        hasError = true;
    }
    else if (isNotEmpty)
    {
        colorizeValidateInput(input);
        hasSomeErrors = false;
        hasError = false;
    }

    if (inputId == 'email' && !isEmailCorrect)
    {
        colorizeInvalidateInput(input);
        errorMessage += "Email is not valid. ";
        hasSomeErrors = true;
        hasError = true;
    }
    else if (inputId == 'email' && isEmailCorrect)
    {
        colorizeValidateInput(input);
        hasSomeErrors = false;
        hasError = false;
    }

    if (inputId == 'password' && !isPasswordCorrect)
    {
        colorizeInvalidateInput(input);
        errorMessage += "Password should have minimum 8 characters. Password should have at least one digit, one upper case letter and one lower case letter. ";
        hasSomeErrors = true;
        hasError = true;
    }
    else if (inputId == 'password' && isPasswordCorrect)
    {
        colorizeValidateInput(input);
        hasSomeErrors = false;
        hasError = false;
    }

    if (inputId == 'check_password')
    {
        if (checkIfEmpty(input))
        {
            if (!isPasswordMatch)
            {
                colorizeInvalidateInput(input);
                errorMessage += "Passwords should match. ";
                hasSomeErrors = true;
                hasError = true;
            }
            else
            {
                colorizeValidateInput(input);
                hasSomeErrors = false;
                hasError = false;
            }
        }
        else
        {
            colorizeInvalidateInput(input);
            errorMessage += "Passwords should not be empty. ";
            hasSomeErrors = true;
            hasError = true;
        }
    }

    if ((hasError || hasSomeErrors) && (errorMessage != '' && errorMessage.length > 0))
    {
        window.userFeedback(errorMessage, false);
        return false;
    }
    else
    {
        $('.bottom-error').html("");
        return true;
    }
}

function checkIfEmpty(object)
{
    if (object.val() == "") return false;
    else return true;
}

function stringMatcher(string1, string2)
{
    if (string1 != string2) return false;
    else return true;
}

function colorizeValidateInput(object)
{
    $('#'+object.attr('id')+'_error').fadeOut(250);
    object.css('border','1px solid #6EFF66');
}

function colorizeInvalidateInput(object)
{
    $('#'+object.attr('id')+'_error').fadeIn(250);
    object.css('border','1px solid #FF6666');
}


/**
 * checking the field values for signup
 * */
function user_signup_validate() {

    var pwd = $('#password').val();
    var checkpwd = $('#check_password').val();
    var errorMessage = "";
    var error = false;
    var tmp = 0;

    $('.reg-form-input').each( function () {
        if ($(this).val() == "")
        {
            $('#'+$(this).attr('id')+'_error').css('display','block')//fadeIn(250);
            $(this).css('border','1px solid #FF6666');
            errorMessage = "Some fields are empty. ";

            ++tmp;
        }
        else
        {
            $('#'+$(this).attr('id')+'_error').fadeOut(250);
            $(this).css('border','1px solid #6EFF66');
        }
    });

    var correctEmail = emailChecker($('#email').val());
    var correctPassword = userPasswordChecker(pwd);

    if (tmp == 0 && !correctEmail)
    {
        $('#email').css('border','1px solid #FF6666');
        $('#email_error').css('display','block');
        errorMessage += "Email is not valid. ";
    }

    if (tmp == 0 && pwd.length < 8)
    {
        $('#password').css('border','1px solid #FF6666');
        $('#password_error').css('display','block');
        errorMessage += "Password should have minimum 8 characters. ";
    }

    if (tmp == 0 && !correctPassword)
    {
        $('#password').css('border','1px solid #FF6666');
        $('#password_error').css('display','block');
        errorMessage += "Password should have at least one digit, one upper case letter and one lower case letter. ";
    }

    if (tmp == 0 && !stringMatcher(pwd, checkpwd))
    {
        $('#password').css('border','1px solid #FF6666');
        $('#check_password').css('border','1px solid #FF6666');
        $('#password_error').css('display','block');
        $('#check_password_error').css('display','block');
        errorMessage += "Passwords should match. ";
    }

    if (tmp > 0 || (errorMessage != '' && errorMessage.length > 0))
    {
        window.userFeedback(errorMessage, false);
        return false;
    }
    else
    {
        $('.bottom-error').html("");
        return true;
    }
}

function emailChecker(object)
{
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(object);
}

/**
 * checking validation for entered password (8 chars min, one small case letter, one big letter, one digit at least)
 * */
function userPasswordChecker(userPass)
{
    var small_case = "qwertyuiopasdfghjklzxcvbnm";
    var big_case = "QWERTYUIOPLKJHGFDSAZXCVBNM";
    var digits = "0123456789";
    var passwordLength = 8;
    //var specialChars = "!@#$%^&*()_-+=\|/.,:;[]{}"; // will be implemented if necessary

    var hasSmallCase = false;
    var hasBigCase = false;
    var hasDigit = false;
    var hasCorrectLength = false;
    //var hasSpecialChars = false;

    var allIsCorrect = false;

    if (userPass.length >= passwordLength)
    {
        hasCorrectLength = true;
        for (var i=0; i<userPass.length; i++)
        {
            if (!hasSmallCase && small_case.indexOf(userPass[i]) != -1) hasSmallCase = true;
            else if (!hasBigCase && big_case.indexOf(userPass[i]) != -1) hasBigCase = true;
            else if (!hasDigit && digits.indexOf(userPass[i]) != -1) hasDigit = true;
//          else if (!hasSpecialChars && specialChars.indexOf(userPass[i]) != -1) hasSpecialChars = true;
        }

//          var returnText = "";
//          if (!hasSmallCase) returnText = "at least one small case letter needed. ";
//          if (!hasBigCase) returnText += "at least one big case letter needed. ";
//          if (!hasDigit) returnText += "at least one digit needed. ";
    }
    else
    {
        hasCorrectLength = false;
    }

    if (!hasCorrectLength)
    {
        return false;
    }
    else
    {
        if (hasSmallCase && hasBigCase && hasDigit)
            allIsCorrect = true;

        if (allIsCorrect)
            return true;
        else
            return false;
    }
}

/**
 * registering new user
 * */
$(function signup_user() {

    var user = new Parse.User();

    $('#signup-btn').click(function (signup) {
        signup.preventDefault();
        showLoading(true);

        if (user_signup_validate()) {
            var locations = new Array();
            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var email = $('#email').val();
            var pass = $('#password').val();

            var image = "",
                intro = "",
                main_location = "",
                theme_color = "",
                defaultAvailableUrl = undefined,
                vip = false,
                cookieAccepted = false;

            setUpdatedUserData(
                user,
                first_name.toLowerCase(),
                last_name.toLowerCase(),
                email,
                pass,
                image,
                intro,
                main_location,
                theme_color,
                vip,
                locations,
                defaultAvailableUrl,
                cookieAccepted
            );

            user.signUp(null, {
                success: function (user) {
                    // landing to welcome page
                    window.location = locationPrefix + "register/thank-you";
                },
                error: function (user, error) {
                    // Show the error message somewhere and let the user try again.
                    alert("Error: " + error.code + " " + error.message);
                }
            });
        }
    });
});

$(function registerStep2 () {
    // adding unique URL
    $(function () {
        $(".check-tick input").on('change', function () {
            if (document.forms.userRegisterDefaultUrlForm.chboxa.checked)
                $('.bottom-error').fadeOut(250);
        });
    });

    $(function(){$("#user-register-default").hide();});
    if (currentLocation.indexOf("step2") >= 0)
    {
        var currentUser = Parse.User.current();
        if (currentUser)
        {
            var userFirstName = currentUser.get("first_name"),
                userLastName = currentUser.get("last_name"),
                defaultUserURL = currentUser.get("unique_path");

            if (typeof defaultUserURL == 'undefined' || defaultUserURL == "")
            {
//                userFirstName = userFirstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
//                    return letter.toUpperCase();
//                });
//                userLastName = userLastName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
//                    return letter.toUpperCase();
//                });

                defaultUserURL = userFirstName + userLastName;
            }

            var pathQuery = new Parse.Query(Parse.User).equalTo("unique_path", defaultUserURL).find({
                success: function (results) {
                    var currentUser = Parse.User.current();
                    if (currentUser)
                    {
                        var currentId = currentUser.id;
                        var current = false;

                        for (var i in results)
                        {
                            if (currentId == results[i].id)
                                current = true;
                        }
                        if (results.length > 0 && !current)
                        {
                            defaultUserURL += (results.length+1);
                        }
                        $("#user-register-default-url").val(defaultUserURL);

                        $('form#userRegisterDefaultUrlForm').on( 'submit', function(submit) {
                            submit.preventDefault();

                            $("#user-register-default-utl-step3").click();
                        });

                        $("#user-register-default-utl-step3").on('click', function (click) {
                            click.preventDefault();
                            showLoading(true);

                            if (document.getElementById("user-register-default-OR-text").style.display != '')
                            {
                                var transformedRadio = $(".jqTransformRadioWrapper a");
                                var tmp = false;
                                for (var i in transformedRadio)
                                {
                                    var classNameToString = "" + transformedRadio[i].className;
                                    if (classNameToString.indexOf("jqTransformChecked") >= 0)
                                        tmp = true;
                                }
                                // if user not checked some new userpath value
                                if (!tmp)
                                {
                                    var errorMessage = "Please choose another username!";
                                    window.userFeedback(errorMessage, false);
                                    showLoading(false);
                                    return false;
                                }
                            }

                            var errorMessage;
                            var newUserUrl = $("#user-register-default-url").val().toLowerCase();

                            if (window.reservedNames.indexOf(newUserUrl) >= 0)
                            {
                                errorMessage = "This username is reserved, please choose another username.";
                            }
                            if (newUserUrl.length <= 0 && newUserUrl == "")
                            {
                                errorMessage = "Please, make sure you entered a valid URL!";
                            }
                            if (!document.forms.userRegisterDefaultUrlForm.chboxa.checked)
                            {
                                errorMessage = "Please, make sure you agreed the terms and conditions!";
                            }
                            if (newUserUrl.length <= 0 && newUserUrl == "" && !document.forms.userRegisterDefaultUrlForm.chboxa.checked)
                            {
                                errorMessage = "Please, enter some valid URL and make sure you agreed the terms and conditions!";
                            }

                            if (typeof errorMessage != 'undefined' && errorMessage.length != 0 && errorMessage != "")
                            {
                                window.userFeedback(errorMessage, false);
                                showLoading(false);
                                return false;
                            }
                            else
                            {
                                var pathQuery = new Parse.Query(Parse.User).equalTo("unique_path", newUserUrl).find({
                                    success: function (results) {
                                        var currentUser = Parse.User.current();
                                        if (currentUser)
                                        {
                                            var currentId = currentUser.id;
                                            var current = false;
                                            for (var i in results)
                                            {
                                                if (currentId == results[i].id)
                                                    current = true;
                                            }
                                            if (results.length > 0 && !current)
                                            {
                                                var index = (results.length + 1);
                                                var current_year = (new Date).getFullYear();
                                                var prefix = "howtofind.me/";
                                                var first_substitution = userLastName + userFirstName + index;
                                                var second_substitution = userFirstName + userLastName + index;
                                                var third_substitution = newUserUrl + current_year + "" + index;

                                                // check if userpath value already taken. then generate a new one
                                                if (newUserUrl == first_substitution)
                                                    first_substitution = userLastName + userFirstName + (index + 1);
                                                if (newUserUrl == second_substitution)
                                                    second_substitution =  userFirstName + userLastName + (index + 1);
                                                if (newUserUrl == third_substitution)
                                                    third_substitution = newUserUrl + current_year + "" + (index + 1);

//                                                console.log("has results");
                                                $("#main .panel-box .fld-for-url").css('width', '125px');
                                                $("#user-register-default-url-common").css('width', '235px');
                                                $("#user-register-default-check-ok").fadeIn(250);
                                                $("#user-register-default-url-h3").html("Sorry somebody has the same name");
                                                $("#user-register-default-url-appended-variants-h3").fadeIn(250);
                                                $("#user-register-default").fadeIn(250);

                                                $("#regRadio1Label").html(prefix + first_substitution);
                                                $("#regRadio2Label").html(prefix + second_substitution);
                                                $("#regRadio3Label").html(prefix + third_substitution);

                                                $("#user-register-default-OR-text").fadeIn(250);

                                                // set the label value to the URL input
                                                $("#user-register-default input").each( function () {
                                                    $(this).on('click', function () {
                                                        var inner = $(this).parent().next().text();
                                                        // substring prefix value to insert just pathname into input value
                                                        var text = inner.substr(prefix.length, inner.length)
                                                        $("#user-register-default-url").val(text);
                                                        $("#user-register-default-check-ok").removeClass('unchecked')
                                                        $("#user-register-default-check-ok").addClass('checked');
                                                        $('.bottom-error').fadeOut(250);
                                                    });
                                                });

                                                showLoading(false);
                                                $('.bottom-error').fadeOut(250);
                                            }
                                            else
                                            {
//                                                console.log("no results");
                                                $('.bottom-error').fadeOut(250);
                                                newUserUrl = $("#user-register-default-url").val();

                                                var currentNetworks = (typeof currentUser.attributes.netw == 'undefined' || currentUser.attributes.netw.length <= 0 ? [] : currentUser.attributes.netw);
                                                var currentFavoriteNetworks = (typeof currentUser.attributes.fav_netw == 'undefined' || currentUser.attributes.fav_netw.length <= 0 ? [] : currentUser.attributes.fav_netw);
                                                var currentLocations = (typeof currentUser.attributes.locations == 'undefined' || currentUser.attributes.locations.length <= 0 ? [] : currentUser.attributes.locations);

                                                currentUser.set("first_name", currentUser.attributes.first_name);
                                                currentUser.set("last_name", currentUser.attributes.last_name);
                                                currentUser.set("email", currentUser.attributes.email);
                                                currentUser.set("password", currentUser.attributes.password);
                                                currentUser.set("username", currentUser.attributes.email);
                                                currentUser.set("image", currentUser.attributes.image);
                                                currentUser.set("intro", currentUser.attributes.intro);
                                                currentUser.set("locations", currentLocations);
                                                currentUser.set("main_location", currentUser.attributes.main_location);
                                                currentUser.set("theme_color", currentUser.attributes.theme_color);
                                                currentUser.set("vip", currentUser.attributes.vip);
                                                currentUser.set("unique_path", newUserUrl);
                                                currentUser.set("cookieAccepted", currentUser.get("cookieAccepted"));
                                                currentUser.save(null, {
                                                    success: function () {
                                                        window.setCookie("fromRegister", "true", 1);
                                                        window.location = locationPrefix + "edit";
                                                    },
                                                    error: function (error) {
                                                        console.log("Error: " + error.code + ". Something went wrong while updating new user with custom URL: " + error.message);
                                                    }
                                                });
                                            }
                                        }
                                    },
                                    error: function (error) {
                                        showLoading(false);
                                        console.log("Error: " + error.code + ". Something went wrong while updating new user with custom URL: " + error.message);
                                    }
                                });
                            }

                        });
                    }
                },
                error: function (error) {
                    console.log("Error: " + error.code + ". Something went wrong while updating new user: " + error.message);
                }
            });
        }
        else
        {
            // if user no logged in and try to access register/step2 page
            window.location = locationPrefix + "sign-in.htm";
        }
    }
    else if (currentLocation.indexOf("thank") >= 0 && !currentUser)
    {
        // if user no logged in and try to access register/thank-you page
        window.location = locationPrefix + "sign-in.htm";
    }
});

//function setUserData
function setUpdatedUserData(user, first_name, last_name, email, pass, image, intro, main_location, theme_color, vip_status, locations, defaultAvailableUrl, cookieAccepted)
{
    user.set("first_name", first_name);
    user.set("last_name", last_name);
    user.set("email", email);
    user.set("password", pass);
    user.set("username", email);
    user.set("image", image);
    user.set("intro", intro);
    user.set("locations", locations);
    user.set("main_location", main_location);
    user.set("theme_color", theme_color);
    user.set("vip", vip_status);
    user.set("unique_path", defaultAvailableUrl);
    user.set("cookieAccepted", cookieAccepted);
}