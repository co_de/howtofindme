/**
 * Created by cow-duck on 6/23/14.
 */

/**
 * Links forwarding for all of a-tags
 * */

var currentHostname = window.location.hostname;
var currentUser = Parse.User.current();
var youtubeVideoID = "A3PDXmYoF5U";

$(function () {

    /**
     * registration - step 3 - setting up user networks
     *  */
    $("#user-register-step2").attr("href" , locationPrefix + "register/step2");

    /**
     * edit profile
     * */
    $('#edit_profile').attr( "href", locationPrefix + "edit" );

    /**
     * header HOME forwarding
     * */
    if (currentUser)
        $(".header a").attr( "href", locationPrefix + ( currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id));
    else
        $(".header a").attr( "href", locationPrefixWithoutDevider);

    /**
     * footer link
     * */
    $("footer a").attr( "href", locationPrefixWithoutDevider);

    /**
     * join now button (if visible)
     * */
    $("#search_not_logged_in").attr( "href", locationPrefix + "sign-in.htm");

    /**
     * join now bottom link
     * */
    $(".txt-link a").attr( "href", locationPrefixWithoutDevider);

    /**
     * setting up link for video
     * */
    $(".youtube").attr( "href", prot + "//www.youtube.com/embed/" + youtubeVideoID);
});