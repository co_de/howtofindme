/**
 * Created by cow-duck on 6/13/14.
 */

$(document).ready( function() {
    //var availableCities = getAvailableInfo('cities');
    //var availableCountries = getAvailableInfo('countries');

    var locationsArray = new Array();
    var citiesArray = new Array();
    var countriesArray = new Array();
    var cities = [];
    var countries = [];

    var query = new Parse.Query(Parse.User).select("locations").find().then( function(results) {
        if (results.length > 0)
        {
            for (var i in results)
            {
                var locationsElement = results[i].attributes.locations;
                if (locationsElement != undefined && locationsElement.length > 0)
                {
                    for (var j in locationsElement)
                    {
                        if ( jQuery.inArray(locationsElement[j], locationsArray) == -1 && locationsElement[j] != '' && locationsElement[j].length > 0 )
                        {
                            locationsArray[i] = locationsElement[j];
                            ++i;
                        }
                    }
                }
            }

            for (var i in locationsArray)
            {
                var locationElement = locationsArray[i];
                if (locationElement.length > 0)
                {
                    for (var j in locationElement)
                    {
                        var bufferedLocationElement = locationElement[j].trim().split(',');
                        if ( jQuery.inArray(bufferedLocationElement[0].trim(), citiesArray) == -1)
                        {
                            citiesArray[i] = bufferedLocationElement[0].trim();
                        }

                        if ( jQuery.inArray(bufferedLocationElement[1].trim(), countriesArray) == -1)
                        {
                            countriesArray[i] = bufferedLocationElement[1].trim();
                        }
                    }
                }
            }

            for (var i in citiesArray)
            {
                if (citiesArray[i] != undefined)
                    cities.push(citiesArray[i]);
            }

            for (var i in countriesArray)
            {
                if (countriesArray[i] != undefined)
                    countries.push(countriesArray[i]);
            }

            $("#city_input").autocomplete({
                source: cities
            });

            $("#country_input").autocomplete({
                source: countries
            });

        }
        else
        {
            console.log("return");
            return "";
        }
    });

} );