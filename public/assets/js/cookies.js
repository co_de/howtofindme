/**
 * Created by cow-duck on 6/4/14.
 */

var isAgreed = "isAgreed";

window.loadCookieFromDataBase = function(currentUser)
{
    if (currentUser)
    {
        if (currentUser.get("cookieAccepted"))
        {
            setCookie(isAgreed, 'true', 7);
        }
    }
}

window.getCookie = function (name)
{
    var nameEQ = name + "=";
    var cookieArr = document.cookie.split(';');
    for (var i=0; i<cookieArr.length;i++)
    {
        var cookieElem = cookieArr[i];
        while (cookieElem.charAt(0) == ' ') cookieElem = cookieElem.substring(1,cookieElem.length);
        if (cookieElem.indexOf(nameEQ) == 0) return cookieElem.substring(nameEQ.length, cookieElem.length);
    }
    return null;
}

window.setCookie = function (name, value, days)
{
    if (days)
    {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

window.unsetCookie = function (name)
{
    if (name == null)
        name = isAgreed;

    setCookie(name, "", -1);
}

function checkCookieValidation()
{
    if (window.getCookie(isAgreed) == 'true')
        return true;
    else
        return false;
}

function setAgreedForCookieUsing(currentUser)
{
    window.setCookie(isAgreed, 'true', 7); // setting 7 days expire for cookies;
    var accepted = true;

    /**
     * saving user choice for cookie accepting
     * */
    if (currentUser)
    {
        currentUser.set("first_name", currentUser.get("first_name").toLowerCase());
        currentUser.set("last_name", currentUser.get("last_name").toLowerCase());
        currentUser.set("email", currentUser.get("email"));
        currentUser.set("password", currentUser.get("password"));
        currentUser.set("username", currentUser.get("email"));
        currentUser.set("image", currentUser.get("image"));
        currentUser.set("intro", currentUser.get("intro"));
        currentUser.set("locations", currentUser.get("locations"));
        currentUser.set("main_location", currentUser.get("main_location"));
        currentUser.set("theme_color", currentUser.get("theme_color"));
        currentUser.set("vip", currentUser.get("vip"));
        currentUser.set("unique_path", currentUser.get("unique_path"));
        currentUser.set("cookieAccepted", accepted);
        currentUser.save( null, {
            success: function () {
                console.log("Successfully accepted the cookie usage!");
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
}

$(document).ready(function ()
{
    /**
     * if user logged in and accepted cookie
     * */
    var currentUser = Parse.User.current();
    if (currentUser)
    {
        loadCookieFromDataBase(currentUser);
    }

    /**
     * dynamically creating popup warning element
     * */
    var popupCookieBasicFragment = $('.popup-cookie');
    if (checkCookieValidation())
    {
        popupCookieBasicFragment.css('display','none');
    }
    else
    {
        var popupCookieInnerFragment = $('.popup-inner');

        var p = document.createElement('p');
        popupCookieInnerFragment.append(p);

        var pText = "By using this site you agree to our use of cookies. You will only see this message once.";
        p.appendChild(document.createTextNode(pText));

        var a = document.createElement('a');
        a.setAttribute('class','btn');

        var aText = "OK";
        a.appendChild(document.createTextNode(aText));

        p.appendChild(a);

        $('.popup-inner a').click
        (
            function (click)
            {
                click.preventDefault();

                window.setAgreedForCookieUsing(currentUser);
                popupCookieBasicFragment.css('display','none');
            }
        );
    }
});
