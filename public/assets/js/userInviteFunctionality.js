/**
 * Created by cow-duck on 7/5/14.
 */

var currentHostname = window.location.hostname;
var currentLocation = window.location.pathname.substr(1);

$( function () {
   if (currentLocation.indexOf("invite") >= 0)
   {
       init();
   }
});

function init()
{
    var inviteForm = $('#invite-friends-form');

    $("#return-to-my-profile-from-invite").attr( "href", locationPrefix + (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id));

    // implementing blur event on each input
    $.each(inviteForm.children(), function() {
        var inputObject = $(this).find('input');

        inputObject.on('blur', function() {
            window.emailValidator($(this));
        });
    });

    // define click event on button
    $('#invite_mail').on('click', function (click) {
        click.preventDefault();

        inviteFriends(inviteForm);
    });
}

function inviteFriends(inviteForm)
{
    var friendsEmails = new Array();
    var errorCount = 0;

    // checks every input on valid email address
    $.each(inviteForm.children(), function() {
        var inputObject = $(this).find('input');

        if (inputObject.attr('type') == "text" && !inputObject.data("disabled"))
        {
            var innerInputValue = inputObject.val();
            if (innerInputValue)
            {
                if (window.emailValidator(inputObject))
                {
                    friendsEmails.push(innerInputValue);
                }
                else
                {
                    ++errorCount;
                }
            }
        }
    });

    if (errorCount == 0 && friendsEmails.length > 0)
    {
        $('.bottom-error').fadeOut(250);
        parseSendInvites(friendsEmails);
    }
    else
    {
        var message = "Enter at last one Email address!";
        $('.bottom-error').html(message);
        $('.bottom-error').fadeIn(250);
    }
}

function parseSendInvites(emailsArray)
{
    var currentUser = Parse.User.current();

    if (currentUser)
    {
        var userFirstName = currentUser.get('first_name');
        var userLastName = currentUser.get('last_name');
        // link to user profile
        var userLink = locationPrefix + (currentUser.get("unique_path") ? currentUser.get("unique_path") : currentUser.id);

        // make capitalize first letter in first and last name
        userFirstName = userFirstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        userLastName = userLastName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });

        // make username who send an invite
        var userName = userFirstName + " " + userLastName;

        // email text
        var emailText =
                "<p>Hello," +
                "<br /><br />This is <b>HowToFind.me.</b> <br /><b><a href=\"" + userLink + "\" title=\"\">" + userName + "</a>" +
                "</b> has invited you to join us! " +
                "<br />You could visit <a href=\"" + locationPrefixWithoutDevider + "\" title=\"HowToFind.me\"/>HowToFind.me</a> to register." +
                "<br /><br />Thank you," +
                "<br /><b>HowToFindMe team.</b></p>",
            emailSubject = "Invite to HowToFindMe!",
            emailFromEmail = "htfm@htfm.com",
            emailFromName = userName,
            emailToName = "";

        Parse.Cloud.run( "isUseraMember", { array: emailsArray }, {
            success: function (membersEmails) {
                var toSend = new Array();
                if (membersEmails)
                {
                    if (membersEmails.length > 0)
                    {
                        for (var i in membersEmails)
                        {
                            for (var j in emailsArray)
                            {
                                var email = membersEmails[i];
                                if (emailsArray[j] != email)
                                {
                                    toSend.push(emailsArray[i]);
                                }
                            }
                        }

                        Parse.Cloud.run( "getUserDataByEmailsArray", { array: membersEmails }, {
                            success: function (usersArray) {
                                if (usersArray.length > 0)
                                {
                                    for (var i in usersArray)
                                    {
                                        for (var j in membersEmails)
                                        {
                                            var memberEmail = membersEmails[j];
                                            var user = usersArray[i];

                                            if (user)
                                            {
                                                var inviteForm = $('#invite-friends-form');

                                                $.each(inviteForm.children(), function() {
                                                    var inputObject = $(this).find('input');
                                                    var inputEmail = inputObject.val();

                                                    if (inputEmail == memberEmail && !inputObject.data("disabled"))
                                                    {
                                                        var inputParent = inputObject.parent();
                                                        var userFirstName = user.get('first_name');
                                                        var userLastName = user.get('last_name');

                                                        userFirstName = userFirstName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                                                            return letter.toUpperCase();
                                                        });
                                                        userLastName = userLastName.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                                                            return letter.toUpperCase();
                                                        });

                                                        var userName = userFirstName + " " + userLastName;
                                                        var userLink = locationPrefix + (user.get("unique_path") ? user.get("unique_path") : user.id);
                                                        var txt = "<b>" + inputEmail +"</b>" + " is already<br />our member! ";
                                                        var find_him_here = "Find him ";
                                                        var or = " or ";

                                                        var $link = $('<a></a>')
                                                            .attr({
                                                                href: userLink,
                                                                title: userName
                                                            })
                                                            .html("here");
                                                        var $return = $('<a></a>')
                                                            .attr({
                                                                class: "member-cancel " + user.id,
                                                                href: "#",
                                                                title: "Return to invite"
                                                            })
                                                            .html("return");
                                                        var $p = $('<p></p>').attr({
                                                            class: "already-member-text txt-link"
                                                        });

                                                        inputObject.hide(250, function () {
                                                            $(this).data( "disabled", true ).data( "id", user.id );
                                                        });
                                                        $p.append(txt);
                                                        $p.append(find_him_here);
                                                        $p.append($link);
                                                        $p.append(or);
                                                        $p.append($return);
                                                        $return.data( "id", user.id );

                                                        inputParent.append($p);

                                                        $return.on( 'click', function (click) {
                                                            click.preventDefault();

                                                            var id = $(this).data( "id" );
                                                            var input = $(this).parents(".row").find("input");

                                                            if (input.data( "id" ) == id)
                                                            {
                                                                $(this).parent().hide(250, function() {
                                                                    $(this).detach();
                                                                    input.data( "disabled", false );
                                                                    input.val("");
                                                                    input.show(250);
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            },
                            error: function (error) {
                                console.error(error);
                            }
                        });
                    }
                    else
                    {
                        toSend = emailsArray;
                    }
                }

                if (toSend.length > 0)
                {
                    for (var i in toSend)
                    {
                        var emailToEmail = toSend[i];

                        var data = {
                            message: {
                                text: emailText,
                                subject: emailSubject,
                                from_email: emailFromEmail,
                                from_name: emailFromName,
                                to: [
                                    {
                                        email: emailToEmail,
                                        name: emailToName
                                    }
                                ]
                            }
                        }

                        Parse.Cloud.run('sendMailTo', data, {
                            success: function(result) {
                                console.log(result);
                                var message = "Invite has been sent successfully!";
                                window.userFeedback(message, true);
                            },
                            error: function(error) {
                                var message = error.message;
                                window.userFeedback(message, false);
                                console.log(error);
                            }
                        });
                    }
                }
            },
            error: function (error) {
                console.error(error);
            }
        });
    }
}