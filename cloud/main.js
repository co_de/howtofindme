require('cloud/app.js');

var parse_id = 'RBCXcBe4RLU3hfbpXYgqSAtgO2MYUpidQfJaFd4I';
var js_key = 'bqJAuYLQXgGrESHGT9LCVUoyyuqp2qpwK1yZ575z';
var mandrill_key = '4aKtgtJoxMc6_HmFBz5KLw';
var Image = require("parse-image");

Parse.initialize(parse_id, js_key);



Parse.Cloud.define( "cropper", function (request, response) {
    var url = request.params.url;
    var width = request.params.width;
    var height = request.params.height;
    var left = request.params.left;
    var top = request.params.top;

    Parse.Cloud.httpRequest({
        url: url
    }).then(function (response) {
        var image = new Image();
        return image.setData(response.buffer);

    }).then(function (image) {
        // Crop the image to the smaller of width or height.
        return image.crop({
            left: left,
            top: top,
            width: width,
            height: height
        });

    }).then(function (image) {
        // Make sure it's a JPEG to save disk space and bandwidth.
        return image.setFormat("JPEG");

    }).then(function (image) {
        // Get the image data in a Buffer.
        return image.data();

    }).then(function (buffer) {
        // Save the image into a new file.
        var base64 = buffer.toString("base64");
        var cropped = new Parse.File("image.jpg", { base64: base64 });
        return cropped.save();
//
//    }).then(function (cropped) {
//        // Attach the image file to the original object.
//        user.set("image", cropped._url);

    }).then(function (cropped) {
        response.success(cropped._url);
    }, function (error) {
        response.error(error);
    });
});

//Use Parse.Cloud.define to define as many cloud functions as you want.
//    For example:
//    Parse.Cloud.define("hello", function(request, response) {
//        response.success(request.params.hello);
//    });

Parse.Cloud.define( "sendMailTo", function(request, response) {
    var Mandrill = require('mandrill');
    Mandrill.initialize(mandrill_key);

    Mandrill.sendEmail({
        message: {
            html: request.params.message.text,
            subject: request.params.message.subject,
            from_email: request.params.message.from_email,
            from_name: request.params.message.from_name,
            to: [
                    {
                        email: request.params.message.to[0].email,
                        name: request.params.message.to[0].name
                    }
            ]
        },
        async: false
    },{
        success: function(httpResponse) {
            response.success("Email sent!");
        },
        error: function(httpResponse) {
            response.error("Uh oh, something went wrong");
        }
    });
});

Parse.Cloud.define( "contactUs", function (req, res) {

    var emailFrom = req.params.email;
    var nameFrom = req.params.name;
    var messageFrom = req.params.message;
    var htfm_href = req.params.href;


    Parse.Cloud.run( 'getAllAdmins', {}, {
        success: function (admins) {
            if (admins.length > 0)
            {
                for (var i in admins)
                {
                    var admin = admins[i].get('user');
                    var admin_email = admin.get('email');
                    var admin_name = admin.get('first_name') + " " + admin.get("last_name");

                    var emailSubject = "HowToFindMe - A new message from contact form";
                    var emailText =
                            "<p><h1>Hello, " + admin_name + "</h1>" +
                                "<br /> <h3>Someone has sent a message for you!</h3>" +
                                "<br /><b>From:</b> " + emailFrom +
                                "<br /><b>Name:</b> " + nameFrom +
                                "<br /><b>Message:</b> " + messageFrom +
                                "<br /><br /><a href=\"mailto:" + emailFrom + "?subject=" + encodeURIComponent(emailSubject) + "\">Answer this person</a>" +
                                "<br /><br />Thank you," +
                                "<br /><a href=\"" + htfm_href + "\"><b>HowToFindMe team</b></a>" +
                            "</p>";

                    var data = {
                        message: {
                            text: emailText,
                            subject: emailSubject,
                            from_email: emailFrom,
                            from_name: nameFrom,
                            to: [
                                {
                                    email: admin_email,
                                    name: admin_name
                                }
                            ]
                        }
                    };

                    Parse.Cloud.run( "sendMailTo", data, {
                        success: function (response) {
                            res.success(response)
                        },
                        error: function (error) {
                            res.error(error);
                        }
                    });
                }
            }
            else
            {
                res.error("No admins was found.");
            }
        },
        error: function (error) {
            res.error(error);
        }
    });

});

Parse.Cloud.define( "checkREcaptcha", function (req, res) {

    var ip_url = "http://api.ipify.org?format=json";
    var validation_url = "https://www.google.com/recaptcha/api/verify";

    var remoteip, privatekey;

    var challenge = req.params.challenge;
    var response = req.params.response;

    Parse.Cloud.httpRequest({
        method: 'GET',
        url: ip_url,
        success: function(httpResponse) {

            Parse.Cloud.run( "getSecureData", { field: 'recaptcha_private_key' }, {
                success: function (key) {
                    if (key)
                    {
                        privatekey = key;
                        remoteip = httpResponse.text.ip;

                        Parse.Cloud.httpRequest({
                            method: 'POST',
                            url: validation_url,
                            headers: {
                                'Access-Control-Allow-Origin': '*'
                            },
                            body: {
                                privatekey: privatekey,
                                remoteip: remoteip,
                                challenge: challenge,
                                response: response,
                                error: 'incorrect-captcha-sol'
                            },
                            success: function(httpResponse) {
                                res.success(httpResponse.text);
                            },
                            error: function(httpResponse) {
                                res.error('reCAPTCHA request failed with response code ' + httpResponse.status);
                            }
                        });
                    }
                    else
                    {
                        res.error("Error while trying to get private key");
                    }
                },
                error: function (error) {
                    res.error(error);
                }
            });

        },
        error: function(httpResponse) {
            res.error('IP request failed with response code ' + httpResponse.status);
        }
    });
});

Parse.Cloud.define( "getSecureData", function (req, res) {

    /**
     * TODO add a new customer pair of a public/private keys to Parse DB (current pair is temporary)
     * */
    var field = req.params.field;
    var SecureDataClass = Parse.Object.extend("SecureData");

    var query = new Parse.Query(SecureDataClass);

    query.equalTo("name", field);
    query.find({
        success: function (securedObject) {
            if (securedObject.length > 0)
                res.success(securedObject[0].get('value'));
            else
                res.error(securedObject);
        },
        error: function (error) {
            res.error(error);
        }
    });

});

Parse.Cloud.define( "getUserDataByUniquePathOrID", function(request, response) {
    var path = request.params.path;

    var query = new Parse.Query(Parse.User);
    query.equalTo("unique_path", path);
    query.find({
        success: function(dataObject)
        {
            if(dataObject.length > 0)
            {
                response.success(dataObject);
            }
            else
            {
                var query = new Parse.Query(Parse.User);
                query.equalTo("objectId", path);
                query.find({
                    success: function(dataObjectWithID) {
                        if (dataObjectWithID.length > 0)
                        {
                            response.success(dataObjectWithID);
                        }
                        else
                        {
                            response.error("getUserDataByUniquePathOrID: No user found!");
                        }
                    },
                    error: function(error)
                    {
                        response.error(error + "!");
                    }
                });
            }
        },
        error: function(error)
        {
            response.error(error + "!");
        }
    });
});


Parse.Cloud.define( "getUserDataByEmail", function (request, response) {
    var email = request.params.email;

    var query = new Parse.Query(Parse.User);
    query.equalTo("email", email);
    query.find({
        success: function(userData) {
            if (userData.length > 0)
            {
                response.success(userData);
            }
            else
            {
                response.error("getUserDataByEmail: No user found");
            }
        },
        error: function(error){
            response.error(error);
        }
    });
});


Parse.Cloud.define( "getUserDataByEmailsArray", function (request, response) {
    var emails = request.params.array;

    var query = new Parse.Query(Parse.User);
    query.find({
        success: function(users) {
            if (users.length > 0)
            {
                var output = new Array();
                for (var i in users)
                {
                    for (var j in emails)
                    {
                        var user = users[i];
                        var email = emails[j];

                        if (user.get("email") == email)
                        {
                            output.push(user);
                        }
                    }
                }
                if (output.length > 0)
                    response.success(output);
                else
                    response.error("getUserDataByEmailsArray: No user found");
            }
            else
            {
                response.error("getUserDataByEmailsArray: No user found");
            }
        },
        error: function(error){
            response.error(error);
        }
    });
});


Parse.Cloud.define( "getUserDataByID", function(request, response) {
    var userID = request.params.id;

    var query = new Parse.Query(Parse.User);
    query.equalTo("objectId", userID);
    query.find({
        success: function(userData) {
            if (userData.length > 0)
            {
                response.success(userData);
            }
            else
            {
                response.error("getUserDataByID: No user found");
            }
        },
        error: function(error){
            response.error(error);
        }
    });
});

Parse.Cloud.define( "getCurrentUserContactsByID", function(request, response) {
    var currentUserID = request.params.id;

    var userClassName = "_User";
    var friendClassName = "UserFriend";
    var UserFriendNodeClass = Parse.Object.extend(friendClassName);
    var currentUserFriends = new Parse.Query(UserFriendNodeClass);
    currentUserFriends.include("user");
    currentUserFriends.equalTo("user", { __type: "Pointer", className: userClassName, objectId: currentUserID });

    currentUserFriends.find({
        success: function(friends) {
            var sharedConnection = {};
            var sharedConnections = new Array();
            if (friends.length > 0)
            {
                for (var i in friends)
                {
                    sharedConnection = {
                        sharedConnectionId: friends[i].get("friend").id
                    };
                sharedConnections.push(sharedConnection);
                }
            }
            response.success(sharedConnections);
        },
        error: function(error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define( "getUserObjectsByIdArray", function(request, response) {
    var idArray = request.params.array;

    /**
     * TODO implement UserNetworks object
     * */
    var query = new Parse.Query(Parse.User);
    query.find({
        success: function(objects) {
            if (objects.length > 0)
            {
                if (idArray.length > 0)
                {
                    var userValues = {};
                    var userObjects = new Array();
                    for (var i in objects)
                    {
                        for (var j in idArray)
                        {
                            if (idArray[j].sharedConnectionId == objects[i].id)
                            {
                                userValues = {
                                    id: objects[i].id,
                                    username: objects[i].get("first_name") + " " + objects[i].get("last_name"),
                                    mainloc: objects[i].get("main_location"),
                                    img: objects[i].get("image"),
                                    url: objects[i].get("unique_path"),
                                    isVip: objects[i].get("vip"),
                                    nets: objects[i].get("netw")
                                };
                                userObjects.push(userValues);
                            }
                        }
                    }
                    if (userObjects.length > 0)
                        response.success(userObjects);
                    else
                        response.error("Nothing found in userValues to push");
                }
                else
                {
                    response.error("request parameter request.params.array is empty");
                }
            }
            else
            {
                response.error("getuserObjectsByIdArray: Nothing found");
            }
        },
        error: function(error) {
            response.error(error);
        }
    });


});

Parse.Cloud.define( "getSharedConnectionsByUserID", function(request, response) {
    var currentUserId = request.params.current;
    var queryUserId = request.params.user;

    //$(".quantity").html(results.length + " People");

    var userClassName = "_User";
    var friendClassName = "UserFriend";
    var UserFriendNodeClass = Parse.Object.extend(friendClassName);
    var currentUserFriends = new Parse.Query(UserFriendNodeClass);
    currentUserFriends.include("user");
    currentUserFriends.equalTo("user", { __type: "Pointer", className: userClassName, objectId: currentUserId });
    /**
     * search through current user friends
     * */
    currentUserFriends.find({
        success: function (currentUserFriends) {
            if (currentUserFriends.length > 0)
            {
                var query = new Parse.Query(UserFriendNodeClass);
                query.include("user");
                query.equalTo("user", { __type: "Pointer", className: userClassName, objectId: queryUserId });
                /**
                 * search through query user friends
                 * */
                query.find({
                    success: function(queryUserFriends) {
                        var sharedConnection = {};
                        var sharedConnections = new Array();
                        for (var i in currentUserFriends)
                        {
                            for (var j in queryUserFriends)
                            {
                                // we push every common friend to an array and return it into js function with response.success
                                if (currentUserFriends[i].get("friend").id == queryUserFriends[j].get("friend").id)
                                {
                                    sharedConnection = {
                                        sharedConnectionId: currentUserFriends[i].get("friend").id
                                    };
                                    sharedConnections.push(sharedConnection);
                                }
                            }
                        }
                        response.success(sharedConnections);
                    },
                    error: function(error) {
                        response.error(error);
                    }
                });
            }
            else
            {
                response.error("You are so alone! Why don't you get a friend?");
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
});

Parse.Cloud.define( "getSharedConnectionsByUserIDsArray", function(request, response) {
    var currentUserId = request.params.current;
    var queryUserIdArray = request.params.userArray;
    var count = 0;

    var userClassName = "_User";
    var friendClassName = "UserFriend";
    /**
     * TODO implement UserNetworks object
     * */
    var networksClassName = "UserNetwork";
    var UserFriendNodeClass = Parse.Object.extend(friendClassName);
    var currentUserFriends = new Parse.Query(UserFriendNodeClass);
    currentUserFriends.include("user");
    currentUserFriends.equalTo("user", { __type: "Pointer", className: userClassName, objectId: currentUserId });
    /**
     * search through current user friends
     * */
    currentUserFriends.find({
        success: function (currentUserFriends) {
            if (currentUserFriends.length > 0)
            {
                var consArray = new Array();
                var query = new Parse.Query(UserFriendNodeClass);
                query.include("user");
                /**
                 * search through query user friends
                 * */
                query.find({
                    success: function(queryUserFriends) {
                        for (var i in queryUserIdArray)
                        {
                            if (currentUserFriends.length > 0)
                            {
                                for (var j in queryUserFriends)
                                {
                                    if (queryUserIdArray[i].id == queryUserFriends[j].get("user").id)
                                    {
                                        for (var k in currentUserFriends)
                                        {
                                            if (currentUserFriends[k].get("friend").id == queryUserFriends[j].get("friend").id)
                                                ++count;
                                        }
                                    }
                                }
                            }
                            consArray.push(count);
                            count = 0;
                        }
                        response.success(consArray);
                    },
                    error: function(error) {
                        response.error(error);
                    }
                });
            }
            else
            {
                response.error("You are so alone! Why don't you get a friend?");
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
});

Parse.Cloud.define( "getUserObjectsFromSearchString", function (request, response) {
    var searchString = request.params.search;
    var currentUserID = request.params.current;
    var city = request.params.city;
    var country = request.params.country;

    var cityAndCountry = city + ", " + country;
    var firstQuery = new Parse.Query(Parse.User),
        secondQuery = new Parse.Query(Parse.User),
        mainQuery = new Parse.Query(Parse.User);
    var count = 0;

    if (searchString == '' && (city != '' || country != '' || (city != '' && country != '')))
    {
        /**
         * search through city or country without name
         * */
        if (city != '' && country != '') {
//            firstQuery = mainQuery.contains("main_location", cityAndCountry);
//            secondQuery = mainQuery.startsWith("locations", cityAndCountry);
            firstQuery.contains("main_location", cityAndCountry);
        }
        else if (city != '') {
            firstQuery.contains("main_location", city);
        }
        else {
            firstQuery.contains("main_location", country);
        }
        mainQuery = firstQuery;
    }
    else
    {
        /**
         * search when name is known
         * */
        if (searchString.indexOf(" ") >= 0)
        {
            /**
             * - We support only two words in search query!
             * - BUT MOOOOOOOOM!
             * */
            var s1 = searchString.split(" ")[0];
            var s2 = searchString.split(" ")[1];

            firstQuery = new Parse.Query(Parse.User).contains("first_name", s1).contains("last_name", s2);
            secondQuery = new Parse.Query(Parse.User).contains("first_name", s2).contains("last_name", s1);
        }
        else
        {
            firstQuery = new Parse.Query(Parse.User).contains("first_name", searchString);
            secondQuery = new Parse.Query(Parse.User).contains("last_name", searchString);
        }

        mainQuery = Parse.Query.or(firstQuery, secondQuery);

        if (city != "" && country != "")
        {
            mainQuery.contains("main_location", cityAndCountry);
        }
        else if (city != '')
        {
            mainQuery.contains("main_location", city);
        }
        else
        {
            mainQuery.contains("main_location", country);
        }
    }
    mainQuery.find({
        success: function(users) {
            if (users.length > 0)
            {
                var userObject = {};
                /*var userFriendsIDs = new Array();*/
                var objectArray = new Array();

                if (typeof currentUserID != 'undefined')
                {
                    var userClassName = "_User";
                    var friendClassName = "UserFriend";
                    var UserFriendNodeClass = Parse.Object.extend(friendClassName);
                    var currentUserFriends = new Parse.Query(UserFriendNodeClass);
                    currentUserFriends.include("user");
                    currentUserFriends.equalTo("user", { __type: "Pointer", className: userClassName, objectId: currentUserID });

                    /**
                     * search through current user friends
                     * */
                    currentUserFriends.find({
                        success: function (currentUserFriends) {

                            var query = new Parse.Query(UserFriendNodeClass);
                            query.include("user");
                            /**
                             * search through query user friends
                             * */
                            query.find({
                                success: function(queryUserFriends) {
                                    for (var i in users)
                                    {
                                        if (currentUserFriends.length > 0)
                                        {
                                            for (var j in queryUserFriends)
                                            {
                                                if (users[i].id == queryUserFriends[j].get("user").id)
                                                {
                                                    for (var k in currentUserFriends)
                                                    {
                                                        if (currentUserFriends[k].get("friend").id == queryUserFriends[j].get("friend").id)
                                                        {
                                                            ++count;
                                                            /*userFriendsIDs.push(queryUserFriends[j].get("friend").id);*/
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        userObject = {
                                            id: users[i].id,
                                            username: users[i].get("first_name") + " " + users[i].get("last_name"),
                                            mainloc: users[i].get("main_location"),
                                            img: users[i].get("image"),
                                            url: users[i].get("unique_path"),
                                            connections: count/*,
                                             friendsIDs: userFriendsIDs*/
                                        }
                                        objectArray.push(userObject);

                                        /*userFriendsIDs.pop();*/
                                        count = 0;
                                    }
                                    response.success(objectArray);
                                },
                                error: function(error) {
                                    response.error(error);
                                }
                            });
//
//                            if (currentUserFriends.length > 0)
//                            {
//
//                            }
//                            else
//                            {
//                                response.error("You are so alone! Why don't you get a friend?");
//                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
                else
                {
                    for (var i in users)
                    {
                        userObject = {
                            id: users[i].id,
                            username: users[i].get("first_name") + " " + users[i].get("last_name"),
                            mainloc: users[i].get("main_location"),
                            img: users[i].get("image"),
                            url: users[i].get("unique_path")
                        }
                        objectArray.push(userObject);
                    }
                    response.success(objectArray);
                }
            }
            else
            {
                response.error("Nothing found for that query!");
            }
        },
        error: function(error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define("inviteFriends", function (request, response) {
    var Mailgun = require('mailgun');
    var fr1 = request.params.fr1;
    var fr2 = request.params.fr2;
    var fr3 = request.params.fr3;
    var fr4 = request.params.fr4;
    Mailgun.initialize('myDomainName', 'myAPIKey');
    Mailgun.sendEmail({
        to: fr1,
        from: "main@howtofind.me",
        subject: "Hello from HowToFindMe",
        text: "Take a look at HowToFindMe!",
        to: fr2,
        from: "main@howtofind.me",
        subject: "Hello from HowToFindMe",
        text: "Take a look at HowToFindMe!",
        to: fr3,
        from: "main@howtofind.me",
        subject: "Hello from HowToFindMe",
        text: "Take a look at HowToFindMe!",
        to: fr4,
        from: "main@howtofind.me",
        subject: "Hello from HowToFindMe",
        text: "Take a look at HowToFindMe!"
    }, {
        success: function(httpResponse) {
            console.log(httpResponse);
            response.success("Email sent!");
        },
        error: function(httpResponse) {
            console.error(httpResponse);
            response.error("Uh oh, something went wrong");
        }
    });
});

Parse.Cloud.define( "a", function (request, response) {
    var id = request.params.id;
    var admins = ["uE76zkQryq", "mSe2IxDDdN", "lmumziLTVv"];

    var adminClassName = "Admin";
    var AdminNodeClass = Parse.Object.extend(adminClassName);
    var admin = new Parse.Query(AdminNodeClass);

    admin.include('user');
    admin.equalTo( "user", { __type: "Pointer", className: "_User", objectId: id } );
    admin.find({
        success: function (admin) {
            if (admin.length > 0 )
            {
                response.success(true);
            }
            else
            {
                response.success(false);
            }
        },
        error: function (error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define( "getAllAdmins", function (req, res) {
    var adminClassName = "Admin";
    var AdminNodeClass = Parse.Object.extend(adminClassName);
    var admin = new Parse.Query(AdminNodeClass);

    admin.include('user');
    admin.find({
        success: function (admins) {
            if (admins.length > 0 )
            {
                res.success(admins);
            }
        },
        error: function (error) {
            res.error(error);
        }
    });
});

Parse.Cloud.define( "getAllNetworks", function (request, response) {
    var userClassName = "_User";
    var networkClassName = "Network";
    var networkNodeClass = Parse.Object.extend(networkClassName);
    var network = new Parse.Query(networkNodeClass);

    network.find({
        success: function (networks)
        {
            response.success(networks);
        },
        error: function (error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define( "saveNetworks", function (request, response) {
    var networks = request.params.networks;
    var currentUserID = request.params.currentID;

    var userNetworkClassName = "UserNetwork";
    var userNodeClassName = "_User";
    var networkClassName = "Network";
    var networksList = new Array();
    var deleteObjects = new Array();

    var deleteNodes = Parse.Object.extend(userNetworkClassName);
    var query = new Parse.Query(deleteNodes);
    query.include( "user" );
    query.equalTo( "user", { __type: "Pointer", className: userNodeClassName, objectId: currentUserID } );
    query.find({
        success: function (toDelete) {

            if (toDelete.length > 0)
            {
                for (var i in toDelete)
                {
                    deleteObjects.push(toDelete[i]);
                }

                Parse.Object.destroyAll(deleteObjects, {
                    success: function () {
                        lastSecondSaved(networks);
                    },
                    error: function (error) {
                        response.error(error);
                    }
                });
            }
            else
            {
                lastSecondSaved(networks);
            }
        },
        error: function (error) {
            response.error(error);
        }
    });

    function lastSecondSaved(networks)
    {
        if (networks.length > 0)
        {
            for (var i in networks)
            {
                var NetworkNodeClass = Parse.Object.extend(userNetworkClassName);
                var networkNode = new NetworkNodeClass();

                var network = networks[i];

                var networkID = network.id;

                var URI = network.href;
                var isFavorite = network.isFavorite;
                var networkPoint = { __type: "Pointer", className: networkClassName, objectId: networkID };
                var userPoint = { __type: "Pointer", className: userNodeClassName, objectId: currentUserID };

                networkNode.set( "URI", URI);
                networkNode.set( "isFavorite", isFavorite);
                networkNode.set( "network", networkPoint);
                networkNode.set( "user", userPoint);

                networksList.push(networkNode);
            }
        }
        else
        {
            networksList.splice(0, networksList.length);
        }

        Parse.Object.saveAll(networksList, {
            success: function (list) {
                response.success(true);
            },
            error: function (error) {
                response.error(error);
            }
        });
    }
});

Parse.Cloud.define( "getUserSavedNetworksByUserID", function(request, response) {
    var userID = request.params.id;

    var userClassName = "_User";
    var networkClassName = "Network";
    var userNetworkClassName = "UserNetwork";
    var User = Parse.User; /** _User */
    var Network = Parse.Object.extend(networkClassName);
    var UserNetwork = Parse.Object.extend(userNetworkClassName);

    var innerQuery = new Parse.Query(UserNetwork);
    innerQuery.include("network");
    innerQuery.include("user");
    innerQuery.equalTo("user", { __type: "Pointer", className: userClassName, objectId: userID });

    innerQuery.find({
        success: function (userNetworks) {
            var dataArray = new Array();

            for (var i in userNetworks)
            {
                var dataObject = {
                    userId: userID,
                    networksCount: userNetworks.length,
                    networkID: userNetworks[i].get("network").id,
                    networkURI: userNetworks[i].get("network").get("maskURI"),
                    networkName: userNetworks[i].get("network").get("name"),
                    networkStatus: userNetworks[i].get("network").get("status"),
                    networkIconURI: userNetworks[i].get("network").get("iconURI"),
                    userURI: userNetworks[i].get("URI"),
                    userFavorite: userNetworks[i].get("isFavorite")
                };
                dataArray.push(dataObject);
            }

            response.success(dataArray);
        },
        error: function (error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define( "resendActivationLink", function (request, response) {
    id = request.params.id;

    Parse.Cloud.run( "getUserDataByID", { id: id }, {
        success: function (users) {
            if (users.length > 0)
            {
                for (var i in users)
                {
                    var user = users[i];
                    var email = user.get("email");

                    user.set("first_name", user.get("first_name"));
                    user.set("last_name", user.get("last_name"));
                    user.set("email", email);
                    user.set("password", user.get("password"));
                    user.set("username", user.get("username"));
                    user.set("image", user.get("image"));
                    user.set("intro", user.get("intro"));
                    user.set("locations", user.get("locations"));
                    user.set("main_location", user.get("main_location"));
                    user.set("theme_color", user.get("theme_color"));
                    user.set("vip", user.get("vip"));
                    user.set("unique_path", user.get("unique_path"));
                    user.set("cookieAccepted", user.get("cookieAccepted"));

                    user.save(null, {
                        success: function () {
                            response.success(true);
                        },
                        error: function (error) {
                            response.error(error);
                        }
                    });
                }
            }
            else
            {
                response.error(user);
            }
        },
        error: function (error) {
            response.error(error);
        }
    });
});


Parse.Cloud.define( "isUseraMember", function (request, response) {
    var emailsArray = request.params.array;

    var query = new Parse.Query(Parse.User);

    query.find({
        success: function(userData) {
            var output = new Array();
            if (userData.length > 0)
            {
                for (var i in userData)
                {
                    for (var j in emailsArray)
                    {
                        var user = userData[i];
                        var requestedEmail = emailsArray[j];

                        if (user.get("email") == requestedEmail)
                        {
                            output.push(requestedEmail);
                        }
                    }
                }
                response.success(output);
            }
            else
            {
                response.error("isUseraMember: No user found");
            }
        },
        error: function(error){
            response.error(error);
        }
    });
});



Parse.Cloud.define( "getAvailableUserColors", function (request, response) {
    var colorId = request.params.id;
    var colorClassName = "Color";
    var colorNodeClass = Parse.Object.extend(colorClassName);
    var color = new Parse.Query(colorNodeClass);

    if (colorId)
    {
        color.equalTo( "objectId", colorId);
    }

    color.find({
        success: function (colors)
        {
            response.success(colors);
        },
        error: function (error) {
            response.error(error);
        }
    });
});



Parse.Cloud.define( "getUserColor", function (request, response) {
    var userID = request.params.id;

    var userClassName = "_User";
    var colorClassName = "Color";
    var userColorClassName = "UserColor";

    var Color = Parse.Object.extend(colorClassName);
    var UserColor = Parse.Object.extend(userColorClassName);

    var query = new Parse.Query(UserColor);

    query.include("userPointer");
    query.include("userColor");
    query.equalTo("userPointer", { __type: "Pointer", className: userClassName, objectId: userID });

    query.find({
        success: function (userColor) {
            response.success(userColor);
        },
        error: function (error) {
            response.error(error);
        }
    });
});


Parse.Cloud.define( "saveUserColorTheme", function (request, response) {
    var userId = request.params.id;
    var colorId = request.params.color;
    var isUpdate = request.params.update;

    var userColorClassName = "UserColor";
    var userNodeClassName = "_User";
    var colorClassName = "Color";

    var UserColors = Parse.Object.extend(userColorClassName);
    var userColorClass = new UserColors();
    var query = new Parse.Query(UserColors);

    if (isUpdate)
    {
        query.include( "userPointer" );
        query.equalTo( "userPointer", { __type: "Pointer", className: userNodeClassName, objectId: userId } );
        query.first({
            success: function (userColorNode)
            {
                userColorNode.set("userColor", { __type: "Pointer", className: colorClassName, objectId: colorId });
                userColorNode.save( null, {
                    success: function(userColorNode) {
                        response.success(userColorNode);
                    },
                    error: function (userColorNode, error) {
                        response.error(error);
                    }
                });
            },
            error: function (userColorNodeArr, error)
            {
                response.error(error);
            }
        });
    }
    else
    {
        userColorClass.set("userPointer", { __type: "Pointer", className: userNodeClassName, objectId: userId });
        userColorClass.set("userColor", { __type: "Pointer", className: colorClassName, objectId: colorId });
        userColorClass.save( null, {
            success: function (userColorClass) {
                response.success(userColorClass);
            },
            error: function (userFriendNode, error) {
                response.error(error);
            }
        });
    }
});