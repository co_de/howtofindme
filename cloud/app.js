// These two lines are required to initialize Express in Cloud Code.
var express = require('express');
var app = express();
var parseExpressHttpsRedirect = require('parse-express-https-redirect');

app.use(parseExpressHttpsRedirect());
// Global app configuration section
app.set('views', 'cloud/views');  // Specify the folder to find templates
app.set('view engine', 'ejs');    // Set the template engine
app.use(express.bodyParser());    // Middleware for reading request body

// This is an example of hooking up a request handler with a specific request
// path and HTTP verb using the Express routing API.

app.get('/search', function (req, res) {
    res.render('search', {  message: 'Search Results' });
});

app.get('/contacts', function (req, res) {
    res.render('contacts', {  message: 'Contact Us' });
});

app.get('/edit', function (req, res) {
    res.render('edit', {  message: 'Edit Profile' });
});

app.get('/connections', function (req, res) {
    res.render('connections', {  message: 'Shared Connections' });
});

app.get('/mycontacts', function (req, res) {
    res.render('myContacts', {  message: 'My Contacts' });
});

app.get('/admin', function (req, res) {
    res.render('admin', {  message: 'Admin panel' });
});

app.get('/myprofile', function (req, res) {
    res.render('myprofile', {  message: 'My Profile' });
});

app.get('/register/thank-you', function (req, res) {
    res.render('step1', { message: 'Thank You' });
});

app.get('/register/step2', function (req, res) {
    res.render('step2', { message: 'adding unique url' });
});

//app.get('/profile/:id', function (req, res) {
//    res.render('userProfile', {  message: req.params.id});
//});

app.get('/:id', function (req, res) {
    res.render('user', {  message: req.params.id });
});

// // Example reading from the request query string of an HTTP get request.
// app.get('/test', function(req, res) {
//   // GET http://example.parseapp.com/test?message=hello
//   res.send(req.query.message);
// });

// // Example reading from the request body of an HTTP post request.
// app.post('/test', function(req, res) {
//   // POST http://example.parseapp.com/test (with request body "message=hello")
//   res.send(req.body.message);
// });

// Attach the Express app to Cloud Code.
app.listen();